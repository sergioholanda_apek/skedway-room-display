import * as React from "react"
import Svg, {Path} from "react-native-svg"
import {useTheme} from "react-native-paper";

const HomeDoor = (props) => {

  const {colors} = useTheme();
  const {focused} = props;

  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      fill={focused ? colors.active : colors.inactive}
      viewBox="0 0 16.5 20"
      {...props}
    >
      <Path
        d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
    </Svg>
  )

}

export default HomeDoor;
