export default {
  hello: "Hello, ",
  welcome: "welcome ",
  to: "to",

  instructions: "Please enter your e-mail address to get started",
  email: "Email",
  invalidEmail: "Invalid Email",
  next: "Next",
  login: "Login",
  password_instructions: "Please enter your password",
  account_info: "Please select the account you want to access",
  accounts: "Accounts",
  forgot_password: "Forgot my password",
  forgot_password_info: "If you forget the password, tap on Forgot my password. %{0} will send instructions for your e-mail address.",
  app_description: "The corporate spaces management platform. Manage rooms and work positions in a simple and organized way, and realize that managing spaces is the smart way to save resources and increase productivity.",

  errorDialog: "An error has occurred",
  user_not_administrator: "This user is not an administrator",
  company_removed: "Your company was removed",
  unknown_user_or_removed: "User unknown or removed",
  company_plan_expired: "Your company plan expired",
  user_blocked: "User blocked after too many attempts to access",
  error_password_expired: "Your password has expired! Register a new password.",
  user_unknown_or_cannot_log_in_to_the_device: "User unknown or cannot log to the device."
}