import React, {useContext, useEffect, useRef, useState} from "react";
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import _ from "lodash";
import {parseISO} from "date-fns";
import i18n from "i18n-js";

import {SchedulingContext} from "contexts/SchedulingContext";
import {StorageContext} from "contexts/StorageContext";
import {LoadingContext} from "contexts/LoadingContext";

import SchedulerHeader from "components/Header/SchedulerHeader";
import SchedulerChunkDetail from "components/Home/BookingDetail/SchedulerChunkDetail";
import CustomCalendar from "components/Calendar/CustomCalendar";
import CustomAreaChart from "components/Calendar/CustomAreaChart";
import MainLayout from "components/common/layout/MainLayout";

import useOrientation from "hooks/useOrientation";

import {mapMonthChart} from "utils/dateUtils";
import normalize from "utils/normalize";
import Calendar from "../../assets/icons/Calendar";

const CalendarScreen = ({navigation}) => {

  const {
    occupancy,
    isFetchingMonthOccupancy,
    selectedSchedule,
    fetchMonthOccupancy,
    setCurrentDay,
    setSelectedSchedule
  } = useContext(SchedulingContext);

  const {appStorage} = useContext(StorageContext);

  const {isLoadingResource, setLoadingResource} = useContext(LoadingContext);

  const {site} = appStorage;

  const [showCalendar, setShowCalendar] = useState(true);

  const orientation = useOrientation();
  const isPortrait = orientation === "PORTRAIT";

  const calendarRef = useRef(null);

  useEffect(() => {
    navigation.setParams({isPortrait});
    navigation.navigate("Home");
  }, [isPortrait]);

  useEffect(() => {
    console.log('[Calendar]')
    const listenerFocus = navigation.addListener("didFocus", () => {
      setSelectedSchedule(null);
      setIsFocused(true);
      // calendarRef.current.scrollToDay(new Date(), undefined, true);
    });

    const listenerBlur = navigation.addListener("didBlur", () => {
      setMonthsIndex([]);
      setIsFocused(false);
    });

    return () => {
      listenerFocus.remove();
      listenerBlur.remove();
    };
  }, []);

  useEffect(() => {
    setLoadingResource({show: isLoadingResource});
  }, [isLoadingResource, isFetchingMonthOccupancy]);

  // To control occupancy fetch requests
  const [monthsIndex, setMonthsIndex] = useState([]);

  const [isFocused, setIsFocused] = useState(true);

  // To know which month is currently visible, e.g., '2020-03-14'
  const [currentMonth, setCurrentMonth] = useState("");

  const monthOccupancy = occupancy && occupancy[currentMonth];

  const data = mapMonthChart(currentMonth, monthOccupancy);

  const onVisibleMonthsChange = _.debounce((months) => {
    const {dateString: date} = months[months.length - 1];
    const index = date;

    setCurrentMonth(index);

    // An occupancy request is made if the month has not yet been fetched
    if (!monthsIndex.find((ind) => ind === index)) {
      fetchMonthOccupancy(date, index, site.openingTime, site.closingTime);
      setMonthsIndex([...monthsIndex, index]);
    }
  }, 150);

  const onDayClicked = (date) => {
    setCurrentDay(parseISO(date.dateString));
    navigation.navigate("Home");
  };

  const contentContainerStyle = {
    flex: 1,
    width: isPortrait ? "100%" : "50%",
    marginBottom: isPortrait ? Math.max(normalize(38), 50) : 0,
  };

  if (!isFocused) {
    return null;
  }

  return (
    <MainLayout>
      <SchedulerHeader showStatusIcon={false}/>
      {selectedSchedule ? (
        <SchedulerChunkDetail {...selectedSchedule} />
      ) : (
        <View style={{flex: 0.9, alignItems: "center"}}>
          {
            showCalendar &&
            <View style={{flex: 10, padding: normalize(4)}}>
              <CustomCalendar
                ref={calendarRef}
                customMarkedDates={occupancy && occupancy[currentMonth]}
                onVisibleMonthsChange={onVisibleMonthsChange}
                onDayClicked={onDayClicked}
                isFetching={isFetchingMonthOccupancy}
              />
            </View>
          }

          {
            !showCalendar &&
            <View style={{flex: 10, marginTop: normalize(10)}}>
              <CustomAreaChart data={data}/>
            </View>
          }

          <View style={{flex: 1}}>
            <TouchableOpacity
              style={{alignContent: "center"}}
              underlayColor="none"
              onPress={() => setShowCalendar(!showCalendar)}
            >
              <Image
                source={showCalendar ? require("../../assets/img/up.png") : require("../../assets/img/down.png")}
              />
            </TouchableOpacity>
          </View>
        </View>
      )}
    </MainLayout>
  );
};

CalendarScreen.navigationOptions = ({navigation}) => {
  const isPortrait = navigation.getParam("isPortrait");

  return {
    tabBarLabel: ({focused}) => {
      return (
        <Text
          style={{
            color: focused ? "#717171" : "#BCC6C6",
            fontFamily: "Manrope_400Regular",
            fontSize: Math.max(normalize(7), 11),
            textAlign: "center"
          }}
        >
          {i18n.t("title_calendar")}
        </Text>
      );
    },
    tabBarIcon: ({focused}) => {
      return (
        <Calendar height="100%" width="100%" focused={focused}/>
      );
    },
  };
};

const styles = StyleSheet.create({
  loadingIndicator: {
    position: "absolute",
    top: 150,
    alignSelf: "center",
  },
  backgroundContainer: {
    justifyContent: "flex-end",
  },
  surfaceCircle: {
    borderRadius: 50,
    width: 50,
    height: 50,
    alignSelf: "center",
    justifyContent: "center",
  },
});

export default CalendarScreen;
