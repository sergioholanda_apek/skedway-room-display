import * as React from "react"
import {SvgXml} from "react-native-svg"

const xml = `
<svg width="566" height="189" viewBox="0 0 566 189" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M362.885 -3.52142V100.916L107.363 -17.2684V-122.436L362.885 -3.52142Z" fill="url(#paint0_linear_2_6676)"/>
  <path d="M-100.884 -25.3376V79.0043L107.361 -17.6325V-122.436L-100.884 -25.3376Z" fill="url(#paint1_linear_2_6676)"/>
  <path d="M-24.8525 -34.6263V5.76118L39.2052 -25.0266V-65.4141L-24.8525 -34.6263Z" fill="url(#paint2_linear_2_6676)" fill-opacity="0.67"/>
  <path d="M152.322 385.269L362.885 196.565L362.885 98.431H153.963L152.322 385.269Z" fill="url(#paint3_linear_2_6676)"/>
  <path d="M-100.885 265.797L175.988 395.006L175.988 111.416L9.99299 111.416L-100.885 79.0102L-100.885 265.797Z" fill="url(#paint4_linear_2_6676)" fill-opacity="0.81"/>
  <path d="M-259.711 72.1109L20.9393 -58.7261L360.984 98.7776L80.3499 229.974L-259.711 72.1109Z" fill="url(#paint5_linear_2_6676)"/>
  <rect width="22.8783" height="22.8686" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 232.198 121.686)" fill="url(#paint6_linear_2_6676)"/>
  <rect width="22.8783" height="22.8686" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 82.033 48.0673)" fill="url(#paint7_linear_2_6676)"/>
  <rect width="22.8783" height="22.8686" transform="matrix(0.906653 -0.421877 0.906653 0.421877 101.887 130.778)" fill="url(#paint8_linear_2_6676)"/>
  <rect width="22.8783" height="22.8686" transform="matrix(0.906653 -0.421877 0.906653 0.421877 64.6755 114.415)" fill="url(#paint9_linear_2_6676)"/>
  <rect width="22.8783" height="22.8686" transform="matrix(0.906653 -0.421877 0.906653 0.421877 29.7263 98.0484)" fill="url(#paint10_linear_2_6676)"/>
  <rect width="23.8596" height="23.8496" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 167.248 62.2166)" fill="url(#paint11_linear_2_6676)"/>
  <path d="M211.448 108.3V131.039L232.801 121.038V98.2988L211.448 108.3Z" fill="white"/>
  <rect width="23.8596" height="23.8496" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 164.992 42.2145)" fill="url(#paint12_linear_2_6676)"/>
  <path d="M40.627 25.7269V48.466L61.9795 38.4645V15.7254L40.627 25.7269Z" fill="white"/>
  <rect width="23.8596" height="23.8496" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 201.074 56.7596)" fill="url(#paint13_linear_2_6676)"/>
  <path d="M86.1792 101.175V123.914L64.8266 113.913V91.1736L86.1792 101.175Z" fill="white"/>
  <path d="M50.5938 85.5192V108.258L29.2412 98.2568V75.5177L50.5938 85.5192Z" fill="white"/>
  <path d="M123.19 118.257V140.996L101.838 130.995V108.255L123.19 118.257Z" fill="white"/>
  <rect width="23.8596" height="23.8496" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 236.026 74.0312)" fill="url(#paint14_linear_2_6676)"/>
  <g filter="url(#filter0_d_2_6676)">
  <rect width="93.7038" height="146.83" rx="0.616626" transform="matrix(0.906653 -0.421877 0.906653 0.421877 24.4348 65.1894)" fill="url(#paint15_linear_2_6676)"/>
  </g>
  <path d="M164.472 20.0397V42.7788L143.119 32.7773V10.0382L164.472 20.0397Z" fill="white"/>
  <path d="M201.483 34.2698V57.0089L180.131 47.0074V24.2683L201.483 34.2698Z" fill="white"/>
  <path d="M235.649 51.3597V74.0988L214.296 64.0973V41.3582L235.649 51.3597Z" fill="white"/>
  <defs>
  <filter id="filter0_d_2_6676" x="22.5012" y="25.8103" width="221.948" height="108.571" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
  <feFlood flood-opacity="0" result="BackgroundImageFix"/>
  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
  <feOffset dy="5.13855"/>
  <feGaussianBlur stdDeviation="1.13048"/>
  <feComposite in2="hardAlpha" operator="out"/>
  <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"/>
  <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_2_6676"/>
  <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_2_6676" result="shape"/>
  </filter>
  <linearGradient id="paint0_linear_2_6676" x1="511.129" y1="193.124" x2="-101.788" y2="50.0911" gradientUnits="userSpaceOnUse">
  <stop offset="0.0410842" stop-color="#B4EFD3"/>
  <stop offset="1" stop-color="white" stop-opacity="0"/>
  </linearGradient>
  <linearGradient id="paint1_linear_2_6676" x1="380.011" y1="-123.797" x2="25.9496" y2="335.148" gradientUnits="userSpaceOnUse">
  <stop offset="0.0410842" stop-color="#B4EFD3"/>
  <stop offset="1" stop-color="white" stop-opacity="0"/>
  </linearGradient>
  <linearGradient id="paint2_linear_2_6676" x1="-24.8525" y1="-60.7923" x2="33.2171" y2="83.6366" gradientUnits="userSpaceOnUse">
  <stop offset="0.0410842" stop-color="#B4EFD3"/>
  <stop offset="1" stop-color="white"/>
  </linearGradient>
  <linearGradient id="paint3_linear_2_6676" x1="117.966" y1="114.036" x2="229.46" y2="329.268" gradientUnits="userSpaceOnUse">
  <stop offset="0.0410842" stop-color="#B4EFD3"/>
  <stop offset="1" stop-color="white" stop-opacity="0"/>
  </linearGradient>
  <linearGradient id="paint4_linear_2_6676" x1="-137.595" y1="68.2478" x2="35.0715" y2="319.773" gradientUnits="userSpaceOnUse">
  <stop offset="0.0410842" stop-color="#B4EFD3"/>
  <stop offset="1" stop-color="white" stop-opacity="0"/>
  </linearGradient>
  <linearGradient id="paint5_linear_2_6676" x1="67.8465" y1="229.73" x2="-146.79" y2="-211.045" gradientUnits="userSpaceOnUse">
  <stop offset="0.0410842" stop-color="#B4EFD3"/>
  <stop offset="1" stop-color="white"/>
  </linearGradient>
  <linearGradient id="paint6_linear_2_6676" x1="23.4147" y1="11.9895" x2="7.32349" y2="8.02673" gradientUnits="userSpaceOnUse">
  <stop stop-color="white"/>
  <stop offset="1" stop-color="#E7F6EF"/>
  </linearGradient>
  <linearGradient id="paint7_linear_2_6676" x1="-8.56236" y1="26.3276" x2="29.4935" y2="25.9528" gradientUnits="userSpaceOnUse">
  <stop stop-color="white"/>
  <stop offset="1" stop-color="#E7F6EF"/>
  </linearGradient>
  <linearGradient id="paint8_linear_2_6676" x1="23.4147" y1="11.9895" x2="7.32349" y2="8.02673" gradientUnits="userSpaceOnUse">
  <stop stop-color="white"/>
  <stop offset="1" stop-color="#E7F6EF"/>
  </linearGradient>
  <linearGradient id="paint9_linear_2_6676" x1="23.4147" y1="11.9895" x2="7.32349" y2="8.02673" gradientUnits="userSpaceOnUse">
  <stop stop-color="white"/>
  <stop offset="1" stop-color="#E7F6EF"/>
  </linearGradient>
  <linearGradient id="paint10_linear_2_6676" x1="23.4147" y1="11.9895" x2="7.32349" y2="8.02673" gradientUnits="userSpaceOnUse">
  <stop stop-color="white"/>
  <stop offset="1" stop-color="#E7F6EF"/>
  </linearGradient>
  <linearGradient id="paint11_linear_2_6676" x1="5.14376" y1="27.9521" x2="3.5485" y2="3.79935" gradientUnits="userSpaceOnUse">
  <stop stop-color="white"/>
  <stop offset="1" stop-color="#E7F6EF"/>
  </linearGradient>
  <linearGradient id="paint12_linear_2_6676" x1="5.14376" y1="27.9521" x2="3.5485" y2="3.79935" gradientUnits="userSpaceOnUse">
  <stop stop-color="white"/>
  <stop offset="1" stop-color="#E7F6EF"/>
  </linearGradient>
  <linearGradient id="paint13_linear_2_6676" x1="5.14376" y1="27.9521" x2="3.5485" y2="3.79935" gradientUnits="userSpaceOnUse">
  <stop stop-color="white"/>
  <stop offset="1" stop-color="#E7F6EF"/>
  </linearGradient>
  <linearGradient id="paint14_linear_2_6676" x1="5.14376" y1="27.9521" x2="3.5485" y2="3.79935" gradientUnits="userSpaceOnUse">
  <stop stop-color="white"/>
  <stop offset="1" stop-color="#E7F6EF"/>
  </linearGradient>
  <linearGradient id="paint15_linear_2_6676" x1="20.2011" y1="172.088" x2="4.90184" y2="24.3262" gradientUnits="userSpaceOnUse">
  <stop stop-color="white"/>
  <stop offset="1" stop-color="white"/>
  </linearGradient>
  </defs>
</svg>
`;

export default (props) => <SvgXml {...props} xml={xml} />;
