import React, {useContext, useEffect, useState} from "react";
import {Dimensions, Image, Keyboard, StyleSheet, TextInput, TouchableHighlight, View} from "react-native";
import i18n from "i18n-js";
import uuid from "react-native-uuid";
import {ActivityIndicator, Chip, Text, useTheme} from "react-native-paper";
import {navigate} from "utils/navigationRef";

import {addMinutes, differenceInSeconds, format, formatRFC3339, getMinutes, isToday, parse,} from "date-fns";
import {datesDifference, getNextRoundedDate} from "utils/dateUtils";

import MultiSlider from "@ptomasroos/react-native-multi-slider";

import {SchedulingContext} from "contexts/SchedulingContext";
import {StorageContext} from "contexts/StorageContext";
import {SnackbarContext} from "contexts/SnackbarContext";
import {UserSuggestionContext} from "contexts/UserSuggestionContext";
import {getHourFormat} from "utils/hourUtils";

import CustomStackCard from "../../common/layout/CustomStackCard";
import PersonFill from "../../../../assets/icons/PersonFill";
import PeopleFill from "../../../../assets/icons/PeopleFill";
import AlarmFill from "../../../../assets/icons/AlarmFill";
import CardText from "../../../../assets/icons/CardText";
import isEmpty from "lodash/isEmpty";
import PinDialog from "../../pinDialog/PinDialog";
import CustomDialog from "../../common/dialog/CustomDialog";

import normalize from "utils/normalize";

const ONE_MINUTE = 60;
const MINUTES_TO_ROUND = 10;

const SLIDER_ICON_FONT_SIZE = 24;
const SLIDER_RIGHT_SPACING = 32;

const CustomMarker = ({pressed}) => {
  const {colors} = useTheme();

  return (
    <View
      style={[styles.marker, pressed && {backgroundColor: colors.primary}]}
    />
  );
};

/*
 * TODO: Do not let the user book when it is close
 * to closingTime, e.g., 20:57 and closingTime: 21:00
 */
const SchedulerBookingModal = ({navigation}) => {

  const title = navigation && navigation.getParam("title");
  const currentHour = navigation && navigation.getParam("currentHour");
  const openingTime = navigation && navigation.getParam("openingTime");
  const closingTime = navigation && navigation.getParam("closingTime");
  const currentDate = navigation && navigation.getParam("currentDay");

  const {colors} = useTheme();

  const {appStorage} = useContext(StorageContext);

  const {
    bookTimeSchedule,
    hasBookedSuccessfully,
    isBookingTimeSchedule,
    userAccount,
    setHasBookedSuccessfully,
    bookingError,
    setBookingError
  } = useContext(SchedulingContext);

  const {selectedUsers, setSelectedUsers, removeUser} = useContext(UserSuggestionContext);
  const {setNotification} = useContext(SnackbarContext);

  const [subject, setSubject] = useState(null);
  const [selectedHost, setSelectedHost] = useState(null);
  const [showPinDialog, setShowPinDialog] = useState(false);
  const [showError, setShowError] = useState(false);
  const [errorTitle, setErrorTitle] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const {site} = appStorage;
  const hourFormat = getHourFormat(site && site.hourFormat24);

  useEffect(() => {
    if (hasBookedSuccessfully) {
      // The modal should be closed when a time schedule has been booked successfully
      onDismiss();
      setHasBookedSuccessfully(false);
      setNotification(i18n.t("success_scheduling"));
    }
    return () => {
      setSelectedUsers([]);
    }
  }, [hasBookedSuccessfully]);

  useEffect(() => {
    if (bookingError) {
      setShowError(true);
      setErrorTitle(i18n.t("error_space_not_available"));
      setErrorMessage(bookingError);
    }
    return () => {
      setBookingError(null);
    }
  }, [bookingError]);

  const onDismiss = () => navigation && navigation.goBack();

  const sliderLength =
    Dimensions.get("window").width -
    (2 * normalize(16) + 16 + SLIDER_ICON_FONT_SIZE + SLIDER_RIGHT_SPACING);

  const verifyCheckInRequiresAuthentication = () => {
    if (appStorage?.company?.checkInRequiresAuthentication) {
      Keyboard.dismiss();
      setShowPinDialog(true);
    } else {
      pinAction();
    }
  };

  const pinAction = () => {
    if (!selectedHost) {
      setShowError(true);
      setErrorTitle(i18n.t("error_field_required"));
      setErrorMessage(i18n.t("booking_required_field_title_error"));
      return;
    }
    const date = format(currentDate, "yyyy-MM-dd");
    const startDate = `${date} ${hourFrom}`;
    const endDate = `${date} ${hourTo}`;

    const parsedStartDate = parse(startDate, `yyyy-MM-dd ${hourFormat}`, new Date());
    const parsedEndDate = parse(endDate, `yyyy-MM-dd ${hourFormat}`, new Date());

    const subject_ = subject ?? i18n.t("Scheduled_for_user", {input: selectedHost.name});

    bookTimeSchedule(
      isToday(currentDate),
      formatRFC3339(parsedStartDate),
      formatRFC3339(parsedEndDate),
      selectedHost.id,
      selectedUsers,
      subject_
    );
  };

  const diffBetweenWindow = datesDifference(openingTime, closingTime);

  const parsedOpeningTime = parse(openingTime, "HH:mm:ss", new Date());
  const parsedClosingTime = parse(closingTime, "HH:mm:ss", new Date());
  const parsedOpeningTimeRounded = getNextRoundedDate(
    MINUTES_TO_ROUND,
    parse(openingTime, "HH:mm:ss", new Date())
  );
  const minutesGap = Math.ceil(
    differenceInSeconds(parsedOpeningTimeRounded, parsedOpeningTime) /
    ONE_MINUTE
  );

  // The hour can change every ten minutes
  const sliderStep = 10;
  const sliderMin = 0;
  const sliderMax = Math.ceil(
    differenceInSeconds(parsedClosingTime, parsedOpeningTime) / ONE_MINUTE
  );

  const parsedHourFrom = parse(currentHour, hourFormat, new Date());

  const parsedHourTo = addMinutes(
    parsedHourFrom,
    diffBetweenWindow >= ONE_MINUTE
      ? ONE_MINUTE - getMinutes(parsedHourFrom)
      : sliderMax
  );

  const epslon = minutesGap ? sliderStep - minutesGap : 0;

  const minutesFrom =
    Math.ceil(
      differenceInSeconds(parsedHourFrom, parsedOpeningTime) / ONE_MINUTE
    ) + epslon;

  const minutesTo =
    Math.ceil(
      differenceInSeconds(parsedHourTo, parsedOpeningTime) / ONE_MINUTE
    ) + epslon;

  const [sliderHours, setSliderHours] = useState([
    minutesFrom < sliderStep ? 0 : minutesFrom,
    minutesTo,
  ]);

  const updatedHourFrom =
    sliderHours[0] === 0
      ? parsedOpeningTime
      : sliderHours[0] === 10
        ? addMinutes(parsedOpeningTime, minutesGap ? minutesGap : sliderStep)
        : addMinutes(
          parsedOpeningTime,
          sliderHours[0] + minutesGap - (minutesGap ? sliderStep : 0)
        );

  const updatedHourTo =
    sliderHours[1] === 0
      ? parsedOpeningTime
      : sliderHours[1] === 10
        ? addMinutes(parsedOpeningTime, minutesGap ? minutesGap : sliderStep)
        : addMinutes(
          parsedOpeningTime,
          sliderHours[1] + minutesGap - (minutesGap ? sliderStep : 0)
        );

  const hourFrom = format(updatedHourFrom, hourFormat);
  const hourTo = format(updatedHourTo, hourFormat);

  const isItToday = isToday(currentDate);

  const todayTextTitle = i18n.t("Schedule_for", {
    input: isItToday
      ? i18n.t("today")
      : format(currentDate, i18n.t("format_date")),
  });

  const handleSelectHost = (user) => {
    setSelectedHost(user);
  }

  const handleSelectGuest = (user) => {
    const updatedSelectedUsers = [...selectedUsers, user];
    setSelectedUsers(updatedSelectedUsers);
  }

  const closeModal = () => {
    setShowError(false);
    setBookingError(null);
    setErrorTitle("")
    setErrorMessage("");
  }

  return (
    <CustomStackCard
      headerLabel={todayTextTitle}
      backAction={() => navigation.goBack()}
      action={verifyCheckInRequiresAuthentication}
      verifyCheckInRequiresAuthentication
      actionLabel={i18n.t("action_schedule")}
    >
      <PinDialog
        pictureUrl={userAccount && userAccount.pictureUrl}
        name={userAccount && userAccount.name}
        pinToValidate={userAccount && userAccount.pin}
        visible={showPinDialog}
        setDialogOpen={setShowPinDialog}
        onConfirmCallback={() => pinAction()}
      />

      <CustomDialog
        visible={showError}
        title={errorTitle}
        content={errorMessage}
        confirmAction={() => closeModal()}
        confirmLabel="Ok"
      />

      <View style={{flexDirection: "row", marginBottom: normalize(10)}}>
        <View style={styles.icon}>
          <CardText height={normalize(14)} width={normalize(14)} color={colors.primary}/>
        </View>
        <View style={styles.subject}>
          <TextInput
            style={{fontSize: normalize(8), fontFamily: "Manrope_400Regular"}}
            multiline
            numberOfLines={4}
            editable
            maxLength={40}
            placeholder={i18n.t("prompt_issue_description")}
            onChangeText={setSubject}
          />
        </View>
      </View>

      <View style={{flexDirection: "row", alignItems: "center", marginBottom: normalize(10)}}>
        <TouchableHighlight
          style={styles.icon}
          onPress={() => {
            navigate("UserSuggestion", {
              todayTextTitle,
              selectedUsers: selectedHost ? [selectedHost] : [],
              selectUser: handleSelectHost
            })
          }}
          underlayColor={colors.white}
        >
          {
            selectedHost ? (
              <Image
                style={{
                  height: "100%",
                  width: "100%",
                  borderRadius: normalize(8),
                  padding: 0,
                  resizeMode: "cover",
                }}
                size={50}
                source={{uri: selectedHost.pictureUrl}}
              />
            ) : (
              <PersonFill height={normalize(14)} width={normalize(14)} color={colors.primary}/>
            )
          }
        </TouchableHighlight>
        <Text style={styles.labelIcon}>
          {selectedHost ? selectedHost.name : i18n.t("Host")}
        </Text>
      </View>

      <View style={{
        flexDirection: "row",
        alignItems: isEmpty(selectedUsers) ? "center" : "flex-start",
        marginBottom: normalize(10)
      }}>
        <TouchableHighlight
          style={styles.icon}
          onPress={() => {
            navigate("UserSuggestion", {todayTextTitle, selectedUsers, selectUser: handleSelectGuest})
          }}
          underlayColor={colors.white}
        >
          <PeopleFill height={normalize(14)} width={normalize(14)} color={colors.primary}/>
        </TouchableHighlight>
        {
          isEmpty(selectedUsers) ? (
            <Text style={styles.labelIcon}>
              {i18n.t("Guests")}
            </Text>
          ) : (
            <View style={{marginLeft: normalize(8), flexDirection: "row", flexWrap: 'wrap'}}>
              {
                selectedUsers.map(guest =>
                  <Chip
                    key={uuid.v4()}
                    style={{marginRight: normalize(8), marginBottom: normalize(8), borderRadius: 10}}
                    textStyle={{fontFamily: "Manrope_400Regular", fontSize: normalize(8)}}
                    avatar={<Image style={{borderRadius: 8, height: 35, width: 35}} source={{uri: guest.pictureUrl}}/>}
                    onClose={() => removeUser(guest)}
                    closeIcon="close"
                  >
                    {guest.name}
                  </Chip>
                )
              }
            </View>
          )
        }
      </View>

      <View style={{flexDirection: "row", alignItems: "center", marginBottom: normalize(10)}}>
        <TouchableHighlight
          style={styles.icon}
          onPress={() => {
            navigate("UserSuggestion", {
              userOnClickCallback: setSelectedHost,
              searchUserPlaceholder: i18n.t("prompt_search_for_user"),
              showLocalStorageList: true,
              shouldDismissWhenClicked: true,
            });
          }}
          underlayColor={colors.white}
        >
          <AlarmFill height={normalize(10)} width={normalize(10)} color={colors.primary}/>
        </TouchableHighlight>
        <Text style={styles.labelIcon}>
          {i18n.t("Time")}
        </Text>
      </View>

      <View style={{flexDirection: "row", justifyContent: "space-between", marginTop: 30}}>
        <View>
          <Text style={{fontFamily: "LibreBaskerville_400Regular_Italic", fontSize: normalize(10)}}>
            {i18n.t("hour_from")}
          </Text>
          <Text style={{fontFamily: "Manrope_500Medium", fontSize: normalize(14)}}>
            {hourFrom}
          </Text>
        </View>
        <View>
          <Text style={{fontFamily: "LibreBaskerville_400Regular_Italic", fontSize: normalize(10)}}>
            {i18n.t("hour_to")}
          </Text>
          <Text style={{fontFamily: "Manrope_600SemiBold", fontSize: normalize(14)}}>
            {hourTo}
          </Text>
        </View>
      </View>

      <View style={{marginLeft: 20, marginTop: 20}}>
        <MultiSlider
          selectedStyle={{backgroundColor: colors.available}}
          trackStyle={styles.trackStyle}
          sliderLength={sliderLength}
          step={sliderStep}
          min={sliderMin}
          max={sliderMax}
          customMarker={CustomMarker}
          snapped
          allowOverlap
          values={sliderHours}
          onValuesChange={setSliderHours}
        />
      </View>
    </CustomStackCard>
  );
};

const styles = StyleSheet.create({
  icon: {
    alignItems: "center",
    justifyContent: 'center',
    backgroundColor: "#E9EDEC",
    borderRadius: normalize(8),
    height: normalize(28),
    width: normalize(28)
  },
  labelIcon: {
    marginLeft: normalize(8),
    fontFamily: "Manrope_400Regular",
    fontSize: normalize(9)
  },
  clockIcon: {
    padding: normalize(16),
  },
  accountPlusIcon: {
    justifyContent: "flex-start",
    paddingVertical: 20,
    paddingLeft: normalize(16),
    paddingRight: 16,
  },
  searchBar: {
    zIndex: -1,
    paddingLeft: normalize(8),
    paddingVertical: 8,
    borderBottomWidth: 1,
    elevation: 0,
    borderBottomColor: "#D5D5D5",
  },
  searchBarText: {
    alignSelf: "center",
    marginBottom: 5,
  },
  trackStyle: {
    borderRadius: 10,
    height: 20,
    marginTop: -10,
  },
  sliderContainer: {
    paddingVertical: 8,
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "#D5D5D5",
  },
  guestsContainer: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "#D5D5D5",
  },
  chipContainer: {
    paddingVertical: 12,
    alignSelf: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    flex: 1,
  },
  hoursContainer: {
    marginTop: 16,
    marginLeft: -16,
    marginRight: -16,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  hour: {
    fontSize: 18,
    fontWeight: "600",
  },
  marker: {
    backgroundColor: "white",
    borderRadius: 14,
    borderWidth: 7,
    borderColor: "green",
    width: 40,
    height: 40,
  },
  chip: {
    marginRight: 8,
    marginVertical: 6,
  },
  subject: {
    backgroundColor: "#FFF",
    borderRadius: normalize(8),
    width: "90%",
    height: "100%",
    marginLeft: normalize(8),
    padding: normalize(10),
  }
});

export default SchedulerBookingModal;
