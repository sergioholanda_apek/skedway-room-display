import React, {useContext, useEffect, useState} from "react";
import {StyleSheet, View} from "react-native";
import {Caption} from "react-native-paper";
import i18n from "i18n-js";

import CustomDropdown from "components/common/CustomDropdown";
import RoundedButton from "components/common/buttons/RoundedButton";
import HeaderLeft from "components/HeaderLeft";
import LoginScreenLayout from "components/Login/LoginScreenLayout";

import {LoginFlowContext} from "contexts/LoginFlowContext";

import normalize from "utils/normalize";
import loginStyles from "styles/loginStyles";

const AccountSelection = ({navigation}) => {

  const [email, setEmail] = useState(null);
  const [account, setAccount] = useState(null);

  const {accounts} = useContext(LoginFlowContext);

  useEffect(() => {
    const currentEmail = navigation.getParam("email");
    setEmail(currentEmail);
  }, []);

  const transformAccounts = () => {
    return accounts.map((account) => {
      return {
        name: account.name,
        onPress: () => setAccount(account),
      };
    });
  };

  const handleNext = () => {
    if (account) {
      navigation.navigate("Password", {email, account});
    }
  };

  return (
    <LoginScreenLayout>
      <View style={{top: "32%"}}>
        <Caption style={loginStyles.instructions}>
          {i18n.t("login.account_info")}
        </Caption>
      </View>

      <View style={{top: "35%", paddingLeft: normalize(40), paddingRight: normalize(40)}}>
        <CustomDropdown
          mode="outlined"
          label={i18n.t("login.accounts")}
          value={account && account.name}
          data={transformAccounts()}
        />
      </View>

      <View style={loginStyles.buttonPosition}>
        <RoundedButton
          label={i18n.t("login.next")}
          action={handleNext}
          type="secondary"
          disabled={!account}
        />
      </View>
    </LoginScreenLayout>
  );
};

AccountSelection.navigationOptions = ({navigation}) => {
  return {
    title: `${i18n.t("login.accounts")}`,
    headerLeft: () => <HeaderLeft onIconPress={() => navigation.goBack()}/>,
  };
};

export default AccountSelection;
