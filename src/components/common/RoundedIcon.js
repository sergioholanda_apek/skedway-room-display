import React from "react";
import {StyleSheet, Text, View} from "react-native";

import normalize from "utils/normalize";

const RoundedIcon = ({alignCenter = false, emoji = "👋"}) => {

  const circleStyle = alignCenter ? {...styles.circle, alignSelf: "center" } : styles.circle

  return (
    <View style={circleStyle}>
      <Text style={styles.icon}>{emoji}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  circle: {
    width: normalize(60),
    height: normalize(60),
    borderRadius: normalize(50),
    backgroundColor: "#fff"
  },
  icon: {
    marginTop: normalize(17),
    marginLeft: normalize(17),
    fontSize: normalize(22)
  }
})


export default RoundedIcon