import * as React from "react"
import {SvgXml} from "react-native-svg"

const xml = `
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 295 300" fill="none">
  <mask id="mask0_2_165" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="299" height="324">
    <rect width="298.287" height="323.089" rx="30.5936" fill="#C4C4C4"/>
  </mask>
  <g mask="url(#mask0_2_165)">
    <path d="M144.345 208.882L15.4984 -1.40456L15.4984 -108.592L287.174 -108.592L287.174 208.882L144.345 208.882Z" fill="url(#paint0_linear_2_165)"/>
    <path d="M260.784 208.868L131.937 -1.4193L131.937 -108.607L403.612 -108.607L403.612 208.868L260.784 208.868Z" fill="url(#paint1_linear_2_165)"/>
    <path d="M144.042 632.031L144.042 208.505L396.884 208.504L390.821 632.031L144.042 632.031Z" fill="url(#paint2_linear_2_165)"/>
    <path d="M-59.6616 -1.0436L15.4554 -1.04292L144.146 208.505L144.146 636.189L-59.6614 636.19L-59.6616 -1.0436Z" fill="url(#paint3_linear_2_165)"/>
    <path d="M-59.3971 618.723L-59.397 167.314C-59.397 151.422 -39.4873 144.683 -30.7113 157.604L141.371 410.962C143.188 413.638 144.162 416.834 144.162 420.125L144.162 618.177C144.162 627.233 136.953 634.655 128.158 634.655L-43.9239 634.655C-52.5114 634.655 -59.3971 627.565 -59.3971 618.723Z" fill="url(#paint4_linear_2_165)"/>
  </g>
  <defs>
    <linearGradient id="paint0_linear_2_165" x1="23.4475" y1="110.573" x2="287.174" y2="110.573" gradientUnits="userSpaceOnUse">
      <stop stop-color="#FA9131"/>
      <stop offset="1" stop-color="#E5E4E4"/>
    </linearGradient>
    <linearGradient id="paint1_linear_2_165" x1="139.886" y1="110.559" x2="403.612" y2="110.559" gradientUnits="userSpaceOnUse">
      <stop stop-color="#FA9131"/>
      <stop offset="1" stop-color="#E5E4E4"/>
    </linearGradient>
    <linearGradient id="paint2_linear_2_165" x1="202.604" y1="204.923" x2="169.456" y2="440.023" gradientUnits="userSpaceOnUse">
      <stop stop-color="#DCCCFF"/>
      <stop offset="1" stop-color="white" stop-opacity="0"/>
    </linearGradient>
    <linearGradient id="paint3_linear_2_165" x1="20.3403" y1="-18.7763" x2="-179.978" y2="59.2648" gradientUnits="userSpaceOnUse">
      <stop stop-color="#142C24"/>
      <stop offset="0.933453" stop-color="#298466" stop-opacity="0"/>
    </linearGradient>
    <linearGradient id="paint4_linear_2_165" x1="20.3563" y1="187.788" x2="-190.264" y2="637.643" gradientUnits="userSpaceOnUse">
      <stop stop-color="#142C24"/>
      <stop offset="0.933453" stop-color="#298466" stop-opacity="0"/>
    </linearGradient>
  </defs>
</svg>
`;

export default ({props}) => <SvgXml width="100%" height="100%" xml={xml} />;
