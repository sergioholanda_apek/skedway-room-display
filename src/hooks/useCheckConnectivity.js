import {useEffect, useState} from "react";
import NetInfo from "@react-native-community/netinfo";

let NetInfoSubscription;

export default function useCheckConnectivity() {

  const [isConnected, setConnected] = useState(false);
  const [connectionType, setConnectionType] = useState("");

  useEffect(() => {
    NetInfoSubscription = NetInfo.fetch().then(state => {
      setConnected(state.isConnected);
      setConnectionType(state.type);
    })
  }, [])


  return {isConnected, connectionType};
}
