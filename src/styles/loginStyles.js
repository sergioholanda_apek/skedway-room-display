import {StyleSheet} from "react-native";
import normalize from "utils/normalize";

const loginStyles = StyleSheet.create({
  instructions: {
    fontSize: 15,
    color: "grey",
    alignSelf: "center",
    fontFamily: "Manrope_300Light"
  },
  buttonPosition: {
    position: "absolute",
    alignSelf: "center",
    zIndex: 1,
    top: "50%"
  },
  appLogoContainer: {
    top: "24%",
    alignSelf: "center"
  },
  appLogo: {
    height: normalize(25),
    alignSelf: "center",
    resizeMode: "contain",
    position: 'absolute'
  },
});

export default loginStyles;