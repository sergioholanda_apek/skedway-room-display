import * as React from "react"
import Svg, { Path } from "react-native-svg"
import {useTheme} from "react-native-paper";

const Phone = (props) => {

  const {height, width} = props;
  const {colors} = useTheme();

  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      fill={colors.active}
      viewBox="0 0 22 34"
      width={height}
      height={width}
    >
      <Path d="M11 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h6zM5 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H5z" />
      <Path d="M8 14a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
    </Svg>
  )
}

export default Phone
