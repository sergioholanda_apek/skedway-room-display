import * as React from "react"
import {SvgXml} from "react-native-svg"

const xml = `
<svg width="123" height="123" viewBox="0 0 123 123" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g filter="url(#filter0_d_2_6704)">
  <rect x="21.7205" y="22.487" width="78.5867" height="78.5867" rx="21.5178" fill="white"/>
  <path d="M45.4263 78.7993C45.094 78.7993 44.7753 78.9379 44.5403 79.1846C44.3053 79.4313 44.1733 79.766 44.1733 80.1149C44.1733 80.4638 44.3053 80.7984 44.5403 81.0452C44.7753 81.2919 45.094 81.4305 45.4263 81.4305H78.0037C78.336 81.4305 78.6547 81.2919 78.8897 81.0452C79.1247 80.7984 79.2567 80.4638 79.2567 80.1149C79.2567 79.766 79.1247 79.4313 78.8897 79.1846C78.6547 78.9379 78.336 78.7993 78.0037 78.7993H74.2448V45.9084C74.2448 44.8617 73.8488 43.8578 73.1438 43.1176C72.4389 42.3774 71.4828 41.9616 70.4859 41.9616H69.2329V40.6459C69.2329 40.4576 69.1943 40.2715 69.1199 40.1001C69.0455 39.9288 68.9369 39.7762 68.8015 39.6528C68.666 39.5293 68.507 39.4378 68.335 39.3844C68.163 39.3311 67.9821 39.3171 67.8045 39.3434L50.2628 41.9747C49.9639 42.0191 49.6903 42.1753 49.4923 42.4146C49.2943 42.6539 49.1853 42.9602 49.1852 43.2772V78.7993H45.4263ZM69.2329 44.5928H70.4859C70.8182 44.5928 71.1369 44.7314 71.3718 44.9782C71.6068 45.2249 71.7388 45.5595 71.7388 45.9084V78.7993H69.2329V44.5928ZM62.968 65.6429C62.2764 65.6429 61.715 64.4641 61.715 63.0117C61.715 61.5592 62.2764 60.3804 62.968 60.3804C63.6596 60.3804 64.221 61.5592 64.221 63.0117C64.221 64.4641 63.6596 65.6429 62.968 65.6429Z" fill="#80DDB1"/>
  </g>
  <defs>
  <filter id="filter0_d_2_6704" x="0.0057106" y="0.772266" width="122.016" height="122.016" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
  <feFlood flood-opacity="0" result="BackgroundImageFix"/>
  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
  <feOffset/>
  <feGaussianBlur stdDeviation="10.8574"/>
  <feComposite in2="hardAlpha" operator="out"/>
  <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08 0"/>
  <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_2_6704"/>
  <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_2_6704" result="shape"/>
  </filter>
  </defs>
</svg>
`;

export default (props) => <SvgXml {...props} xml={xml} />;
