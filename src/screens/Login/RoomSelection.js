import React, {useContext, useEffect, useState} from "react";
import {FlatList, StyleSheet, TouchableOpacity, View} from "react-native";
import {ActivityIndicator, Card, Subheading, Title, useTheme,} from "react-native-paper";
import i18n from "i18n-js";
import merge from "lodash/merge";
import isEmpty from "lodash/isEmpty";

import {MaterialCommunityIcons} from "@expo/vector-icons";

import useDebounce from "hooks/useDebounce";

import normalize from "utils/normalize";

import {StorageContext} from "contexts/StorageContext";
import {SyncContext} from "contexts/SyncContext";
import {LoginFlowContext} from "contexts/LoginFlowContext";
import {SchedulingContext} from "contexts/SchedulingContext";

import HeaderLeft from "components/HeaderLeft";
import CustomDropdown from "components/common/CustomDropdown";
import CustomInput from "components/common/CustomInput";
import CustomDialog from "components/common/dialog/CustomDialog";
import SpaceCard from "components/Login/SpaceCard";
import useOrientation from "hooks/useOrientation";

const PAGINATION_LIMIT = 10;

const RoomSelection = ({navigation}) => {

  const {colors, unit} = useTheme();
  const debouncedQuery = useDebounce(query, 300);

  const [sites, setSites] = useState([]);
  const [selectedSite, setSelectedSite] = useState(null);
  const [companyId, setCompanyId] = useState(null);
  const [offset, setOffset] = useState(0);
  const [query, setQuery] = useState("");
  const [dialogOpen, setDialogOpen] = useState(null);

  const {
    fetchSpaces,
    loginSpace,
    roomError,
    space,
    spaces,
    setSpaces,
    listEnd,
    isFetchingSpaces,
    setLogoutSuccessfully
  } = useContext(LoginFlowContext);

  const {sync} = useContext(SyncContext);

  const {appStorage, insertAppStorage} = useContext(StorageContext);

  const {setCurrentDay} = useContext(SchedulingContext);

  const orientation = useOrientation();
  const isPortrait = orientation === "PORTRAIT";

  const cardHeight = isPortrait ? {height: "20%"} : {height: "100%"};

  /*
   * For the initial configuration:
   * Retrieves the 'user' object that comes from navigation to set
   * the initial values and select the first site if there is only one
   */
  useEffect(() => {
    initUserCompanyData();
  }, []);

  useEffect(() => {
    if (space) {
      const {planProducts, ...filteredData} = space;
      const mainAppData = merge(appStorage, filteredData);

      insertAppStorage(mainAppData, () => {
        sync();
        setCurrentDay(new Date());
        setLogoutSuccessfully(false);
        navigation.navigate("Home")
      })
    }
  }, [space]);

  function initUserCompanyData() {
    const userCompany = navigation.getParam("userCompany");

    const {
      id,
      sites,
      checkInAdvance,
      checkInAdvance2,
      noShowMinutes,
      noShowMinutes2,
      checkInRequiresAuthentication,
    } = userCompany;

    setCompanyId(id);
    setSites(sites);
    setSpaces([]);

    if (sites.length === 1) {
      setSelectedSite(sites[0]);
    }

    const company = {
      checkInAdvance,
      checkInAdvance2,
      noShowMinutes,
      noShowMinutes2,
      checkInRequiresAuthentication,
    }

    const merged = merge(appStorage, {company});
    insertAppStorage(merged);
  }

  /*
   * Triggers when there's a site change in the dropdown
   * or by searching in the input
   */
  useEffect(() => {
    // Sets offset to zero to begin search without pagination
    setOffset(0);

    if (companyId) {
      fetchSpaces(companyId, selectedSite, PAGINATION_LIMIT, debouncedQuery, 0);
    }
  }, [selectedSite, debouncedQuery]);

  /*
   * Triggers when there's a pagination change by increasing the offset
   */
  useEffect(() => {
    if (offset) {
      fetchSpaces(companyId, selectedSite, PAGINATION_LIMIT, query, offset);
    }
  }, [offset]);

  const dropdownValue = selectedSite
    ? selectedSite.name
    : i18n.t("Select_the_site");

  const siteSelection = sites.map((site) => ({
    onPress: () => setSelectedSite(site),
    ...site,
  }));

  const handleLoginSpace = (space) => {
    loginSpace(space.id);
  }

  const FlatListPagination = () => {
    if (roomError) {
      return <Subheading>{roomError}</Subheading>;
    }

    if (selectedSite && isEmpty(spaces)) {
      return (
        <View
          style={[
            styles.noSpaceFoundContainer,
            {padding: normalize(unit(2))},
          ]}
        >
          <MaterialCommunityIcons size={30} name="emoticon-sad-outline"/>
          <Title style={{
            fontFamily: 'LibreBaskerville_400Regular_Italic',
            color: colors.primary
          }}>{i18n.t("No_space_found")}</Title>
        </View>
      );
    }

    return (
      <View style={styles.flatListContainer}>
        <FlatList
          keyboardShouldPersistTaps={"handled"}
          data={spaces}
          keyExtractor={(result) => result.id && result.id.toString()}
          onEndReachedThreshold={0.5}
          ListFooterComponent={<View style={{padding: normalize(unit(2))}}/>}
          onEndReached={() => {
            /*
             * Increases the offset when the pagination has not ended yet
             * and the array length is divisible by PAGINATION_LIMIT
             */
            if (!listEnd && spaces.length % PAGINATION_LIMIT === 0) {
              setOffset(offset + PAGINATION_LIMIT);
            }
          }}
          renderItem={({item}) => (
            <TouchableOpacity onPress={() => setDialogOpen({item})}>
              <SpaceCard space={item}/>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  };

  const FlatListWithDialog = () => {
    const item = dialogOpen && dialogOpen.item;
    const itemName = item ? item.name : null;

    return (
      <>
        <CustomDialog
          style={styles.dialogStyle}
          visible={dialogOpen}
          onDismiss={() => setDialogOpen(false)}
          cancelAction={() => setDialogOpen(false)}
          confirmAction={() => handleLoginSpace(item)}
          title={itemName}
          content={i18n.t("confirm_space", {input: itemName})}
        />
        {FlatListPagination()}
      </>
    );
  };

  return (
    <View
      style={{flexDirection: isPortrait ? "column" : "row", height: "100%"}}
    >
      <View style={isPortrait ? {} : {flex: 3}}>
        <Card style={[styles.cardContainer, cardHeight]}>
          <View
            style={{
              marginHorizontal: normalize(unit(2)),
              marginBottom: normalize(unit(1)),
              paddingTop: normalize(26),
            }}
          >
            <View>
              <CustomDropdown
                mode="outlined"
                label="Site"
                value={dropdownValue}
                data={siteSelection}
              />
            </View>

            <View style={{paddingTop: 10}}>
              <CustomInput
                mode="outlined"
                value={query}
                onChangeText={(text) => setQuery(text)}
                label={i18n.t("Space")}
                style={{height: 60}}
              />
            </View>
          </View>
        </Card>
      </View>

      <View style={isPortrait ? {flex: 1, backgroundColor: "white"} : {flex: 6}}>
        {FlatListWithDialog()}
      </View>
    </View>
  );
};

RoomSelection.navigationOptions = ({navigation}) => {
  return {
    title: `${i18n.t("title_activity_space")}`,
    headerLeft: () => <HeaderLeft onIconPress={() => navigation.goBack()}/>,
  };
};

const styles = StyleSheet.create({
  cardContainer: {
    borderRadius: 0,
    shadowColor: "#F2F2F2",
    backgroundColor: "#F2F2F2"
  },
  flatListContainer: {
    flex: 1,
  },
  noSpaceFoundContainer: {
    alignItems: "center",
  },
  dialogStyle: {
    marginHorizontal: "15%",
  },
});

export default RoomSelection;
