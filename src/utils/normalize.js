import { Dimensions, Platform, PixelRatio } from "react-native";

export var { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get(
  "window"
);

export default function normalize(size, based = "width") {
  const portraitWidth = Math.min(SCREEN_WIDTH, SCREEN_HEIGHT);
  const portraitHeight = Math.max(SCREEN_WIDTH, SCREEN_HEIGHT);

  // based on iPhone 8's scale
  const wscale = portraitWidth / 375;
  const hscale = portraitHeight / 667;

  const newSize = based === "height" ? size * hscale : size * wscale;
  if (Platform.OS === "ios") {
    return Math.round(PixelRatio.roundToNearestPixel(newSize));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
  }
}
