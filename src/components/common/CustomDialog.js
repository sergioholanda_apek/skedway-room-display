import React, { useEffect } from "react";
import { StyleSheet, View, KeyboardAvoidingView, Keyboard } from "react-native";
import {
  Dialog,
  Portal,
  Title,
  Button,
  Card,
  useTheme,
} from "react-native-paper";
import i18n from "i18n-js";

const CustomDialog = ({
  visible = false,
  onDismiss = null,
  cancelAction = null,
  confirmAction = null,
  confirmLabel = i18n.t("action_confirm"),
  cancelLabel = i18n.t("action_cancel"),
  dismissable = true,
  title = "",
  Icon = null,
  children,
  ...otherProps
}) => {
  if (!visible) {
    return null;
  }

  useEffect(() => {
    Keyboard.dismiss();
  }, []);

  const { unit } = useTheme();

  return (
    <Portal>
      <KeyboardAvoidingView
        keyboardVerticalOffset={-100}
        behavior="position"
        contentContainerStyle={styles.avoidViewContainer}
        style={styles.avoidViewContainer}
      >
        <Dialog
          visible={visible}
          dismissable={dismissable}
          onDismiss={onDismiss}
          {...otherProps}
        >
          {title ? (
            <Card style={styles.cardContainer}>
              <View
                style={[
                  styles.titleContainer,
                  { paddingHorizontal: unit(3), paddingVertical: unit(2) },
                ]}
              >
                {Icon ? (
                  <View
                    style={[styles.iconContainer, { paddingRight: unit(3) }]}
                  >
                    {Icon}
                  </View>
                ) : null}
                <Title>{title}</Title>
              </View>
            </Card>
          ) : null}
          <Dialog.Content
            style={{ paddingHorizontal: unit(2), paddingVertical: unit(2) }}
          >
            {children}
          </Dialog.Content>
          {cancelAction || confirmAction ? (
            <Dialog.Actions>
              {cancelAction ? (
                <Button onPress={cancelAction}>{cancelLabel}</Button>
              ) : null}
              {confirmAction ? (
                <Button onPress={confirmAction}>{confirmLabel}</Button>
              ) : null}
            </Dialog.Actions>
          ) : null}
        </Dialog>
      </KeyboardAvoidingView>
    </Portal>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    elevation: 3,
  },
  iconContainer: {
    alignSelf: "center",
  },
  titleContainer: {
    flexDirection: "row",
  },
  avoidViewContainer: {
    flex: 1,
    position: "absolute",
    bottom: 0,
    top: 0,
    right: 0,
    left: 0,
  },
});

export default CustomDialog;
