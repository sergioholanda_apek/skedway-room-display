import React from "react";
import {ActivityIndicator, useTheme} from "react-native-paper";
import {View, StyleSheet} from "react-native";

const Loading = ({show, size = "large", transparent = true}) => {

  const {colors} = useTheme();

  return (
    <>
      {
        show &&
        <View style={[styles.container, {backgroundColor: transparent ? '#2c2c2c94' : colors.white}]}>
          <ActivityIndicator
            style={styles.loading}
            size={size}
            animating={true}
            color={transparent ? colors.white : colors.darkGreen}
          />
        </View>
      }
    </>
  )

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 10,
    height: "100%",
    width: "100%",
    justifyContent: 'center',
    alignItems: 'center',
    position: "absolute",
  },
  loading: {
    zIndex: 30,
  },
});

export default Loading;