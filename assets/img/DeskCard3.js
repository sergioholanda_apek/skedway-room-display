import * as React from "react"
import {SvgXml} from "react-native-svg"

const xml = `
<svg width="200" height="1000" viewBox="0 0 3000 1500" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M3207.01 567.176L3210.32 1075.04L1989.74 511.173V0L3207.01 567.176Z" fill="url(#paint0_linear_1519_38344)"/>
<path d="M977.539 471.952V979.114L1989.73 509.403V0L977.539 471.952Z" fill="url(#paint1_linear_1519_38344)"/>
<path d="M1347.09 426.836V623.142L1658.45 473.496V277.189L1347.09 426.836Z" fill="url(#paint2_linear_1519_38344)" fill-opacity="0.67"/>
<path d="M2208.27 2467.74L3223.48 1995.35L3228.97 1078.16L2216.24 1073.55L2208.27 2467.74Z" fill="url(#paint3_linear_1519_38344)"/>
<path d="M977.538 1886.99L2323.3 2515.02L2323.3 1136.61L1516.47 1136.61L977.538 979.099L977.538 1886.99Z" fill="url(#paint4_linear_1519_38344)" fill-opacity="0.81"/>
<path d="M977.54 979.107L1987.1 508.458L3210.32 1075.03L2200.81 1546.98L977.54 979.107Z" fill="url(#paint5_linear_1519_38344)"/>
<rect width="111.202" height="111.155" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 2596.52 1186.55)" fill="url(#paint6_linear_1519_38344)"/>
<rect width="111.202" height="111.155" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 1866.61 828.751)" fill="url(#paint7_linear_1519_38344)"/>
<rect width="111.202" height="111.155" transform="matrix(0.906653 -0.421877 0.906653 0.421877 1963.12 1230.73)" fill="url(#paint8_linear_1519_38344)"/>
<rect width="111.202" height="111.155" transform="matrix(0.906653 -0.421877 0.906653 0.421877 1782.27 1151.22)" fill="url(#paint9_linear_1519_38344)"/>
<rect width="111.202" height="111.155" transform="matrix(0.906653 -0.421877 0.906653 0.421877 1612.37 1071.71)" fill="url(#paint10_linear_1519_38344)"/>
<rect width="115.972" height="115.923" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 2280.82 897.479)" fill="url(#paint11_linear_1519_38344)"/>
<path d="M2495.66 1121.49V1232.02L2599.44 1183.4V1072.88L2495.66 1121.49Z" fill="white"/>
<rect width="115.972" height="115.923" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 2269.85 800.301)" fill="url(#paint12_linear_1519_38344)"/>
<path d="M1665.37 720.179V830.704L1769.16 782.091V671.565L1665.37 720.179Z" fill="white"/>
<rect width="115.972" height="115.923" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 2445.23 870.977)" fill="url(#paint13_linear_1519_38344)"/>
<path d="M1886.78 1086.89V1197.42L1783 1148.8V1038.28L1886.78 1086.89Z" fill="white"/>
<path d="M1713.8 1010.78V1121.31L1610.01 1072.7V962.171L1713.8 1010.78Z" fill="white"/>
<path d="M2066.68 1169.93V1280.45L1962.89 1231.84V1121.31L2066.68 1169.93Z" fill="white"/>
<rect width="115.972" height="115.923" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 2615.12 954.909)" fill="url(#paint14_linear_1519_38344)"/>
<g filter="url(#filter0_d_1519_38344)">
<rect width="455.455" height="713.681" rx="5.81799" transform="matrix(0.906653 -0.421877 0.906653 0.421877 1586.66 911.921)" fill="url(#paint15_linear_1519_38344)"/>
</g>
<path d="M2267.32 692.504V803.029L2163.54 754.416V643.891L2267.32 692.504Z" fill="white"/>
<path d="M2447.22 761.693V872.218L2343.44 823.605V713.08L2447.22 761.693Z" fill="white"/>
<path d="M2613.29 844.726V955.251L2509.5 906.637V796.112L2613.29 844.726Z" fill="white"/>
<defs>
<filter id="filter0_d_1519_38344" x="1568.41" y="721.213" width="1096.49" height="560.171" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="48.4833"/>
<feGaussianBlur stdDeviation="10.6663"/>
<feComposite in2="hardAlpha" operator="out"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1519_38344"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1519_38344" result="shape"/>
</filter>
<linearGradient id="paint0_linear_1519_38344" x1="3952.27" y1="1533.8" x2="973.136" y2="838.579" gradientUnits="userSpaceOnUse">
<stop offset="0.0410842" stop-color="#B4EFD3"/>
<stop offset="1" stop-color="white" stop-opacity="0"/>
</linearGradient>
<linearGradient id="paint1_linear_1519_38344" x1="3314.97" y1="-6.61541" x2="1594.02" y2="2224.12" gradientUnits="userSpaceOnUse">
<stop offset="0.0410842" stop-color="#B4EFD3"/>
<stop offset="1" stop-color="white" stop-opacity="0"/>
</linearGradient>
<linearGradient id="paint2_linear_1519_38344" x1="1347.09" y1="299.654" x2="1629.35" y2="1001.66" gradientUnits="userSpaceOnUse">
<stop offset="0.0410842" stop-color="#B4EFD3"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint3_linear_1519_38344" x1="2041.28" y1="1149.4" x2="2583.2" y2="2195.55" gradientUnits="userSpaceOnUse">
<stop offset="0.0410842" stop-color="#B4EFD3"/>
<stop offset="1" stop-color="white" stop-opacity="0"/>
</linearGradient>
<linearGradient id="paint4_linear_1519_38344" x1="799.103" y1="926.788" x2="1638.36" y2="2149.35" gradientUnits="userSpaceOnUse">
<stop offset="0.0410842" stop-color="#B4EFD3"/>
<stop offset="1" stop-color="white" stop-opacity="0"/>
</linearGradient>
<linearGradient id="paint5_linear_1519_38344" x1="2155.84" y1="1546.1" x2="1383.74" y2="-39.4683" gradientUnits="userSpaceOnUse">
<stop offset="0.0410842" stop-color="#B4EFD3"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint6_linear_1519_38344" x1="113.809" y1="58.2759" x2="35.5964" y2="39.0145" gradientUnits="userSpaceOnUse">
<stop stop-color="white"/>
<stop offset="1" stop-color="#E7F6EF"/>
</linearGradient>
<linearGradient id="paint7_linear_1519_38344" x1="-41.618" y1="127.967" x2="143.355" y2="126.145" gradientUnits="userSpaceOnUse">
<stop stop-color="white"/>
<stop offset="1" stop-color="#E7F6EF"/>
</linearGradient>
<linearGradient id="paint8_linear_1519_38344" x1="113.809" y1="58.2759" x2="35.5964" y2="39.0145" gradientUnits="userSpaceOnUse">
<stop stop-color="white"/>
<stop offset="1" stop-color="#E7F6EF"/>
</linearGradient>
<linearGradient id="paint9_linear_1519_38344" x1="113.809" y1="58.2759" x2="35.5964" y2="39.0145" gradientUnits="userSpaceOnUse">
<stop stop-color="white"/>
<stop offset="1" stop-color="#E7F6EF"/>
</linearGradient>
<linearGradient id="paint10_linear_1519_38344" x1="113.809" y1="58.2759" x2="35.5964" y2="39.0145" gradientUnits="userSpaceOnUse">
<stop stop-color="white"/>
<stop offset="1" stop-color="#E7F6EF"/>
</linearGradient>
<linearGradient id="paint11_linear_1519_38344" x1="25.0017" y1="135.863" x2="17.2477" y2="18.467" gradientUnits="userSpaceOnUse">
<stop stop-color="white"/>
<stop offset="1" stop-color="#E7F6EF"/>
</linearGradient>
<linearGradient id="paint12_linear_1519_38344" x1="25.0017" y1="135.863" x2="17.2477" y2="18.467" gradientUnits="userSpaceOnUse">
<stop stop-color="white"/>
<stop offset="1" stop-color="#E7F6EF"/>
</linearGradient>
<linearGradient id="paint13_linear_1519_38344" x1="25.0017" y1="135.863" x2="17.2477" y2="18.467" gradientUnits="userSpaceOnUse">
<stop stop-color="white"/>
<stop offset="1" stop-color="#E7F6EF"/>
</linearGradient>
<linearGradient id="paint14_linear_1519_38344" x1="25.0017" y1="135.863" x2="17.2477" y2="18.467" gradientUnits="userSpaceOnUse">
<stop stop-color="white"/>
<stop offset="1" stop-color="#E7F6EF"/>
</linearGradient>
<linearGradient id="paint15_linear_1519_38344" x1="98.189" y1="836.447" x2="23.8258" y2="118.239" gradientUnits="userSpaceOnUse">
<stop stop-color="white"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
</defs>
</svg>

`;

export default (props) => <SvgXml {...props} xml={xml} />;
