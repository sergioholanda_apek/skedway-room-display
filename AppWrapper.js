import React, {useContext} from "react";
import {View} from "react-native";

import {createAppContainer, createSwitchNavigator} from "react-navigation";
import {createStackNavigator} from "react-navigation-stack";
import {BottomTabBar, createBottomTabNavigator} from "react-navigation-tabs";
import {createDrawerNavigator} from "react-navigation-drawer";

import {setNavigator} from "utils/navigationRef";
import normalize from "utils/normalize";

import CheckLogin from "screens/Login/CheckLogin";
import Welcome from "screens/Login/Welcome";
import AccountSelection from "screens/Login/AccountSelection";
import Password from "screens/Login/Password";
import RoomSelection from "screens/Login/RoomSelection";

import Home from "screens/Home";
import CalendarScreen from "screens/CalendarScreen";
import MaintenanceScreen from "screens/MaintenanceScreen";
import AboutScreen from "screens/AboutScreen";
import DrawerScreen from "screens/DrawerScreen";
import ScreenSaverScreen from "screens/ScreenSaverScreen";

import IssueReportModal from "components/Issues/IssueReportModal";
import UserSuggestionsModal from "components/User/UserSuggestionsModal";
import SchedulerBookingModal from "components/Home/Booking/SchedulerBookingModal";
import DrawerAboutModal from "components/Drawer/DrawerAboutModal";
import PrivacyPolicyModal from "components/modals/PrivacyPolicyModal";
import CheckinModal from "components/Booking/CheckinModal";
import Loading from "components/Loading";

import {LoadingContext} from "contexts/LoadingContext";

import useOrientation from "hooks/useOrientation";

const loginFlow = createStackNavigator(
  {
    CheckLogin: CheckLogin,
    Welcome: Welcome,
    AccountSelection: AccountSelection,
    Password: Password,
    RoomSelection: RoomSelection
  },
  {
    mode: "modal",
    defaultNavigationOptions: {
      gestureEnabled: false,
    },
  }
);

const TabBarComponent = (props) => {
  const orientation = useOrientation();
  const isPortrait = orientation === "PORTRAIT";

  const portraitTabStyle = isPortrait
    ? {backgroundColor: "white"}
    : {borderTopWidth: 0};

  return (
    <View
      style={{
        position: "absolute",
        left: 0,
        bottom: 0,
        right: 0,
        width: isPortrait ? "100%" : "50%",
      }}
    >
      <BottomTabBar
        {...props}
        style={{...props.style, ...portraitTabStyle}}
      />
    </View>
  );
};

const mainFlow = createBottomTabNavigator(
  {
    Home: Home,
    Calendar: CalendarScreen,
    Maintenance: MaintenanceScreen,
    About: AboutScreen,
  },
  {
    tabBarComponent: (props) => {
      return <TabBarComponent {...props} />;
    },
    lazy: false,
    tabBarOptions: {
      style: {
        height: Math.max(normalize(45), 58),
        borderTopWidth: 0
      },
      tabStyle: {
        flexDirection: "column",
        backgroundColor: "#FDFDFD",
      },
    },
  }
);

const mainFlowWithModals = createStackNavigator(
  {
    mainFlow,
    ScreenSaver: ScreenSaverScreen,
    IssueReport: IssueReportModal,
    UserSuggestion: UserSuggestionsModal,
    Checkin: CheckinModal,
    SchedulerBooking: SchedulerBookingModal,
    DrawerAbout: DrawerAboutModal,
    PrivacyPolicy: PrivacyPolicyModal,
  },
  {
    mode: "modal",
    headerMode: "none",
    defaultNavigationOptions: {
      gestureEnabled: false,
      cardStyle: {
        backgroundColor: "transparent",
      },
    },
  }
);

const mainFlowWithModalsWithDrawer = createDrawerNavigator(
  {mainFlow: mainFlowWithModals},
  {
    edgeWidth: 50,
    contentComponent: () => <DrawerScreen/>,
  },
);

const switchNavigator = createSwitchNavigator({
  loginFlow,
  mainFlowWithModalsWithDrawer,
});

const App = createAppContainer(switchNavigator);

const AppWrapper = () => {

  const {loadingResource} = useContext(LoadingContext);

  return (
    <>
      <Loading show={loadingResource.show} transparent={loadingResource.isTransparent} />
      <App
        ref={(navigator) => {
          setNavigator(navigator);
        }}
      />
    </>
  );
};

export default AppWrapper;