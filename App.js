import React from "react";
import {View} from "react-native";

import i18n from "i18n-js";
import {LocaleConfig} from "react-native-calendars";
import * as Localization from "expo-localization";

import ptDictionary from "i18n/root/pt";
import enDictionary from "i18n/root/en";
import esDictionary from "i18n/root/es";

import ptCalendar from "i18n/calendar/calendar_pt";
import enCalendar from "i18n/calendar/calendar_en";
import esCalendar from "i18n/calendar/calendar_es";

import {DefaultTheme, Provider as PaperProvider} from "react-native-paper";

import IssueProvider from "contexts/IssueContext";
import LoginFlowProvider from "contexts/LoginFlowContext";
import SchedulingProvider from "contexts/SchedulingContext";
import ScreenSaverProvider from "contexts/ScreenSaverContext";
import SnackbarProvider from "contexts/SnackbarContext";
import SyncCheckInsProvider from "contexts/SyncCheckInsContext";
import SyncProvider from "contexts/SyncContext";
import StorageProvider from "contexts/StorageContext";
import UserSuggestionProvider from "contexts/UserSuggestionContext";
import LoadingProvider from "contexts/LoadingContext";

import ScreenSaver from "components/ScreenSaver/ScreenSaverController";
import SnackbarNotifier from "components/common/SnackbarNotifier";
import BackgroundSync from "components/BackgroundSync";

import {useFonts} from "expo-font";
import {Manrope_300Light, Manrope_400Regular, Manrope_500Medium, Manrope_600SemiBold} from "@expo-google-fonts/manrope";
import {LibreBaskerville_400Regular_Italic,} from "@expo-google-fonts/libre-baskerville";
import AppWrapper from "./AppWrapper";

// Set the key-value pairs for the different languages you want to support.
i18n.translations = {
  en: enDictionary,
  pt: ptDictionary,
  es: esDictionary,
};
// Set the locale once at the beginning of your app.

i18n.locale = Localization.locale;
// When a value is missing from a language it'll fallback to another language with the key present.
i18n.fallbacks = true;

LocaleConfig.locales = {
  en: enCalendar,
  pt: ptCalendar,
  es: esCalendar,
};

// We have to remove the region from the locale e.g., 'pt-BR' turns into 'pt'
LocaleConfig.defaultLocale = Localization.locale.split("-")[0] || "pt";

const theme = {
  ...DefaultTheme,
  roundness: 10,
  colors: {
    ...DefaultTheme.colors,
    primary: "#1E2B2D",
    accent: "#FFFFFF",
    background: "#F7F7F7",
    surface: "#F2F2F2",
    text: "#1D3731",
    disabled: "#93A3A1",
    placeholder: "#93A3A1",
    active: "#717171",
    inactive: "#BCC6C6",
    intermediate: "#FA9233",
    intermediateDisabled: "#FFCD9F",
    error: "#B9262E",
    darkRed: "#561416",
    available: "#80DDB1",
    warning: "#FB4E31",
    white: "#FFFFFF",
    black: "#000000",
    tintColor: "#8e8e93",
    onSurface: "#2E2F2D",
    darkGreen: "#66AE8C",
    yellow: "#FFCC00",
    lightYellow: "#F6EECF",
    lightPink: "#F9E4E2",
    red: "#FB4E31",
    iconBackground: "#E9EDEC",
    lightRed: "#FFD5CE",
    lightGreen: "#e6f8ef"
  },
  unit: (factor) => 8 * factor,
};

export default () => {

  let [fontsLoaded] = useFonts({
    Manrope_300Light, Manrope_400Regular, Manrope_500Medium, Manrope_600SemiBold, LibreBaskerville_400Regular_Italic
  })

  if (!fontsLoaded) {
    return <View/>
  }

  return (
    <LoadingProvider>
      <IssueProvider>
        <SchedulingProvider>
          <ScreenSaverProvider>
            <LoginFlowProvider>
              <SnackbarProvider>
                <PaperProvider theme={theme}>
                  <StorageProvider>
                    <SyncCheckInsProvider>
                      <SyncProvider>
                        <UserSuggestionProvider>
                          <SnackbarNotifier/>
                          <BackgroundSync/>
                          <ScreenSaver>
                            <AppWrapper/>
                          </ScreenSaver>
                        </UserSuggestionProvider>
                      </SyncProvider>
                    </SyncCheckInsProvider>
                  </StorageProvider>
                </PaperProvider>
              </SnackbarProvider>
            </LoginFlowProvider>
          </ScreenSaverProvider>
        </SchedulingProvider>
    </IssueProvider>
    </LoadingProvider>
  );
};

