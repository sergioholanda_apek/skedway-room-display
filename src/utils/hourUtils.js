export function getHourFormat(isHourFormat24) {
  const HOUR_FORMAT_24 = "HH:mm";
  const HOUR_FORMAT_12 = "h:mm a";
  return isHourFormat24 ? HOUR_FORMAT_24 : HOUR_FORMAT_12;
}
