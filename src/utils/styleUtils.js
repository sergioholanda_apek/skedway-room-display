import normalize from "utils/normalize";

export function styledText(fontFamily, size) {
  return (
    {fontFamily, fontSize: normalize(size)}
  )
}