import React from "react";
import uuid from "react-native-uuid";
import { addMinutes, format, getMinutes, parse } from "date-fns";

import EventSlot from "./EventSlot";
import { MIN_HEIGHT_PROPORTION, ONE_MINUTE } from "utils/timelineBuilder";
import { datesDifference, getHour, isHour } from "utils/dateUtils";

const EmptyTimeline = (props) => {

  const {
    openingTime,
    closingTime,
    currentDay,
    todayTextTitle,
    locale,
    timezone,
    hourFormat,
  } = props;

  const getEventSlots = () => {
    let iterations = Math.ceil(
      datesDifference(openingTime, closingTime) / ONE_MINUTE
    );
    return iterations <= 0 ? [] : Array(iterations).fill(0);
  };

  const getProps = (index) => {
    const first = index === 0;

    const hour = isHour(openingTime)
      ? openingTime
      : getHour({ date: openingTime, locale, options: { timezone } });

    const parsedHour = parse(hour, "HH:mm:ss", new Date());

    const minutes = getMinutes(parsedHour);
    const minutesToAdd = ONE_MINUTE * index - minutes;

    const increasedDate = first
      ? parsedHour
      : addMinutes(parsedHour, minutesToAdd);

    const title = format(increasedDate, hourFormat); //increasedHour

    const heightProportion = first
      ? Math.max(1 - minutes / ONE_MINUTE, MIN_HEIGHT_PROPORTION)
      : Math.max(minutes / ONE_MINUTE, 1);

    return {
      title,
      heightProportion,
      openingTime,
      closingTime,
      currentDay,
      todayTextTitle,
    };
  };

  return (
    <>
      {getEventSlots().map((_, index) => (
        <EventSlot key={uuid.v4()} {...getProps(index)} />
      ))}
    </>
  );
};

export default EmptyTimeline;
