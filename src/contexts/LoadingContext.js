import React, {useState} from "react";

export const LoadingContext = React.createContext({});

const LoadingProvider = ({children}) => {

  const [loadingResource, setLoadingResource] = useState({
    show: false,
    isTransparent: true
  });

  return (
    <LoadingContext.Provider value={{loadingResource, setLoadingResource}}>
      {children}
    </LoadingContext.Provider>
  );
};

export default LoadingProvider;