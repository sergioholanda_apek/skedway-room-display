import React, {useContext, useEffect, useState} from "react";

import {Image, StyleSheet, TouchableOpacity, View} from "react-native";
import {ActivityIndicator, Caption, useTheme} from "react-native-paper";
import i18n from "i18n-js";
import size from "lodash/size";
import head from "lodash/head";
import debounce from "lodash/debounce";

import {StorageContext} from "contexts/StorageContext";
import {LoginFlowContext} from "contexts/LoginFlowContext";

import CustomInput from "components/common/CustomInput";
import CustomDropdown from "components/common/CustomDropdown";
import RoundedButton from "components/common/buttons/RoundedButton";
import RoundedIcon from "components/common/RoundedIcon";
import CustomDialog from "components/common/dialog/CustomDialog";
import WelcomeMessage from "components/Login/WelcomeMessage";

import {setDevelopmentApi, setProductionApi} from "api";
import {DEVELOPMENT, PRODUCTION} from "react-native-dotenv";

import useOrientation from "hooks/useOrientation";
import usePermissionCamera from "hooks/usePermissionCamera";
import usePermissionLocation from "hooks/usePermissionLocation";

import {version} from "utils/versionUtils";
import normalize from "utils/normalize";
import loginStyles from "styles/loginStyles";

import BackgroundLogin from "../../../assets/img/BackgroundLogin";

const MAX_NUMBER_OF_TOUCHES = 5;

const Welcome = ({navigation}) => {

  const {colors} = useTheme();

  const [email, setEmail] = useState("");

  const [showEnvDropdown, setShowEnvDropdown] = useState(false);
  const [touches, setTouches] = useState(0);
  const [showModalError, setShowModalError] = useState(false);

  const camera = usePermissionCamera();
  const location = usePermissionLocation();

  const {appStorage, insertAppStorage} = useContext(StorageContext);

  const {
    isLoading,
    emailError,
    accounts,
    validEmail,
    verifyEmailAndFetchAccounts,
    setInitialState
  } = useContext(LoginFlowContext);

  const environments = [
    {
      name: i18n.t("env_production"),
      onPress: () => {
        setProductionApi();
        insertAppStorage({env: PRODUCTION});
      },
    },
    {
      name: i18n.t("env_development"),
      onPress: () => {
        setDevelopmentApi();
        insertAppStorage({env: DEVELOPMENT});
      },
    },
  ];

  const orientation = useOrientation();
  const isPortrait = orientation === "PORTRAIT";

  useEffect(() => {
    setProductionApi();
    insertAppStorage({env: PRODUCTION});
    camera.ask()
    location.ask()
  }, []);

  useEffect(() => {
    handleNavigation();
  }, [emailError, validEmail]);

  const handleNavigation = () => {
    if (validEmail) {
      if (size(accounts) >= 2) {
        navigation.navigate("AccountSelection", {email});
      } else {
        const defaultAccount = head(accounts);
        navigation.navigate("Password", {email, account: defaultAccount});
      }
    } else if (emailError) {
      setShowModalError(true);
    }
  };

  const getEnvironmentValue = () => {
    const {env = "production"} = appStorage;
    const envLabel = String(`env_${env?.toLowerCase()}`);
    return i18n.t(envLabel);
  }

  const debouncedClearingFunc = debounce(() => {
    setTouches(0);
  }, 1000);

  const handleOnPress = () => {
    setTouches(touches + 1);
    debouncedClearingFunc();
  };

  if (touches === MAX_NUMBER_OF_TOUCHES) {
    setTouches(0);
    setShowEnvDropdown(true);
  }

  const handleNext = () => {
    verifyEmailAndFetchAccounts(email);
  };

  const handleEmail = (value) => {
    setEmail(value);
  };

  const handleCloseModal = () => {
    setShowModalError(false);
    setInitialState();
  }

  const paddingInputs = (top) => {
    const padding = isPortrait ? 50 : 100;
    return {top: top, paddingLeft: normalize(padding), paddingRight: normalize(padding)}
  }

  return (
    <View style={[styles.safeAreaContainer, {backgroundColor: colors.background}]}>
      <View style={{flex: 6, zIndex: 2}}>
        <View style={{top: "20%"}}>
          <RoundedIcon alignCenter={true}/>
        </View>

        <View style={{top: "23%"}}>
          <WelcomeMessage/>
        </View>

        <TouchableOpacity
          activeOpacity={1}
          onPress={() => handleOnPress()}
          style={loginStyles.appLogoContainer}
        >
          <Image
            style={loginStyles.appLogo}
            source={require("../../../assets/skedway_logo.png")}
          />
        </TouchableOpacity>

        <View style={{top: "40%"}}>
          <Caption style={[loginStyles.instructions, {color: colors.placeholder}]}>
            {i18n.t("login.instructions")}
          </Caption>
        </View>

        {showEnvDropdown && (
          <View style={paddingInputs("41%")}>
            <CustomDropdown
              mode="outlined"
              label={i18n.t("prompt_environment")}
              value={getEnvironmentValue()}
              data={environments}
            />
          </View>
        )}

        <View style={paddingInputs("42%")}>
          <CustomInput
            label={i18n.t("login.email")}
            value={email}
            onChangeText={handleEmail}
            autoCapitalize="none"
            mode="outlined"
            autoCorrect={false}
            bordered={true}
          />
        </View>
      </View>

      <View style={[styles.buttonPosition, {top: isPortrait ? "50%" : "70%", zIndex: 10}]}>
        <RoundedButton
          type="secondary"
          label={i18n.t("login.next")}
          loading={isLoading}
          action={handleNext}
        />
      </View>

      <View style={{flex: isPortrait ? 5 : 2}}>
        <BackgroundLogin
          width={isPortrait ? normalize(375) : normalize(500)}
          height={isPortrait ? normalize(385) : normalize(515)}
        />
      </View>

      <CustomDialog
        style={{marginHorizontal: "15%"}}
        visible={showModalError}
        onDismiss={() => handleCloseModal()}
        confirmAction={() => handleCloseModal()}
        confirmLabel="Ok"
        title={i18n.t("login.errorDialog")}
        content={emailError}
      />
    </View>
  );
};

Welcome.navigationOptions = () => {
  return {
    title: `${i18n.t("app_name")} - ${i18n.t("Version_x", {input: version})}`,
    headerLeft: () => <></>,
  };
};

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    flexDirection: "column",
    marginTop: 0,
    position: "relative",
  },
  buttonPosition: {
    position: "absolute",
    alignSelf: "center",
    zIndex: 1
  }
});

export default Welcome;
