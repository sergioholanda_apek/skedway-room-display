import React, {useState, useContext, useEffect} from "react";
import {View, StyleSheet} from "react-native";
import {SafeAreaView} from "react-navigation";
import {Searchbar, HelperText} from "react-native-paper";
import {MaterialIcons} from "@expo/vector-icons";
import i18n from "i18n-js";

import {navigate} from "utils/navigationRef";
import normalize from "utils/normalize";

import {SchedulingContext} from "contexts/SchedulingContext";

import BookingTitle from "./BookingTitle";
import BookingErrorDialog from "components/Home/Booking/BookingErrorDialog";

const BookingInputs = (props) => {

  const {
    title,
    buttonString,
    onDismiss,
    onUserSelected,
    buttonIsLoading,
    pinAction,
    validationAction,
    children,
  } = props;

  const [selectedUser, setSelectedUser] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);
  const [errorRequiredField, setErrorRequiredField] = useState(null);
  const [showErrorMessage, setShowErrorMessage] = useState(false);

  const {bookingError, setBookingError} = useContext(SchedulingContext);

  useEffect(() => {
    if (onUserSelected) {
      onUserSelected(selectedUser);
    }
  }, [selectedUser]);

  useEffect(() => {
    if (bookingError) {
      setErrorMessage(bookingError);
      setShowErrorMessage(true);
    }
  }, [bookingError]);

  useEffect(() => {
    return () => {
      cleanUp();
    };
  }, []);

  const cleanUp = () => {
    setErrorMessage(null);
    setBookingError(null);
  };

  const getBookingTitleProps = () => ({
    title,
    buttonString,
    buttonIsLoading,
    onDismiss,
    validationAction,
    selectedUser,
    setErrorRequiredField,
    cleanUp,
    pinAction,
  });

  const clearErrorMessage = () => {
    setErrorMessage(null)
    setShowErrorMessage(false)
  }

  const getErrorMessageProps = () => ({
    showErrorMessage, clearErrorMessage, errorMessage
  });

  const renderErrorRequiredField = () => {
    return !selectedUser && errorRequiredField ? (
      <>
        <MaterialIcons name="error" style={styles.warningIcon}/>
        <View style={styles.errorContainer}>
          <View style={styles.arrowUp}/>
          <HelperText style={styles.helperText} visible={errorRequiredField}>
            {errorRequiredField}
          </HelperText>
        </View>
      </>
    ) : null;
  };

  const renderContent = () => (
    <View>
      <Searchbar
        style={styles.searchBar}
        icon="account"
        iconColor="black"
        placeholder={i18n.t("prompt_user")}
        clearButtonMode="never"
        clearIcon={() => null}
        value={(selectedUser && selectedUser.name) || ""}
        onFocus={() => {
          navigate("UserSuggestion", {
            userOnClickCallback: setSelectedUser,
            searchUserPlaceholder: i18n.t("prompt_search_for_user"),
            showLocalStorageList: true,
            shouldDissmissWhenClicked: true,
          });
        }}
      />
      {renderErrorRequiredField()}
    </View>
  );

  return (
    <View>
      <BookingTitle {...getBookingTitleProps()} />
      {renderContent()}
      {children}
      <BookingErrorDialog {...getErrorMessageProps()} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    height: "100%",
    overflow: "hidden",
  },
  searchBar: {
    zIndex: -1,
    paddingHorizontal: normalize(8),
    paddingVertical: 8,
    borderWidth: 1,
    elevation: 0,
    borderColor: "#D5D5D5",
  },
  helperText: {
    color: "white",
    margin: 3,
    backgroundColor: "black",
  },
  warningIcon: {
    position: "absolute",
    right: normalize(16),
    top: 16,
    fontSize: 26,
    color: "red",
  },
  errorContainer: {
    position: "absolute",
    right: normalize(13),
    top: 48,
    maxWidth: 300,
    color: "white",
    backgroundColor: "black",
    borderTopWidth: 3,
    borderTopColor: "red",
  },
  arrowUp: {
    borderLeftWidth: 8,
    borderRightWidth: 8,
    borderBottomWidth: 10,
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: "red",
    right: 10,
    top: -10,
    position: "absolute",
  },
});

export default BookingInputs;
