export default {
  free_now: "Livre agora",
  all_day_free: "Todo o dia livre",
  awaiting_check_in: "Aguardando Check-in",
  happening_now: "Acontecendo agora",
  next_meeting: "Próxima reunião",
  no_reservation_today: "Nenhuma reserva hoje",
  availability_of: "Disponível em %{0}",
  hour: "hora",
  hours: {
    "one": "hora e",
    "other": "horas e"
  },
  minutes: {
    "one": "minuto",
    "other": "minutos"
  },
  time_interval: "%{0} a %{1}",
  free_until: "até %{0}",
  countdown_to: "Contagem regressiva para",
  check_in: "check-in"
}