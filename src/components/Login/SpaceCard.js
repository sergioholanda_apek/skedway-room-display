import React from "react";
import {Image, StyleSheet, View} from "react-native";
import {Caption, Divider, List, Paragraph, useTheme} from "react-native-paper";
import i18n from "i18n-js";

import DeskCard from "../../../assets/img/DeskCard";
import DeskIcon from "../../../assets/img/DeskIcon";
import RoomCard from "../../../assets/img/RoomCard";
import RoomIcon from "../../../assets/img/RoomIcon";

import normalize from "utils/normalize";

const SpaceCard = ({space: {pictureUrl, name, capacity, typeName, floorName, baseType}}) => {

  const {colors} = useTheme();

  return (
    <View style={{
      backgroundColor: "white",
      paddingTop: normalize(8),
      paddingRight: normalize(8),
      paddingLeft: normalize(8)
    }}>
      <List.Item
        style={[styles.listContainer, {backgroundColor: colors.background}]}
        titleStyle={styles.titleStyle}
        title={name}
        description={({color}) => (
          <View>
            <Divider style={{marginLeft: normalize(10), marginRight: normalize(20), top: normalize(15)}}/>
            <View style={{
              paddingLeft: normalize(10),
              paddingRight: normalize(20),
              top: normalize(20),
              flexDirection: "row",
              justifyContent: "space-between"
            }}>
              <Paragraph style={[{color}, {fontSize: normalize(8)}]}>
                {capacity > 1
                  ? i18n.t("other", {input: capacity})
                  : capacity === 1
                    ? i18n.t("one", {input: capacity})
                    : ""}
              </Paragraph>
              <Caption style={{fontSize: normalize(8)}}>{typeName}</Caption>
            </View>
          </View>
        )}
        left={() => (
          <View>
            <View style={{position: "absolute", top: "20%", zIndex: 1}}>
              {
                baseType === 1 ? (
                  <DeskIcon height={normalize(50)}/>
                ) : (
                  <RoomIcon height={normalize(50)}/>
                )
              }
            </View>
            {
              pictureUrl ? (
                <View style={{height: normalize(90), width: normalize(160)}}>
                  <Image
                    style={styles.image}
                    source={{uri: pictureUrl}}
                  />
                </View>
              ) : (
                <View style={{height: normalize(90), width: normalize(160)}}>
                  {
                    baseType === 1 ? (
                      <DeskCard height="100%" width="181%"/>
                    ) : (
                      <RoomCard height="100%" width="156%"/>
                    )
                  }
                </View>
              )
            }
          </View>
        )}
        right={() => (
          <View style={styles.rightContainer}>
            <Caption style={styles.floorCaption}>{floorName}</Caption>
          </View>
        )}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  listContainer: {
    borderRadius: 12,
    padding: 0,
    height: normalize(90),
  },
  titleStyle: {
    bottom: normalize(14),
    left: normalize(10),
    fontSize: normalize(10),
    fontFamily: 'LibreBaskerville_400Regular_Italic'
  },
  rightContainer: {
    position: "absolute",
    right: 0,
    bottom: 0,
  },
  floorCaption: {
    marginBottom: 12,
    marginTop: "auto",
    marginRight: normalize(16),
  },
  image: {
    height: "100%",
    width: "100%",
    borderBottomLeftRadius: 12,
    borderTopLeftRadius: 12,
    padding: 0,
    resizeMode: "cover",
  },
});

export default SpaceCard;
