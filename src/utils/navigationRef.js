import { NavigationActions } from "react-navigation";
import { DrawerActions } from "react-navigation-drawer";

let navigator;

export const setNavigator = (nav) => {
  navigator = nav;
};

export const closeDrawer = () => {
  navigator.dispatch(DrawerActions.closeDrawer());
};

export const openDrawer = () => {
  navigator.dispatch(DrawerActions.openDrawer());
};

export const navigate = (routeName, params) => {
  navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
};
