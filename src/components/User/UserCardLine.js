import React, {useState} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {useTheme} from "react-native-paper";

import normalize from "utils/normalize";

const UserCardLine = (props) => {

  const {
    email = "",
    name = "",
    pictureUrl,
    handleSelectUser,
    clickable = true,
  } = props;

  const {colors} = useTheme();

  // To prevent double click insertions
  const [clicked, setClicked] = useState(false);

  const Container = clickable ? TouchableOpacity : View;

  return (
    <Container
      onPress={() => {
        if (!clicked) handleSelectUser()
        setClicked(true);
      }}
      style={{
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        marginBottom: 20,
        paddingLeft: 3,
        paddingRight: 3
      }}
    >
      <View style={{flexDirection: "row", alignItems: "center"}}>
        {pictureUrl ? (
          <Image
            style={{
              height: 60,
              width: 60,
              borderRadius: normalize(8),
              padding: 0,
              resizeMode: "cover",
            }}
            size={50}
            source={{uri: pictureUrl}}
          />
        ) : null}
        <View style={styles.userInformationContainer}>
          <Text style={{fontFamily: "Manrope_500Medium", fontSize: normalize(9), color: colors.text}}>
            {name}
          </Text>
          <Text style={{fontFamily: "Manrope_500Medium", fontSize: normalize(7), color: colors.disabled}}>
            {email}
          </Text>
        </View>
      </View>
    </Container>
  );
};

const styles = StyleSheet.create({
  deleteIcon: {
    alignSelf: "center",
  },
  userCardContainer: {
    paddingHorizontal: normalize(16),
    paddingVertical: Math.max(normalize(8), 16),
    borderBottomColor: "#D5D5D5",
    flexDirection: "row",
  },
  userInformationContainer: {
    flex: 1,
    paddingLeft: 16,
  },
  borderBottom: {
    borderBottomWidth: 1,
  },
});

export default UserCardLine;
