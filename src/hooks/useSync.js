import { useState, useEffect } from "react";

export default function useSync(callback) {
  const [date, setDate] = useState(new Date());
  const timestamp = date.getTime();

  useEffect(() => {
    if (callback) {
      callback();
    }
  }, []);

  useEffect(() => {
    const handler = setInterval(() => {
      const newDate = new Date();
      if (newDate.getMinutes() !== date.getMinutes()) {
        setDate(newDate);
        if (callback) {
          callback();
        }
      }
    }, 1000);

    return () => {
      clearTimeout(handler);
    };
  }, [timestamp]);

  return date;
}
