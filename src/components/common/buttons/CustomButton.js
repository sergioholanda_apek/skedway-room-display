import React, {useContext} from "react";
import {Button, useTheme} from "react-native-paper";
import {StyleSheet, View} from "react-native";

import normalize from "utils/normalize";

const CustomButton = ({onPress, label, type = "primary", style, labelStyle, isFetching}) => {
  const {colors} = useTheme();

  function getButtonColor() {
    switch (type) {
      case "primary":
        return colors.accent;
      case "secondary":
        return colors.primary;
      default:
        return colors.accent;
    }
  }

  function getLabelColor() {
    switch (type) {
      case "primary":
        return colors.primary;
      case "secondary":
        return colors.accent;
      default:
        return colors.primary;
    }
  }

  return (
    <View>
      <Button
        style={[style, styles.button, {backgroundColor: getButtonColor()}]}
        labelStyle={[labelStyle, styles.label, {color: getLabelColor()}]}
        mode={type === "primary" && "outlined"}
        loading={isFetching}
        uppercase={false}
        onPress={() => onPress()}
      >
        {label}
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    width: normalize(80),
  },
  label: {
    fontFamily: "Manrope_600SemiBold",
  },
});


export default CustomButton;
