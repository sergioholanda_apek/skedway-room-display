import i18n from "i18n-js";

const mapDayNumberToString = {
  0: "day_of_week_sun",
  1: "day_of_week_mon",
  2: "day_of_week_tue",
  3: "day_of_week_wed",
  4: "day_of_week_thu",
  5: "day_of_week_fri",
  6: "day_of_week_sat",
};

const mapPeriodCodesToString = {
  1: "day_for_meeting_room",
  2: "week_for_meeting_room",
  3: "month_for_meeting_room",
  4: "day_for_desk",
  5: "week_for_desk",
  6: "month_for_desk",
};

export default function errorHandler({ errorCode, errorValue }) {
  if (!errorCode || !errorValue) {
    return i18n.t("error_http_500");
  }

  if (
    errorCode === "user_time_exceeded" ||
    errorCode === "error_space_time_exceeded"
  ) {
    const [hourLimit, periodCode] = errorValue;
    const hourString = hourLimit === 1 ? "booked_hour" : "booked_hours";
    const translatedHourLimit = i18n.t(hourString, { 0: hourLimit });
    const translatedPeriod = i18n.t(mapPeriodCodesToString[periodCode]);
    return i18n.t(errorCode, {
      0: translatedHourLimit,
      1: translatedPeriod,
    });
  }

  if (
    errorCode === "error_user_weekday_denied" ||
    errorCode === "error_space_weekday_denied"
  ) {
    const [name, weekDay] = errorValue;
    const weekDayTranslation = i18n.t(mapDayNumberToString[weekDay]);
    return i18n.t(errorCode, { 0: name, 1: weekDayTranslation });
  }

  if (errorCode === "error_time_interval_hour") {
    let [operation, space, weekDay, time_interval] = errorValue;
    //TODO backend needs refactoring this piece of response code
    time_interval = time_interval.replaceAll('<', '').replaceAll('>', '').replaceAll('strong', '').replace('/', '')
    const { name } = space;
    const weekDayTranslation = i18n.t(mapDayNumberToString[weekDay]);
    if (operation === 1) {
      if (time_interval) {
        return i18n.t("error_time_interval_hour_op_allowed_with_text",
          { 0: name, 1: weekDayTranslation, 2: time_interval });
      } else {
        return i18n.t("error_time_interval_hour_op_allowed_no_text", { 0: name, 1: weekDayTranslation });
      }
    } else if (operation === 2) {
      return i18n.t("error_time_interval_hour_op_denied", { 0: name, 1: weekDayTranslation, 2: time_interval });
    }
  }

  if (Array.isArray(errorValue)) {
    const [firstArg, secondArg] = errorValue;
    return i18n.t(errorCode, { 0: firstArg, 1: secondArg });
  }

  return i18n.t(errorCode, { 0: errorValue });
}
