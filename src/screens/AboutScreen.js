import React, {useContext, useEffect, useState} from "react";
import {Image, ScrollView, StyleSheet, Text, View} from "react-native";
import {useTheme,} from "react-native-paper";

import uuid from "react-native-uuid";
import i18n from "i18n-js";
import {isEmpty} from "lodash";

import {SchedulingContext} from "contexts/SchedulingContext";
import {StorageContext} from "contexts/StorageContext";

import useOrientation from "hooks/useOrientation";

import normalize from "utils/normalize";

import SchedulerHeader from "components/Header/SchedulerHeader";
import MainLayout from "components/common/layout/MainLayout";

import Info from "../../assets/icons/Info";
import DeskIcon from "../../assets/img/DeskIcon";
import RoomIcon from "../../assets/img/RoomIcon";
import GeoAltFill from "../../assets/icons/GeoAltFill";
import Telephone from "../../assets/icons/Telephone";
import Display from "../../assets/icons/Display";
import Key from "../../assets/icons/Key";
import ExclamationCircle from "../../assets/icons/ExclamationCircle";

import DeskCard from "../../assets/img/DeskCard";
import RoomCard from "../../assets/img/RoomCard";

const AboutScreen = ({navigation}) => {

  const {setSelectedSchedule} = useContext(SchedulingContext);

  const {appStorage} = useContext(StorageContext);

  const {space, amenities} = appStorage;

  const [spaceAmenities, setSpaceAmenities] = useState([]);

  const orientation = useOrientation();
  const isPortrait = orientation === "PORTRAIT";

  useEffect(() => {
    if (!isEmpty(amenities)) {
      setSpaceAmenities(amenities)
    }
  }, [amenities]);

  useEffect(() => {
    console.log("[About]")
    const listener = navigation.addListener("didFocus", () => {
      setSelectedSchedule(null);
    });

    return () => {
      listener.remove();
    };
  }, []);

  useEffect(() => {
    navigation.setParams({isPortrait});
    navigation.navigate("Home");
  }, [isPortrait]);

  const {unit, colors} = useTheme();

  const pictureUrl = space?.pictureUrl;
  const capacity = space?.capacity;
  const phone = space?.phone;
  const level = space?.level;
  const floorName = space?.floorName;
  const baseType = space?.baseType;

  /*
   * level 1 - Usuário
   * level 2 - Gerente
   * level 3 - Diretor
   * level 4 - Presidente
   * level 99 - Não agendável
   */
  const levels = [
    {id: 1, label: "level_user_can"},
    {id: 2, label: "level_manager_can"},
    {id: 3, label: "level_director_can"},
    {id: 4, label: "level_president_can"},
    {id: 99, label: "level_not_schedulable"},
  ];

  const levelString = () => {
    const level_ = levels.find(item => item.id === level);
    return i18n.t(level_?.label);
  };

  const bookingLevelString = levelString();

  const mainContainerStyle = isPortrait
    ? {display: "flex", flex: 1, flexDirection: "column"}
    : {display: "flex", flex: 1, flexDirection: "row"};

  const headerContainerStyle = isPortrait
    ? {}
    : {height: "100%", width: "50%"};

  const contentContainerStyle = {
    flex: 1,
    width: isPortrait ? "100%" : "50%",
    marginBottom: isPortrait ? Math.max(normalize(38), 50) : 0,
  };

  function getCapacity() {
    return capacity && capacity > 1 ? i18n.t("other", {input: capacity}) : i18n.t("one", {input: capacity})
  }

  const LineInformation = (text, icon) => {
    return (
      <View style={{flexDirection: "row", marginTop: normalize(10)}}>
        <View
          style={[
            styles.iconLineInformation,
            {
              backgroundColor: "#E9EDEC",
              borderRadius: normalize(8),
              height: normalize(28),
              width: normalize(28)
            }
          ]}
        >
          {icon}
        </View>
        <Text style={[styles.textLineInformation, {fontSize: normalize(9), color: colors.text}]}>
          {text}
        </Text>
      </View>
    )
  }

  return (
    <MainLayout>
      <SchedulerHeader showStatusIcon={false}/>
      <View style={[styles.mainContent, {backgroundColor: colors.background}]}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View>
            {
              pictureUrl ? (
                <Image
                  style={styles.imageBackground}
                  source={{uri: pictureUrl}}
                />
              ) : (
                <View>
                  {
                    baseType === 1 ? (
                      <DeskCard height={100} width={100}/>
                    ) : (
                      <RoomCard/>
                    )
                  }
                </View>
              )
            }

            <View style={{position: "absolute", top: "30%", zIndex: 1}}>
              {
                baseType === 1 ? (
                  <DeskIcon height={normalize(50)}/>
                ) : (
                  <RoomIcon height={normalize(50)}/>
                )
              }
            </View>
          </View>

          <View style={{flexDirection: "row", marginTop: normalize(10)}}>
            <View style={{flexDirection: "column", marginTop: normalize(10)}}>
              {
                floorName && LineInformation(floorName, <GeoAltFill height={normalize(14)} width={normalize(14)}/>)
              }

              {
                capacity && LineInformation(getCapacity(), <Display height={normalize(13)}
                                                                    width={normalize(13)} color={colors.primary}/>)
              }

              {
                phone && LineInformation(phone, <Telephone height={normalize(13)}
                                                           width={normalize(13)} color={colors.primary}/>)
              }

              {
                bookingLevelString &&
                LineInformation(bookingLevelString, <Key height={normalize(13)} width={normalize(13)}
                                                         color={colors.primary}/>)
              }
            </View>
            <View style={{flexDirection: "column", marginTop: normalize(10), marginLeft: normalize(60)}}>
              {
                !isEmpty(spaceAmenities) &&
                <View style={{paddingBottom: 20}}>
                  {LineInformation(i18n.t("prompt_amenities"),
                    <ExclamationCircle height={normalize(10)} width={normalize(10)} color={colors.primary}/>)}
                </View>
              }
              {
                spaceAmenities.map(amenity =>
                  <Text key={uuid.v4()} style={{
                    fontFamily: "Manrope_400Regular",
                    color: colors.disabled,
                    fontSize: normalize(7),
                    marginTop: 8,
                    marginLeft: 20
                  }}>
                    {`● ${amenity?.amenityName}`}
                  </Text>
                )
              }
            </View>
          </View>
        </ScrollView>
      </View>
    </MainLayout>
  );
};

AboutScreen.navigationOptions = ({navigation}) => {
  const isPortrait = navigation.getParam("isPortrait");

  return {
    tabBarLabel: ({focused}) => {
      return (
        <Text
          style={{
            color: focused ? "#717171" : "#BCC6C6",
            fontFamily: "Manrope_400Regular",
            fontSize: Math.max(normalize(7), 11),
            textAlign: "center"
          }}
        >
          {i18n.t("title_activity_about")}
        </Text>
      );
    },
    tabBarIcon: ({focused}) => {
      return (
        <Info height="140%" width="140%" focused={focused}/>
      );
    },
  };
};

const styles = StyleSheet.create({
  mainContent: {
    flex: 0.8,
    borderRadius: normalize(10),
    padding: normalize(10),
    marginTop: normalize(10)
  },
  iconLineInformation: {
    padding: 10,
    alignItems: "center",
    flexDirection: 'row',
    justifyContent: 'center'
  },
  textLineInformation: {
    alignSelf: "center",
    fontFamily: "Manrope_400Regular",
    paddingLeft: 20
  },
  imageBackground: {
    width: "100%",
    height: Math.max(normalize(120), 190),
    borderRadius: 10,
  },
  row: {
    flexDirection: "row",
  },
  divider: {
    width: 1,
  },
  flex1: {
    flex: 1,
  },
  infoContainer: {
    paddingVertical: normalize(16),
  },
  leftContainer: {
    paddingHorizontal: normalize(16),
  },
  rightContainer: {
    paddingHorizontal: normalize(16),
  },
});

export default AboutScreen;
