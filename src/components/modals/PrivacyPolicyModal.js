import React, {useState} from "react";
import {View, StyleSheet} from "react-native";
import {Card, Title, Caption, useTheme} from "react-native-paper";
import {SafeAreaView} from "react-navigation";
import {WebView} from "react-native-webview";

import {MaterialCommunityIcons} from "@expo/vector-icons";

import {getApi} from "api";

import normalize from "utils/normalize";

const PrivacyPolicy = ({navigation}) => {
  const {unit} = useTheme();

  const [title, setTitle] = useState("");
  const [urlText, setUrlText] = useState("");

  const uri = `${getApi().url}/privacy.php`;

  return (
    <SafeAreaView forceInset={{top: "always"}} style={styles.container}>
      <Card style={styles.cardContainer}>
        <View style={styles.titleContainer}>
          <MaterialCommunityIcons
            style={styles.closeIcon}
            onPress={() => navigation.goBack()}
            name="close"
          />
          <View style={{paddingBottom: unit(0.5)}}>
            <Title
              style={{marginTop: "auto"}}
              ellipsizeMode="tail"
              numberOfLines={1}
            >
              {title}
            </Title>
            <Caption style={{marginBottom: "auto"}}>{urlText}</Caption>
          </View>
        </View>
      </Card>
      <WebView
        source={{uri}}
        startInLoadingState={true}
        onLoad={(syntheticEvent) => {
          const {
            nativeEvent: {title, url},
          } = syntheticEvent;

          setTitle(title);
          setUrlText(url);
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    borderRadius: 0,
    zIndex: 1,
    elevation: 3,
  },
  titleContainer: {
    padding: 0,
    flexDirection: "row",
    alignItems: "center",
  },
  container: {
    backgroundColor: "white",
    height: "100%",
    overflow: "hidden",
  },
  closeIcon: {
    fontSize: 24,
    padding: 16,
    paddingLeft: normalize(16),
  },
});
export default PrivacyPolicy;
