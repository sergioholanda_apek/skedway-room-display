import React, {createContext, useState} from "react";

import useAsyncStorage from "hooks/useAsyncStorage";

export const StorageContext = createContext({});

const StorageProvider = ({children}) => {

  const [wasRead, setWasRead] = useState(null);

  const [appStorage, _, insertAppStorage] = useAsyncStorage(
    "@APP_STORAGE",
    "",
    () => setWasRead(true)
  );

  return (
    <StorageContext.Provider value={{appStorage, insertAppStorage, wasRead}}>
      {children}
    </StorageContext.Provider>
  );
};

export default StorageProvider
