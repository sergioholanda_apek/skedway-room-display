export const encodeBitwiseList = (list) => {
  if (!list.length) {
    return 0;
  }

  return list.reduce((acc, curr) => acc | curr, 0);
};

export const decodeBitwise = (list, number) => {
  return list.reduce((acc, curr) => {
    if (curr & number) {
      return [...acc, curr];
    }
    return acc;
  }, []);
};
