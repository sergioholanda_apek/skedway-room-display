import React from "react";
import Svg, { Path } from "react-native-svg";

export default ({ ...props }) => {
  return (
    <Svg {...props} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 313 60">
      <Path
        fill="#676767"
        d="M85.6 40c1.7 1.1 4.6 2.3 7.5 2.3 4.1 0 6.1-2.1 6.1-4.6 0-2.7-1.6-4.2-5.8-5.7-5.6-2-8.2-5.1-8.2-8.8 0-5 4.1-9.1 10.8-9.1 3.2 0 5.9.9 7.7 1.9l-1.4 4.1c-1.2-.8-3.5-1.8-6.4-1.8-3.3 0-5.2 1.9-5.2 4.3 0 2.6 1.9 3.7 5.9 5.3 5.4 2.1 8.2 4.8 8.2 9.4 0 5.5-4.3 9.3-11.7 9.3-3.4 0-6.6-.8-8.8-2.1l1.3-4.5z"
      ></Path>
      <Path
        fill="#676767"
        d="M117.5 28.9c.9-1.1 2-2.4 2.9-3.5l9.1-10.8h6.8l-12 12.8L138 45.8h-6.9l-10.8-14.9-2.9 3.2v11.7h-5.6V0h5.6l.1 28.9z"
      ></Path>
      <Path
        fill="#676767"
        d="M145.3 31.2c.1 7.7 5 10.8 10.7 10.8 4.1 0 6.5-.7 8.6-1.6l1 4.1c-2 .9-5.4 1.9-10.4 1.9-9.6 0-15.3-6.3-15.3-15.7 0-9.4 5.5-16.8 14.6-16.8 10.2 0 12.9 9 12.9 14.7 0 1.2-.1 2.1-.2 2.6h-21.9zm16.6-4c.1-3.6-1.5-9.2-7.9-9.2-5.7 0-8.2 5.3-8.7 9.2h16.6z"
      ></Path>
      <Path
        fill="#676767"
        d="M201.3 0v37.7c0 2.8.1 5.9.3 8.1h-5.1l-.3-5.4h-.1c-1.7 3.5-5.5 6.1-10.6 6.1-7.5 0-13.3-6.4-13.3-15.8-.1-10.4 6.4-16.7 14-16.7 4.8 0 8 2.3 9.4 4.8h.1V0h5.6zm-5.7 27.3c0-.7-.1-1.7-.3-2.4-.8-3.6-3.9-6.6-8.2-6.6-5.9 0-9.3 5.2-9.3 12 0 6.3 3.1 11.5 9.2 11.5 3.8 0 7.3-2.5 8.3-6.8.2-.8.3-1.5.3-2.4v-5.3z"
      ></Path>
      <Path
        fill="#676767"
        d="M213 14.6l4.1 15.8c.9 3.5 1.7 6.7 2.3 9.9h.2c.7-3.2 1.7-6.5 2.8-9.9l5.1-15.9h4.8l4.8 15.6c1.2 3.7 2.1 7 2.8 10.2h.2c.5-3.2 1.4-6.4 2.4-10.1l4.4-15.7h5.6l-10 31.2h-5.2l-4.8-14.9c-1.1-3.5-2-6.6-2.8-10.2h-.1c-.8 3.7-1.7 7-2.8 10.3l-5 14.8h-5.2l-9.4-31.2 5.8.1z"
      ></Path>
      <Path
        fill="#676767"
        d="M275.1 45.8l-.4-3.9h-.2c-1.7 2.4-5.1 4.6-9.5 4.6-6.3 0-9.5-4.4-9.5-9 0-7.5 6.7-11.7 18.7-11.6v-.6c0-2.6-.7-7.2-7.1-7.2-2.9 0-5.9.9-8.1 2.3l-1.3-3.7c2.6-1.7 6.3-2.8 10.2-2.8 9.5 0 11.9 6.5 11.9 12.8v11.7c0 2.7.1 5.3.5 7.5l-5.2-.1zm-.8-15.9c-6.2-.1-13.2 1-13.2 7 0 3.7 2.4 5.4 5.3 5.4 4.1 0 6.6-2.6 7.5-5.2.2-.6.3-1.2.3-1.8v-5.4h.1z"
      ></Path>
      <Path
        fill="#676767"
        d="M290.5 14.6l6.8 18.4c.7 2.1 1.5 4.5 2 6.4h.1c.6-1.9 1.2-4.3 2-6.5l6.2-18.3h6l-8.5 22.2c-4.1 10.7-6.8 16.2-10.7 19.5-2.8 2.4-5.5 3.4-7 3.7l-1.4-4.8c1.4-.5 3.3-1.4 5-2.8 1.5-1.2 3.5-3.4 4.8-6.3.3-.6.4-1 .4-1.4 0-.3-.1-.8-.4-1.5l-11.5-28.7 6.2.1z"
      ></Path>
      <Path
        fill="#676767"
        d="M53.9 0H6.1C2.7 0 0 2.7 0 6.1V54c0 3.3 2.7 6 6.1 6H54c3.3 0 6.1-2.7 6.1-6.1V6.1C60 2.7 57.3 0 53.9 0zm-33 54.9l17.1-17c4.4-4.4 4.4-11.4 0-15.8s-11.4-4.4-15.8 0l-17 17v-31c0-1.7 1.4-3 3-3H52c1.7 0 3 1.4 3 3v43.8c0 1.7-1.4 3-3 3H20.9zm3-24.9c0-1.7.7-3.2 1.8-4.3 1.1-1.1 2.6-1.8 4.3-1.8 3.4 0 6.1 2.7 6.1 6.1 0 1.7-.7 3.2-1.8 4.3-1.1 1.1-2.6 1.8-4.3 1.8-3.4 0-6.1-2.7-6.1-6.1z"
      ></Path>
      <Path
        fill="#76bf43"
        d="M36.1 30c0 1.7-.7 3.2-1.8 4.3-1.1 1.1-2.6 1.8-4.3 1.8-3.4 0-6.1-2.7-6.1-6.1 0-1.7.7-3.2 1.8-4.3 1.1-1.1 2.6-1.8 4.3-1.8 3.4 0 6.1 2.7 6.1 6.1z"
      ></Path>
    </Svg>
  );
};
