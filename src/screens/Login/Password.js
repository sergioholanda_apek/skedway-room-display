import React, {useContext, useEffect, useState} from "react";
import {View} from "react-native";
import {Caption, useTheme} from "react-native-paper";
import i18n from "i18n-js";
import HeaderLeft from "components/HeaderLeft";
import {LoginFlowContext} from "contexts/LoginFlowContext";

import CustomInput from "components/common/CustomInput";
import RoundedButton from "components/common/buttons/RoundedButton";
import CustomDialog from "components/common/dialog/CustomDialog";
import LoginScreenLayout from "components/Login/LoginScreenLayout";

import normalize from "utils/normalize";
import loginStyles from "styles/loginStyles";

const Password = ({navigation}) => {

  const {colors} = useTheme();

  const [email, setEmail] = useState(null);
  const [showModalError, setShowModalError] = useState(false);
  const [companyId, setCompanyId] = useState(null);
  const [password, setPassword] = useState("");
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  const {
    isLoading,
    passwordError,
    validPassword,
    userCompany,
    verifyPasswordAndLogin,
    setInitialState
  } = useContext(LoginFlowContext);

  useEffect(() => {
    const currentEmail = navigation.getParam("email");
    const currentAccount = navigation.getParam("account");

    setEmail(currentEmail);
    setCompanyId(currentAccount?.id);

    if (passwordError) {
      setShowModalError(true);
    }
  }, [passwordError]);

  useEffect(() => {
    if (validPassword) {
      navigation.navigate("RoomSelection", {userCompany});
    }
  }, [passwordError, validPassword]);

  const handleNext = () => {
    verifyPasswordAndLogin(email, password, companyId);
  };

  const handlePassword = (value) => {
    setPassword(value);
  };

  const toggleShowPassword = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const handleCloseModal = () => {
    setShowModalError(false);
    setInitialState();
  }

  return (
    <>
      <LoginScreenLayout>
        <View style={{top: "32%"}}>
          <Caption style={loginStyles.instructions}>
            {i18n.t("login.password_instructions")}
          </Caption>
        </View>

        <View style={{top: "35%", paddingLeft: normalize(40), paddingRight: normalize(40)}}>
          <CustomInput
            label={i18n.t("prompt_password")}
            value={password}
            onChangeText={handlePassword}
            secureTextEntry={secureTextEntry}
            isPassword={true}
            onPress={toggleShowPassword}
            autoCapitalize="none"
            autoCorrect={false}
          />
        </View>

        <View style={[loginStyles.buttonPosition, {zIndex: 10}]}>
          <RoundedButton
            label={i18n.t("login.login")}
            action={handleNext}
            type="secondary"
          />
        </View>
      </LoginScreenLayout>

      <CustomDialog
        style={{marginHorizontal: "15%"}}
        visible={showModalError}
        onDismiss={handleCloseModal}
        confirmAction={handleCloseModal}
        confirmLabel="Ok"
        title={i18n.t("error_password_incorrect_title")}
        content={passwordError}
      />
    </>
  );
};

Password.navigationOptions = ({navigation}) => {
  return {
    title: `${i18n.t("prompt_password")}`,
    headerLeft: () => <HeaderLeft onIconPress={() => navigation.goBack()}/>,
  };
};

export default Password;
