import React from "react";
import { View, StyleSheet } from "react-native";
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/Ionicons';
import { Text, Button, useTheme } from "react-native-paper";
import normalize from "utils/normalize";

const Line = ({ numbers, onClickCallback }) => {

  const { colors, unit } = useTheme();

  return (
    <View style={styles.lineContainer}>
      {numbers.map(
        ({ digit, callback, contentStyle, containerStyle, digitStyle }) => (
          <Button
            contentStyle={contentStyle}
            style={[{marginLeft: normalize(6), marginRight: normalize(6), marginBottom: normalize(10), backgroundColor: colors.white, height: 55, width: 50, borderRadius: normalize(9)}, containerStyle]}
            key={`number_pad_${digit}`}
            onPress={() => {
              if (callback) {
                callback();
                return;
              }
              onClickCallback(digit);
            }}
          >
            {
              !isNaN(digit) ? (
                <Text
                  style={[
                    {
                      fontSize: normalize(10),
                      fontFamily: "Manrope_600SemiBold",
                      color: colors.text,
                      marginTop: 2,
                    },
                    digitStyle,
                  ]}
                >
                  {digit}
                </Text>
              ) : digit === 'C' ? (
                <Icon name="check" size={30}  color="#fff" style={{marginRight: 10}} />
              ) : (
                <Icon2 name="backspace-outline" size={30} color="#fff" style={{marginRight: 10}} />
              )
            }
          </Button>
        )
      )}
    </View>
  );
};

const NumberPad = ({ onDeleteCallback, onConfirmCallback, ...otherProps }) => {
  const { colors, unit } = useTheme();

  const buttonStyle = {
    marginTop: 2,
    borderRadius: normalize(9),
  };

  const digitStyle = {
    color: colors.white,
  };

  return (
    <View style={styles.digitPadContainer}>
      <Line
        {...otherProps}
        numbers={[{ digit: 1 }, { digit: 2 }, { digit: 3 }]}
      />
      <Line
        {...otherProps}
        numbers={[{ digit: 4 }, { digit: 5 }, { digit: 6 }]}
      />
      <Line
        {...otherProps}
        numbers={[{ digit: 7 }, { digit: 8 }, { digit: 9 }]}
      />
      <Line
        {...otherProps}
        numbers={[
          {
            digit: "X",
            digitStyle,
            contentStyle: buttonStyle,
            containerStyle: {
              ...buttonStyle,
              backgroundColor: colors.intermediate,
            },
            callback: onDeleteCallback,
          },
          { digit: 0, containerStyle: { marginHorizontal: unit(0.5) } },
          {
            digit: "C",
            digitStyle,
            contentStyle: buttonStyle,
            containerStyle: {
              ...buttonStyle,
              backgroundColor: colors.darkGreen,
            },
            callback: onConfirmCallback,
          },
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  lineContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  digitPadContainer: {
    alignItems: "center",
    marginTop: normalize(12)
  },
});
export default NumberPad;
