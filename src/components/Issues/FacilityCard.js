import React from "react";
import {Image, StyleSheet, Text, View} from "react-native";
import {Caption, List, useTheme,} from "react-native-paper";

import i18n from "i18n-js";
import uuid from "react-native-uuid";
import {format} from "date-fns";

import {ASSET_AIR_CONDITIONING, ASSET_FURNITURE, ASSET_LIGHTING, ASSETS,} from "./IssueReportModal";

import {decodeBitwise} from "utils/bitwiseUtils";
import {parseWithNoTZ} from "utils/dateUtils";
import normalize from "utils/normalize";
import {getCurrentLocale} from "utils/localizationUtils";

const STATUS_NEW = 1;
const STATUS_AWARE = 2;
const STATUS_FIXED = 3;

const FacilityCard = (props) => {

  const {
    userName,
    userPictureUrl,
    createdDate,
    asset,
    amenities,
    status,
    description,
  } = props;

  if (status === STATUS_FIXED) {
    return null;
  }

  const {colors} = useTheme();

  const defaultAssetList = [
    ASSET_LIGHTING,
    ASSET_FURNITURE,
    ASSET_AIR_CONDITIONING,
  ];

  const assetsList = decodeBitwise(defaultAssetList, asset);

  const assetsTitles =
    (assetsList &&
      assetsList
        .map((assetId) => ASSETS.find((as) => as.id === assetId))
        .reduce(
          (acc, curr) =>
            !acc ? `${i18n.t(curr.title)}` : `${acc}, ${i18n.t(curr.title)}`,
          ""
        )) ||
    "";

  const amenitiesTitles =
    (amenities &&
      amenities.reduce(
        (acc, curr) => (!acc ? `${curr}` : `${acc}, ${curr}`),
        ""
      )) ||
    "";

  const glue = assetsTitles && amenitiesTitles ? ", " : "";

  const joinedFacilities = `${assetsTitles}${glue}${amenitiesTitles}`;

  const parsedCreatedDate = parseWithNoTZ(createdDate);

  const dateString = format(parsedCreatedDate, "d MMMM y 'às' HH:mm", {
    locale: getCurrentLocale(),
  });

  const Status = () => {
    const label = status === STATUS_NEW ? i18n.t("New") : status === STATUS_AWARE ? i18n.t("Aware") : "";
    const color = status === STATUS_NEW ? colors.intermediate : status === STATUS_AWARE ? colors.warning : "";

    return (
      <Text style={[styles.fontStatus, {color: color}]}>
        {i18n.t(label)}
      </Text>
    )
  };

  return (
    <View style={{marginTop: normalize(10), borderRadius: 10, backgroundColor: colors.background}}>
      <List.Item
        titleStyle={[styles.title, styles.fontMedium]}
        style={styles.container}
        title={userName}
        description={({color}) => (
          <View style={styles.description}>
            {
              dateString ?
                <Caption style={{fontFamily: "Manrope_400Regular", fontSize: normalize(7)}}>{dateString}</Caption> :
                null
            }

            {
              joinedFacilities.split(", ").map(facility =>
                <Caption key={uuid.v4()} style={[styles.fontMedium, styles.joinedFacilities]}>
                  {`● ${facility}`}
                </Caption>
              )}

            {
              description ? (
                <View style={{backgroundColor: colors.white, padding: 10, marginTop: 16, borderRadius: 10}}>
                  <Caption style={{fontFamily: "Manrope_400Regular", fontSize: normalize(7)}}>{description}</Caption>
                </View>
              ) : null
            }
          </View>
        )}
        left={() => (
          <Image
            style={{
              marginTop: 16,
              marginRight: 12,
              height: 60,
              width: 60,
              borderRadius: normalize(8),
              resizeMode: "cover",
            }}
            source={{uri: userPictureUrl}}
          />
        )}
        right={() => <>{Status()}</>}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    height: 60,
    width: 106,
    resizeMode: "contain",
    marginVertical: 12,
    marginRight: 0,
    marginLeft: 16,
  },
  title: {
    marginTop: normalize(4),
    fontSize: normalize(9),
  },
  fontMedium: {
    fontFamily: 'Manrope_400Regular',
  },
  fontStatus: {
    fontStyle: "italic",
    fontFamily: "LibreBaskerville_400Regular_Italic",
    marginTop: normalize(10),
    fontSize: normalize(10),
  },
  avatar: {
    marginRight: normalize(8),
    marginTop: normalize(4),
  },
  container: {
    paddingHorizontal: normalize(16),
  },
  joinedFacilities: {
    fontSize: normalize(8),
    paddingTop: normalize(6),
  },
  description: {
    fontFamily: "Manrope_600SemiBold",
    paddingBottom: normalize(4),
  },
});

export default FacilityCard;
