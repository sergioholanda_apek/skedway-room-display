import * as React from "react"
import Svg, { Path } from "react-native-svg"
import {useTheme} from "react-native-paper";

const Info = (props) => {

  const {colors} = useTheme();
  const {focused} = props;

  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      fill={focused ? colors.active : colors.inactive}
      viewBox="0 0 16.5 17"
      {...props}
    >
      <Path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
    </Svg>
  )

}

export default Info