import React, {useState} from "react";
import {Keyboard, StyleSheet, TextInput, TouchableOpacity, View} from "react-native";
import {Menu,} from "react-native-paper";
import ChevronDown from "../../../assets/icons/ChevronDownDark";

const CustomDropdown = (props) => {
  const {
    label = "",
    value = "",
    data = [],
    style = {marginBottom: 8, height: 50},
  } = props;

  const [visible, setVisible] = useState(false);

  const handleOnPress = (item) => {
    if (item.onPress) {
      item.onPress();
    }
    setVisible(false);
  };

  return (
    <Menu
      style={styles.menuContainer}
      visible={visible}
      onDismiss={() => setVisible(false)}
      anchor={
        <TouchableOpacity
          onPress={() => {
            Keyboard.dismiss();
            setVisible(true);
          }}
        >
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              pointerEvents="none"
              editable={false}
              value={value}
              mode="flat"
              placeholder={label}
            />
            <ChevronDown style={styles.downIcon} height={22} width={22}/>
          </View>
        </TouchableOpacity>
      }
    >
      {data.map((item, index) => (
        <Menu.Item
          key={index}
          onPress={() => handleOnPress(item)}
          title={item.name}
        />
      ))}
    </Menu>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 55,
    backgroundColor: "#fff",
    borderRadius: 10,
    fontSize: 18,
    fontFamily: "Manrope_400Regular",
    paddingLeft: 20
  },
  menuContainer: {
    paddingTop: 64,
    borderRadius: 10,
  },
  menuItem: {
    minWidth: "100%",
  },
  inputContainer: {
    position: "relative",
  },
  downIcon: {
    position: "absolute",
    right: 16,
    top: 18,
  },
});

export default CustomDropdown;
