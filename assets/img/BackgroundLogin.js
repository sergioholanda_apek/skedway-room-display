import * as React from "react"
import {SvgXml} from "react-native-svg"

const xml = `
<svg width="298" height="305" viewBox="0 0 298 305" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g clip-path="url(#clip0_395_745)">
    <path d="M298 0H0V305H298V0Z" fill="#E5E5E5"/>
    <mask id="mask0_395_745" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="299" height="324">
      <path d="M0 0H298.287V323.089H0V0Z" fill="#C4C4C4"/>
    </mask>
    <g mask="url(#mask0_395_745)">
      <path d="M144.345 208.881L15.498 -1.40575V-108.594H287.174V208.881H144.345Z" fill="url(#paint0_linear_395_745)"/>
      <path d="M260.785 208.867L131.938 -1.42008V-108.607H403.612V208.867H260.785Z" fill="url(#paint1_linear_395_745)"/>
      <path d="M144.042 632.031V208.504H396.884L390.82 632.031H144.042Z" fill="url(#paint2_linear_395_745)"/>
      <path d="M-59.6616 -1.04395L15.4554 -1.04327L144.146 208.505V636.189L-59.6614 636.19L-59.6616 -1.04395Z" fill="url(#paint3_linear_395_745)"/>
      <path d="M-59.397 618.723V167.314C-59.397 151.422 -39.4872 144.683 -30.7113 157.604L141.371 410.962C143.188 413.638 144.162 416.834 144.162 420.125V618.177C144.162 627.233 136.953 634.655 128.158 634.655H-43.9238C-52.5113 634.655 -59.397 627.565 -59.397 618.723Z" fill="url(#paint4_linear_395_745)"/>
    </g>
  </g>
  <defs>
    <linearGradient id="paint0_linear_395_745" x1="23.4472" y1="110.572" x2="287.174" y2="110.572" gradientUnits="userSpaceOnUse">
      <stop stop-color="#FA9131"/>
      <stop offset="1" stop-color="#E5E4E4"/>
    </linearGradient>
    <linearGradient id="paint1_linear_395_745" x1="139.886" y1="110.558" x2="403.612" y2="110.558" gradientUnits="userSpaceOnUse">
      <stop stop-color="#FA9131"/>
      <stop offset="1" stop-color="#E5E4E4"/>
    </linearGradient>
    <linearGradient id="paint2_linear_395_745" x1="202.604" y1="204.923" x2="169.455" y2="440.022" gradientUnits="userSpaceOnUse">
      <stop stop-color="#DCCCFF"/>
      <stop offset="1" stop-color="white" stop-opacity="0"/>
    </linearGradient>
    <linearGradient id="paint3_linear_395_745" x1="20.3403" y1="-18.7766" x2="-179.978" y2="59.2645" gradientUnits="userSpaceOnUse">
      <stop stop-color="#142C24"/>
      <stop offset="0.933453" stop-color="#298466" stop-opacity="0"/>
    </linearGradient>
    <linearGradient id="paint4_linear_395_745" x1="20.3564" y1="187.788" x2="-190.264" y2="637.643" gradientUnits="userSpaceOnUse">
      <stop stop-color="#142C24"/>
      <stop offset="0.933453" stop-color="#298466" stop-opacity="0"/>
    </linearGradient>
    <clipPath id="clip0_395_745">
      <path d="M0 30.5C0 13.6553 13.6553 0 30.5 0H267.5C284.345 0 298 13.6553 298 30.5V305H0V30.5Z" fill="white"/>
    </clipPath>
  </defs>
</svg>
`;

export default (props) => <SvgXml {...props} xml={xml} />;
