import React, {createContext, useState} from "react";

export const ScreenSaverContext = createContext({});

const ScreenSaverProvider = ({children}) => {

  let timer = null;
  const TIMEOUT = 30_000; //30 seconds

  const [active, setActive_] = useState(true);

  function setActive() {
    clearTimeout(timer);

    timer = setTimeout(() => {
      setActive_(false);
    }, TIMEOUT);

    setActive_(true);
  }

  return(
    <ScreenSaverContext.Provider value={{active, setActive}}>
      {children}
    </ScreenSaverContext.Provider>
  )

}

export default ScreenSaverProvider
