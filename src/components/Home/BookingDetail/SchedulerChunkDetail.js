import React, {useContext} from "react";
import {ScrollView, StyleSheet, View} from "react-native";
import {Divider, Subheading, Title, useTheme} from "react-native-paper";

import {MaterialCommunityIcons} from "@expo/vector-icons";

import {SchedulingContext} from "contexts/SchedulingContext";

import normalize from "utils/normalize";
import Attendee from "../../Booking/Attendee";

const SchedulerChunkDetail = (props) => {
  const {attendees, isCheckIn = false} = props;

  const {setSelectedSchedule} = useContext(SchedulingContext);

  const onClose = () => setSelectedSchedule(null);

  function getAttendeeProps(attendee) {
    return {
      ...attendee,
      isCheckIn,
      onClose,
    };
  }

  return (
    <>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.chunkContainer}
      >
        <Attendee attendees={attendees} />
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  chunkContainer: {
    paddingTop: normalize(18),
    height: "100%",
  },
  closeIcon: {
    marginLeft: "auto",
  },
  spacer: {
    paddingBottom: 32,
  },
  row: {
    flexDirection: "row",
  },
});

export default SchedulerChunkDetail;
