import React from "react";
import {StyleSheet, Text, View} from "react-native";

import SchedulerHeader from "components/Header/SchedulerHeader";

import normalize from "utils/normalize";
import {useTheme} from "react-native-paper";

import {StatusScheduling} from "utils/schedulerUtils";
import Capacity from "../common/Capacity";
import Availability from "components/ScreenSaver/common/Availability";

const SkedwayLayout = (props) => {

  const {
    startedSchedule,
    nextSchedule,
    currentStatus,
    statusColor,
    spaceName,
    isThereAtLeastOneCheckIn,
    status,
    deadlineToCheckout,
  } = props

  const {colors} = useTheme();

  function getCardColor() {
    return currentStatus === StatusScheduling.FREE ? colors.darkGreen : currentStatus === StatusScheduling.CHECKIN_ADVANCE ?
      colors.yellow : currentStatus === StatusScheduling.OCCUPIED ? colors.red : colors.darkGreen;
  }

  return (
    <View style={{height: "100%", flexDirection: "column"}}>
      <View style={{marginBottom: normalize(10)}}>
        <SchedulerHeader isScreenSaver={true}/>
      </View>

      <View style={[styles.content, {backgroundColor: getCardColor()}]}>
        <View style={{flexDirection: "row", alignItems: "center"}}>
          <Text style={[styles.statusText, {color: colors.white}]}>
            {status}
          </Text>
        </View>

        <View style={{marginLeft: 5}}>
          <Availability {...{startedSchedule, nextSchedule, color: colors.white, fontSize: normalize(8)}}/>
        </View>

        <Text style={[styles.spaceNameText, {color: colors.white}]}>
          {spaceName}
        </Text>

        <Capacity direction="row" size="small" fontSize={normalize(7)} color={colors.white}/>
      </View>
    </View>
  )

}
const styles = StyleSheet.create({
  content: {
    flex: 1,
    padding: normalize(10),
    borderRadius: normalize(10),
    justifyContent: "flex-end"
  },
  statusText: {
    marginLeft: 5,
    fontSize: normalize(10),
    fontFamily: "Manrope_500Medium"
  },
  availabilityText: {
    marginLeft: 5,
    fontSize: normalize(12),
    fontFamily: "Manrope_600SemiBold"
  },
  spaceNameText: {
    marginLeft: 5,
    fontSize: normalize(9),
    fontFamily: "Manrope_400Regular",
    marginTop: normalize(5)
  }
});

export default SkedwayLayout;


