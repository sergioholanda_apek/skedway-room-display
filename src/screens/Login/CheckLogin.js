import {useContext, useEffect} from "react";

import {StorageContext} from "contexts/StorageContext";

const CheckLogin = ({navigation}) => {

  const factoryReset = navigation.getParam("factoryReset");

  const {appStorage, wasRead} = useContext(StorageContext);

  useEffect(() => {
    if (factoryReset) {
      navigation.navigate("Welcome");
      return;
    }
    if (wasRead) {
      if (
        appStorage &&
        appStorage.authToken &&
        appStorage.site &&
        appStorage.space
      ) {
        navigation.navigate("Home", {data: appStorage});
        return;
      }

      navigation.navigate("Welcome");
    }
  }, [wasRead]);

  return null;
};

CheckLogin.navigationOptions = () => {
  return {
    headerShown: false,
  };
};

export default CheckLogin;
