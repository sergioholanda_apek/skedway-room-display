# Skedway Room Display (React Native)

_Check-in your way. Go far beyond the basics._

With Skedway Room Display App you go beyond the basics, exploring amazing features. Check-in via Apple boarding pass chooses the best day for your meeting by using Chart Calendar.

## Main tools

- [React Native](https://reactnative.dev/)
- [React Native Paper](https://callstack.github.io/react-native-paper/)
- [Expo](https://docs.expo.dev/)

## Prerequisites

To open the app you need to install a simulator/emulator. We recommend one of below:

- [Android Studio Emulator (Android)](https://docs.expo.dev/workflow/android-studio-emulator/)
- [Xcode Simulator (iOS)](https://docs.expo.dev/workflow/ios-simulator/)

## Installation and running

First all, install dependencies:

- `npm install`

Running Expo:

- `npm start`

To open the simulator, press inside terminal:

- `ctrl+i` Windows
- `cmd+i` MacOS


### Build new version:
- `eas build --platform ios`

Author: _Sérgio Holanda_
_sergio.holanda@apektelecom.com_

Copyright © 1999-2021 Apek International
