import React from "react";
import {Image} from "react-native";

import normalize from "utils/normalize";

const UserImage = ({pictureUrl}) => {

  return (
    <Image
      style={{
        height: normalize(28),
        width: normalize(28),
        borderRadius: normalize(8),
        padding: 0,
        resizeMode: "cover",
      }}
      size={50}
      source={{uri: pictureUrl}}
    />
  )
}

export default UserImage;