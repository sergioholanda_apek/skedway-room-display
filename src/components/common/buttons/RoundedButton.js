import React from "react";
import {Button, useTheme} from "react-native-paper";
import {StyleSheet, View} from "react-native";

import ArrowRightLight from "../../../../assets/icons/ArrowRightLight";
import ArrowRightDark from "../../../../assets/icons/ArrowRightDark";

import normalize from "utils/normalize";

const RoundedButton = ({action, label, type, isLoading, disabled = false}) => {

  const {colors} = useTheme();

  function getButtonColor() {
    switch (type) {
      case "primary":
        return disabled ? colors.disabled : colors.accent;
      case "secondary":
        return disabled ? colors.disabled : colors.primary;
      default:
        return disabled ? colors.disabled : colors.accent;
    }
  }

  function getLabelColor() {
    switch (type) {
      case "primary":
        return colors.primary;
      case "secondary":
        return colors.accent;
      default:
        return colors.primary;
    }
  }

  function generateIcon() {
    switch (type) {
      case "primary":
        return <ArrowRightDark
          style={styles.icon}
          width="30%"
          height="30%"
        />;
      case "secondary":
        return <ArrowRightLight
          style={styles.icon}
          width="30%"
          height="30%"
        />
      default:
        return <ArrowRightDark
          style={styles.icon}
          width="30%"
          height="30%"
        />;
    }
  }

  return (
    <View style={styles.buttonContainer}>
      <Button
        style={[styles.button, {backgroundColor: getButtonColor()}]}
        labelStyle={[styles.label, {color: getLabelColor()}]}
        loading={isLoading}
        uppercase={false}
        onPress={() => action()}
        disabled={disabled}
      >
        {label}
      </Button>
      {
        generateIcon()
      }
    </View>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    width: "100%"
  },
  button: {
    marginVertical: 31,
    textAlignVertical: "center",
    borderRadius: normalize(25),
    height: normalize(25),
    width: normalize(100),
    paddingVertical: normalize(1),
  },
  label: {
    fontSize: normalize(9),
    fontFamily: "Manrope_600SemiBold",
    marginRight: "50%",
  },
  icon: {
    position: "absolute",
    top: 39,
    left: 140
  }
});


export default RoundedButton;
