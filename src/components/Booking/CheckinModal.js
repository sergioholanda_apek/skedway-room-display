import React, {useContext} from "react";
import i18n from "i18n-js";
import {useTheme} from "react-native-paper";
import {View} from "react-native";
import {normalize} from "react-native-elements";
import {differenceInHours, differenceInMilliseconds, format, parseISO} from "date-fns";

import MainLayout from "components/common/layout/MainLayout";
import SchedulingInfoCard from "../common/SchedulingInfoCard";
import Attendee from "./Attendee";

import {SchedulingContext} from "contexts/SchedulingContext";
import {StorageContext} from "contexts/StorageContext";
import {SyncCheckInsContext} from "contexts/SyncCheckInsContext";

import useSync from "hooks/useSync";

import {
  getCurrentStatus,
  getNextMeeting,
  getNowHappeningMeeting,
  getScheduleHost,
  StatusScheduling,
  verifyAtLeastOneCheckIn,
  verifyCheckInAdvance
} from "utils/schedulerUtils";

import {getFormattedDate} from "utils/dateUtils";

const CheckinModal = ({navigation}) => {

  const {colors} = useTheme();
  const today = useSync();

  const {todayDayScheduling} = useContext(SchedulingContext);
  const {appStorage} = useContext(StorageContext);
  const {checkIn} = useContext(SyncCheckInsContext);

  const {space, site} = appStorage;

  const startedSchedule = getNowHappeningMeeting(today, todayDayScheduling);
  const nextSchedule = getNextMeeting(today, todayDayScheduling);
  const isCheckInAdvance = verifyCheckInAdvance(appStorage, nextSchedule);
  const statusScheduling = getCurrentStatus(startedSchedule, nextSchedule, isCheckInAdvance);
  const isThereAtLeastOneCheckIn = verifyAtLeastOneCheckIn(checkIn, startedSchedule, nextSchedule);

  const onDismiss = () => {
    navigation.goBack();
  }

  function getStatus() {
    if (statusScheduling === StatusScheduling.FREE) {
      return i18n.t("free_label");
    } else if (statusScheduling === StatusScheduling.CHECKIN_ADVANCE) {
      return i18n.t("start_in_label", {time: getTimeUntilSchedule()});
    } else {
      return i18n.t("occupied_label");
    }
  }

  function getTimeUntilSchedule() {
    const {startDate} = nextSchedule;
    const startDateISO = parseISO(startDate);
    const diff = differenceInMilliseconds(startDateISO, today);
    const hours = differenceInHours(startDateISO, today);
    const minutes = parseInt(format(new Date(diff), "m"));
    const seconds = parseInt(format(new Date(diff), "s"));

    let output = "";
    if (hours > 0) {
      output += `${hours} ${i18n.t("screenSaver.hours", {count: hours})} `;
    } else if (minutes === 59) {
      output += `${1} ${i18n.t("screenSaver.hour", {count: 1})} `;
    }

    if (minutes >= 0 && minutes < 59) {
      output += `${minutes + 1} ${i18n.t("screenSaver.minutes", {count: minutes + 1})}`;
    } else if (minutes === 0 && seconds > 0) {
      output += `${1} ${i18n.t("screenSaver.minutes", {count: 1})}`;
    }
    return output;
  }

  function getColor() {
    if (statusScheduling === StatusScheduling.FREE) {
      return colors.darkGreen;
    } else if (statusScheduling === StatusScheduling.CHECKIN_ADVANCE) {
      return colors.yellow;
    } else if (statusScheduling === StatusScheduling.OCCUPIED) {
      return colors.red;
    }
  }

  function getOccupancy() {
    if (statusScheduling === StatusScheduling.OCCUPIED) {
      const {startDate, endDate} = startedSchedule;
      const formattedStartDate = getFormattedDate(startDate, site?.hourFormat24);
      const formattedEndDate = getFormattedDate(endDate, site?.hourFormat24);
      return i18n.t("screenSaver.time_interval", [formattedStartDate, formattedEndDate]);
    }
  }

  function isShowCapacity() {
    return statusScheduling === StatusScheduling.CHECKIN_ADVANCE || statusScheduling === StatusScheduling.FREE;
  }

  function getHostId() {
    const host = getScheduleHost(nextSchedule)
    return host?.userId;
  }

  function getSchedulingId() {
    return nextSchedule?.id;
  }

  function setShowDelete() {
    return statusScheduling === StatusScheduling.CHECKIN_ADVANCE
  }

  const schedulingProps = () => (
    {
      hostId: getHostId(),
      schedulingId: getSchedulingId(),
      showGoBack: true,
      showStatusBar: true,
      showDelete: setShowDelete(),
      showStatusIcon: false,
      showCapacity: isShowCapacity(),
      onDismiss,
      statusColor: getColor(),
      eventTitle: space?.name,
      status: getStatus(),
      occupancy: getOccupancy()
    }
  )

  return (
    <MainLayout>
      <SchedulingInfoCard {...schedulingProps()}/>
      <View style={{marginTop: normalize(20)}}/>
      <Attendee statusScheduling={statusScheduling} isThereAtLeastOneCheckIn={isThereAtLeastOneCheckIn}/>
    </MainLayout>
  )

}

export default CheckinModal;
