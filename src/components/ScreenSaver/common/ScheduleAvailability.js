import React, {useContext, useEffect, useState} from "react";
import {StyleSheet, Text, View} from "react-native";
import {useTheme} from "react-native-paper";
import i18n from "i18n-js";
import {format, parse} from "date-fns";

import {SchedulingContext} from "contexts/SchedulingContext";
import {StorageContext} from "contexts/StorageContext";

import {getNextMeeting, getNowHappeningMeeting} from "utils/schedulerUtils";
import {parseWithNoTZ} from "utils/dateUtils";
import {getHourFormat} from "utils/hourUtils";

import useSync from "hooks/useSync";
import normalize from "utils/normalize";

const ScheduleAvailability = (props) => {
  const {direction = "column", textStyle = {}, statusColor, showNextMeetingLabel = true} = props;

  const today = useSync();
  const {colors} = useTheme();

  const {todayDayScheduling} = useContext(SchedulingContext);

  const {appStorage} = useContext(StorageContext);
  const {site} = appStorage;

  //const [isNextMeeting, setIsNextMeeting] = useState(false);
  const [nextMeetingLabel, setNextMeetingLabel] = useState("");
  const [formattedStartDate, setFormattedStartDate] = useState("");
  const [formattedEndDate, setFormattedEndDate] = useState("");

  const startedSchedule = getNowHappeningMeeting(today, todayDayScheduling);
  const nextSchedule = getNextMeeting(today, todayDayScheduling);

  useEffect(() => {
    if (startedSchedule) {
      setStartScheduleValues(startedSchedule);
    } else if (nextSchedule) {
      setNextScheduleValues(nextSchedule);
    } else {
      setDefaultScheduleValues();
    }
  }, []);

  const setStartScheduleValues = (startedSchedule) => {
    const {subject, startDate, endDate} = startedSchedule;
    const parsedStartDate = parseWithNoTZ(startDate);
    const parsedEndDate = parseWithNoTZ(endDate);

    //setIsNextMeeting(false);
    setNextMeetingLabel(subject);
    setFormattedStartDate(
      format(parsedStartDate, getHourFormat(site.hourFormat24))
    );
    setFormattedEndDate(
      format(parsedEndDate, getHourFormat(site.hourFormat24))
    );
  };

  const setNextScheduleValues = (nextSchedule) => {
    const {startDate, endDate} = nextSchedule;
    const parsedStartDate = parseWithNoTZ(startDate);
    const parsedEndDate = parseWithNoTZ(endDate);

    //setIsNextMeeting(true);
    setNextMeetingLabel(i18n.t("Next_Meeting"));
    setFormattedStartDate(
      format(parsedStartDate, getHourFormat(site.hourFormat24))
    );
    setFormattedEndDate(
      format(parsedEndDate, getHourFormat(site.hourFormat24))
    );
  };

  const setDefaultScheduleValues = () => {
    if (site && site.hourFormat24) {
      const nowString = format(today, getHourFormat(site.hourFormat24));
      const closingTime = site && site.closingTime;
      const parsedClosingTime = parse(closingTime, "HH:mm:ss", new Date());

      //setIsNextMeeting(true);
      setNextMeetingLabel(i18n.t("Free"));
      setFormattedStartDate(nowString);
      setFormattedEndDate(
        format(parsedClosingTime, getHourFormat(site.hourFormat24))
      );
    }
  };

  return (
    <View style={{flexDirection: direction}}>
      {
        showNextMeetingLabel &&
        <Text style={[styles.spaceCapacity, {color: statusColor}, textStyle]}>
          {nextMeetingLabel}
        </Text>
      }
      <Text style={[styles.spaceCapacity, {color: colors.white}, textStyle]}>
        {`${formattedStartDate} - ${formattedEndDate}`}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  spaceCapacity: {
    fontSize: normalize(14),
    textAlign: "center",
    fontFamily: "Manrope_500Medium"
  },
});

export default ScheduleAvailability;
