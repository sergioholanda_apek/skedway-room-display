import React from "react";
import {StyleSheet, Text, TouchableHighlight, View} from "react-native";

import MainLayout from "./MainLayout";
import ChevronLeft from "../../../../assets/icons/ChevronLeft";
import CustomButton from "../buttons/CustomButton";

import normalize from "utils/normalize";
import {useTheme} from "react-native-paper";

const CustomStackCard = (props) => {

  const {children, subHeader, headerLabel, backAction, action, actionLabel, showContent = true} = props;

  const {colors} = useTheme();

  return (
    <MainLayout>
      <View style={styles.contentHeader}>
        <TouchableHighlight
          style={styles.backButton}
          onPress={backAction}
          underlayColor={colors.white}
        >
          <ChevronLeft
            color={colors.primary}
            height={normalize(10)}
            width={normalize(10)}
          />
        </TouchableHighlight>
        <Text style={styles.header}>
          {headerLabel}
        </Text>
      </View>

      <View style={styles.contentSubheader}>
        {subHeader}
      </View>

      {
        showContent &&
        <View style={styles.contentCard}>
          <View style={[styles.card, {backgroundColor: colors.background}]}>
            {children}
          </View>

          {
            action &&
            <View style={{position: "absolute", bottom: normalize(10)}}>
              <CustomButton
                style={styles.scheduleButton}
                labelStyle={styles.scheduleButtonLabel}
                type="secondary"
                onPress={action}
                label={actionLabel}
              />
            </View>
          }
        </View>
      }

    </MainLayout>
  );
};

const styles = StyleSheet.create({
  contentHeader: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: normalize(12)
  },
  contentSubheader: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: normalize(12)
  },
  backButton: {
    alignItems: "center",
    justifyContent: 'center',
    backgroundColor: "#E9EDEC",
    borderRadius: 50,
    height: normalize(16),
    width: normalize(16),
    paddingRight: 3
  },
  scheduleButton: {
    paddingTop: normalize(4),
    paddingBottom: normalize(4),
    borderRadius: normalize(18),
  },
  scheduleButtonLabel: {
    textTransform: "uppercase",
    fontSize: normalize(9)
  },
  header: {
    marginLeft: normalize(10),
    fontFamily: "Manrope_500Medium",
    fontSize: normalize(9)
  },
  contentCard: {
    flex: 1,
    justifyContent: "space-between",
    alignItems: "flex-end",
  },
  card: {
    padding: normalize(16),
    marginTop: normalize(12),
    borderRadius: normalize(8),
    width: "100%"
  },
})

export default CustomStackCard;
