export default {
  free_now: "Free now",
  all_day_free: "All day free",
  awaiting_check_in: "Waiting for Check-in",
  happening_now: "Happening now",
  next_meeting: "Next meeting",
  no_reservation_today: "No reservations today",
  availability_of: "Availability of %{0}",
  hour: "hour",
  hours: {
    "one": "hour and",
    "other": "hours and"
  },
  minutes: {
    "one": "minute",
    "other": "minutes"
  },
  time_interval: "%{0} to %{1}",
  free_until: "until %{0}",
  countdown_to: "Countdown to",
  check_in: "check-in"
}