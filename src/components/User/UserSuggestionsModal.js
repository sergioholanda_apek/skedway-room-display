import React, {useContext, useEffect, useState} from "react";
import {StyleSheet, Text, View} from "react-native";
import {ActivityIndicator, Searchbar, useTheme,} from "react-native-paper";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scrollview";

import i18n from "i18n-js";
import uuid from "uuid";
import isEmpty from "lodash/isEmpty";
import keyBy from "lodash/keyBy";
import filter from "lodash/filter";

import {SchedulingContext} from "contexts/SchedulingContext";

import UserCardLine from "./UserCardLine";
import CustomStackCard from "components/common/layout/CustomStackCard";

import useDebounce from "hooks/useDebounce";
import normalize from "utils/normalize";

const UserSuggestionsModal = ({ navigation}) => {

  const todayTextTitle = navigation.getParam("todayTextTitle");
  const selectedUsers = navigation.getParam("selectedUsers");
  const selectUser = navigation.getParam("selectUser");

  const {colors} = useTheme();

  const {fetchUserSuggestions, isFetchingUserSuggestions, userSuggestions} = useContext(SchedulingContext);

  const [query, setQuery] = useState("");

  const debouncedValue = useDebounce(query, 300);

  useEffect(() => {
    fetchUserSuggestions(debouncedValue);
  }, [debouncedValue]);

  const onDismiss = () => navigation.goBack();

  const goBack = () => {
    setQuery("");
    onDismiss();
  };

  const handleSelectUser = (user) => {
    selectUser(user);
    goBack();
  }

  const SubHeader = () => (
    <Searchbar
      style={styles.searchBar}
      inputStyle={styles.inputSearchBar}
      autoFocus
      iconColor="black"
      placeholder={i18n.t("prompt_search_for_user")}
      clearButtonMode="never"
      clearIcon={() => null}
      value={query}
      onChangeText={setQuery}
    />
  )

  const filteredSelectedUsers = () => {
    const lookup = keyBy(selectedUsers, (selectedUser_) => selectedUser_.id);
    return filter(userSuggestions, (userSuggestion_) => lookup[userSuggestion_.id] === undefined)
  }

  return (
    <CustomStackCard
      headerLabel={todayTextTitle}
      backAction={goBack}
      subHeader={SubHeader()}
      showContent={!isEmpty(selectedUsers) || !!query}
    >
      {
        (!isEmpty(selectedUsers) && !query) && (
          <View>
            <Text style={{fontFamily: "Manrope_400Regular", fontSize: normalize(10), marginBottom: normalize(20)}}>
              {i18n.t("Added_attendees")}
            </Text>
            {
              selectedUsers.map(user =>
                <UserCardLine
                  key={uuid.v4()}
                  clickable={false}
                  {...user}
                />
              )
            }
          </View>
        )
      }

      {
        !!query && (
          <KeyboardAwareScrollView
            keyboardShouldPersistTaps={"handled"}
            showsVerticalScrollIndicator={false}
            automaticallyAdjustContentInsets={false}
            contentInsetAdjustmentBehavior="automatic"
          >
            {
              isFetchingUserSuggestions &&
              <ActivityIndicator
                size="large"
                style={styles.loadingIndicator}
                animating
                color={colors.primary}
              />
            }
            {
              filteredSelectedUsers().map(userSuggestion =>
                <UserCardLine
                  key={uuid.v4()}
                  handleSelectUser={() => handleSelectUser(userSuggestion)}
                  clickable={true}
                  {...userSuggestion}
                />
              )
            }
          </KeyboardAwareScrollView>
        )
      }
    </CustomStackCard>
  );
};

const styles = StyleSheet.create({
  magnifyingGlassIcon: {
    position: "absolute",
    right: normalize(16),
  },
  cardContainer: {
    borderRadius: 0,
    zIndex: 1,
    elevation: 3,
  },
  container: {
    backgroundColor: "white",
    height: "100%",
    overflow: "hidden",
  },
  titleContainer: {
    padding: 0,
    flexDirection: "row",
    alignItems: "center",
  },
  subtitle: {
    paddingHorizontal: normalize(16),
    paddingVertical: normalize(8),
  },
  searchBar: {
    backgroundColor: "#fff",
    zIndex: -1,
    paddingLeft: normalize(8),
    paddingVertical: 8,
    borderWidth: 1,
    elevation: 0,
    borderColor: "#A89CDF",
  },
  inputSearchBar: {
    fontFamily: "Manrope_500Medium",
    fontSize: normalize(8)
  },
  loadingIndicator: {
    padding: 40,
  },
});

export default UserSuggestionsModal;
