import { useState, useEffect } from "react";

import AsyncStorage from '@react-native-async-storage/async-storage';

export default function useAsyncStorage(key, initialValue, readCallback) {
  const [storageList, setStorageList] = useState(initialValue);

  useEffect(() => {
    retrieveItemAndUpdateState();
  }, []);

  const retrieveItemAndUpdateState = async () => {
    const listFromStorage = await retrieveFromAsyncStorage({
      key,
    });
    setStorageList(listFromStorage);
    if (readCallback) {
      readCallback();
    }
  };

  const retrieveFromAsyncStorage = async ({ key }) => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        return JSON.parse(value);
      }
      return initialValue;
    } catch (error) {
      throw new Error("AsyncStorage retrieve" + error.message);
    }
  };

  const mergeIntoList = async (item) => {
    try {
      const value = await AsyncStorage.getItem(key);
      const jsonArray = JSON.parse(value) || [];
      const distinctArray = jsonArray.filter(
        (storageItem) => storageItem.id != item.id
      );
      await AsyncStorage.setItem(key, JSON.stringify([item, ...distinctArray]));
      await retrieveItemAndUpdateState();
    } catch (error) {
      throw new Error("AsyncStorage merge" + error.message);
    }
  };

  const insert = async (value = initialValue, callback) => {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(value));
      await retrieveItemAndUpdateState();

      if (callback) {
        callback();
      }
    } catch (error) {
      throw new Error("AsyncStorage insert" + error.message);
    }
  };

  return [storageList, mergeIntoList, insert];
}
