import React, {useContext, useEffect, useState} from "react";
import {StyleSheet, View} from "react-native";
import {Button} from "react-native-paper";
import i18n from "i18n-js";
import {format} from "date-fns";

import PinDialog from "../pinDialog/PinDialog";

import {SchedulingContext} from "contexts/SchedulingContext";
import {SnackbarContext} from "contexts/SnackbarContext";
import {StorageContext} from "contexts/StorageContext";

import normalize from "utils/normalize";
import {parseWithNoTZ} from "utils/dateUtils";
import {getHourFormat} from "utils/hourUtils";

const CheckingButtons = ({startedSchedule, nextSchedule}) => {

  const [pinDialogOpened, setPinDialogOpened] = useState(false);

  const currentSchedule = startedSchedule ? startedSchedule : nextSchedule;
  const {id, userId, subject, attendees, startDate, endDate} = currentSchedule;

  const {setNotification} = useContext(SnackbarContext);

  const {
    fetchUserAccount,
    checkoutTimeSchedule,
    setUserActionId,
    setSelectedSchedule,
    clearGenericError,
    userAccount,
    isFetchingUserAccount,
    isCheckingOutTimeSchedule,
    userActionId,
    genericError,
  } = useContext(SchedulingContext);

  const {appStorage} = useContext(StorageContext);

  const {site} = appStorage

  useEffect(() => {
    if (genericError) {
      setNotification(genericError);
      clearGenericError();
    }
  }, [genericError]);

  const parsedStartDate = parseWithNoTZ(startDate);
  const parsedEndDate = parseWithNoTZ(endDate);

  const formattedStartDate = format(
    parsedStartDate,
    getHourFormat(site && site.hourFormat24)
  );

  const formattedEndDate = format(
    parsedEndDate,
    getHourFormat(site && site.hourFormat24)
  );

  const title = `${formattedStartDate} - ${formattedEndDate}`;

  const handleCheckIn = () => {
    setSelectedSchedule({
      schedulingId: id,
      userId,
      title,
      subject,
      attendees,
      hostActionLabel: i18n.t("action_check_in"),
      hostActionIcon: "check",
      isCheckIn: true,
    });
  };

  const handleCheckOut = () => {
    setUserActionId(`checkout_${userId}`);
    fetchUserAccount({
      id: userId,
      callback: togglePinDialog,
    });
  };

  const togglePinDialog = () => {
    if (appStorage?.company?.checkInRequiresAuthentication) {
      setPinDialogOpened(true);
    } else {
      checkingOut();
    }
  };

  const checkingOut = () => {
    setSelectedSchedule(null);
    checkoutTimeSchedule(userId, id);
  };

  return (
    <View style={styles.checksButtonContainer}>
      <Button
        style={styles.checkinButton}
        icon="check"
        mode="contained"
        color="rgba(0, 0, 0, 0.72)"
        onPress={handleCheckIn}
      >
        {i18n.t("action_check_in")}
      </Button>

      {
        startedSchedule &&
        <Button
          style={styles.checkinButton}
          icon="close"
          mode="contained"
          color="rgba(0, 0, 0, 0.72)"
          loading={
            userActionId === `checkout_${userId}` &&
            (isFetchingUserAccount || isCheckingOutTimeSchedule)
          }
          onPress={handleCheckOut}
        >
          {i18n.t("action_check_out")}
        </Button>
      }

      <PinDialog
        pictureUrl={userAccount && userAccount.pictureUrl}
        name={userAccount && userAccount.name}
        pinToValidate={userAccount && userAccount.pin}
        visible={pinDialogOpened}
        setDialogOpen={setPinDialogOpened}
        onConfirmCallback={checkingOut}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  checksButtonContainer: {
    paddingVertical: 8,
    flexDirection: "row",
  },
  checkinButton: {
    alignSelf: "flex-start",
    marginRight: normalize(8),
  },
});

export default CheckingButtons;
