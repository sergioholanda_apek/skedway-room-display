import React from 'react';
import {Caption, Subheading} from "react-native-paper";
import i18n from "i18n-js";

import CustomDialog from "components/common/CustomDialog";

const BookingErrorDialog = ({showErrorMessage, clearErrorMessage, errorMessage}) => {
  return (
    <CustomDialog
      style={{ marginHorizontal: "15%" }}
      visible={showErrorMessage}
      onDismiss={() => clearErrorMessage()}
      confirmAction={() => clearErrorMessage()}
      confirmLabel="Ok"
    >
      <Subheading>{i18n.t("error_dialog")}</Subheading>
      <Caption>{errorMessage}</Caption>
    </CustomDialog>
  );
};

export default BookingErrorDialog;