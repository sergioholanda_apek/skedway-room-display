import React from "react";
import { Image, StyleSheet, TouchableOpacity, View } from "react-native";
import { Chip } from "react-native-paper";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import normalize from "utils/normalize";

const SearchBarGuests = (props) => {
  const { selectedGuests, updateScreen } = props;

  return (
    <TouchableOpacity style={styles.guestsContainer} onPress={updateScreen}>
      <MaterialCommunityIcons
        style={styles.accountPlusIcon}
        name="account-plus"
        size={24}
      />
      <View style={styles.chipContainer}>
        {selectedGuests.map((guest) => {
          return (
            <Chip
              key={`chip_${guest.id}`}
              mode="outlined"
              avatar={<Image source={{ uri: guest.pictureUrl }} />}
              style={styles.chip}
            >
              {guest.name}
            </Chip>
          );
        })}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  accountPlusIcon: {
    justifyContent: "flex-start",
    paddingVertical: 20,
    paddingLeft: normalize(16),
    paddingRight: 16,
  },
  guestsContainer: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "#D5D5D5",
  },
  chipContainer: {
    paddingVertical: 12,
    alignSelf: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    flex: 1,
  },
  chip: {
    marginRight: 8,
    marginVertical: 6,
  },
});

export default SearchBarGuests;
