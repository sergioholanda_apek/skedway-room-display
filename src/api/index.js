import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";

import {BASE_PATH, DEVELOPMENT, URL_DEVELOPMENT, PRODUCTION, URL_PRODUCTION} from "react-native-dotenv";

const initAxios = async () => {
  const appStorage = await AsyncStorage.getItem("@APP_STORAGE");
  const appStorageObject = appStorage != null ? JSON.parse(appStorage) : null;
  const env = appStorageObject?.env;
  if (env === DEVELOPMENT) {
    setDevelopmentApi()
  } else if (env === PRODUCTION) {
    setProductionApi()
  }
}

let axios_ = initAxios();

const setProductionApi = () => {
  axios_ = axios.create({
    baseURL: `${URL_PRODUCTION}${BASE_PATH}`,
  });
}

const setDevelopmentApi = () => {
  axios_ = axios.create({
    baseURL: `${URL_DEVELOPMENT}${BASE_PATH}`,
  });
}

const getAuthToken = async () => {
  const appStorage = await AsyncStorage.getItem("@APP_STORAGE");
  const appStorageObject = appStorage != null ? JSON.parse(appStorage) : null;
  return appStorageObject?.authToken;
}

export {axios_, setProductionApi, setDevelopmentApi, getAuthToken};
