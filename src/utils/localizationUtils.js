import { ptBR, enUS, es } from "date-fns/locale";
import * as Localization from "expo-localization";

const locales = { ptBR, pt: ptBR, enUS, en: enUS, esUS: es };

const fallback = "ptBR";

export function getCurrentLocale() {
  const { locale } = Localization;
  const key = locale.split("-").join("") || fallback;
  return locales[key];
}
