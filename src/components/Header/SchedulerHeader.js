import React, {useContext, useEffect, useState} from "react";
import {View} from "react-native";
import {useTheme,} from "react-native-paper";

import i18n from "i18n-js";
import {format, parse} from "date-fns";
import Availability from "components/ScreenSaver/common/Availability";

import {SchedulingContext} from "contexts/SchedulingContext";
import {SyncCheckInsContext} from "contexts/SyncCheckInsContext";
import {StorageContext} from "contexts/StorageContext";

import useSync from "hooks/useSync";
import usePrevious from "hooks/usePrevious";
import useOrientation from "hooks/useOrientation";

import normalize from "utils/normalize";
import {parseWithNoTZ} from "utils/dateUtils";
import {
  getCurrentStatus,
  getNextMeeting,
  getNowHappeningMeeting,
  getScheduleHost,
  StatusScheduling,
  verifyAtLeastOneCheckIn,
  verifyCheckInAdvance
} from "utils/schedulerUtils";

import {getHourFormat} from "utils/hourUtils";

import Verify from "../../../assets/icons/Verify";
import SchedulingInfoCard from "../common/SchedulingInfoCard";
import Hourglass from "../../../assets/icons/Hourglass";
import XLg from "../../../assets/icons/XLg";

const SchedulerHeader = ({isScreenSaver = false}) => {

  const [occupancy, setOccupancy] = useState("");

  const {todayDayScheduling} = useContext(SchedulingContext);
  const {setCheckIn, checkIn} = useContext(SyncCheckInsContext);
  const {appStorage} = useContext(StorageContext);

  const {space, site} = appStorage;

  const theme = useTheme();
  const orientation = useOrientation();
  const today = useSync();

  const {colors} = theme;
  const isPortrait = orientation === "PORTRAIT";
  const spaceName = space && space.name;
  const startedSchedule = getNowHappeningMeeting(today, todayDayScheduling);
  const nextSchedule = getNextMeeting(today, todayDayScheduling);
  const prevStartedSchedule = usePrevious(startedSchedule);
  const isCheckInAdvance = verifyCheckInAdvance(appStorage, nextSchedule);
  const isThereAtLeastOneCheckIn = verifyAtLeastOneCheckIn(checkIn, startedSchedule, nextSchedule);
  const statusScheduling = getCurrentStatus(startedSchedule, nextSchedule, isCheckInAdvance);

  const hourString = format(today, getHourFormat(site && site.hourFormat24));

  const rawHeight = (startedSchedule || isCheckInAdvance) ? 180 : 110;
  const minHeight = (startedSchedule || isCheckInAdvance) ? 300 : 220;
  const height = isPortrait
    ? Math.max(normalize(rawHeight), minHeight)
    : "100%";

  /**
   * Resets checkIns array if a something happens to
   * the startedSchedule (it has ended or it has just begun etc...)
   */
  useEffect(() => {
    if (
      (prevStartedSchedule &&
        startedSchedule &&
        prevStartedSchedule.id !== startedSchedule?.id) ||
      (!prevStartedSchedule && startedSchedule) ||
      (prevStartedSchedule && !startedSchedule)
    ) {
      setCheckIn([]);
    }
  }, [startedSchedule]);


  const getFormattedDate = (date) => {
    const dateWithNoTZ = parseWithNoTZ(date);
    return format(dateWithNoTZ, getHourFormat(site?.hourFormat24))
  }

  function getEventTitle() {
    const schedule = startedSchedule ?? nextSchedule;
    if (isThereAtLeastOneCheckIn && schedule) {
      const host = getScheduleHost(schedule)
      return host?.name;
    } else {
      return spaceName
    }
  }

  function getAvailability() {
    return (<Availability {...{startedSchedule, nextSchedule, color: colors.text, fontSize: normalize(8)}}/>);
  }

  function getIcon() {
    if (statusScheduling === StatusScheduling.FREE) {
      return (
        <View style={{marginBottom: normalize(14)}}>
          <Verify height={75} width={74} color={colors.darkGreen}/>
        </View>
      )
    } else if (statusScheduling === StatusScheduling.CHECKIN_ADVANCE) {
      return <Hourglass height={120} width={120} color={colors.yellow}/>
    } else if (statusScheduling === StatusScheduling.OCCUPIED) {
      return <XLg height={120} width={120} color={colors.red}/>
    }
  }

  function getColor() {
    if (statusScheduling === StatusScheduling.FREE) {
      return colors.darkGreen;
    } else if (statusScheduling === StatusScheduling.CHECKIN_ADVANCE) {
      return colors.yellow;
    } else if (statusScheduling === StatusScheduling.OCCUPIED) {
      return colors.red;
    }
  }

  function getOccupancy() {
    const {site} = appStorage;

    if (startedSchedule) {
      const {startDate, endDate} = startedSchedule;
      const formattedStartDate = getFormattedDate(startDate);
      const formattedEndDate = getFormattedDate(endDate);
      return i18n.t("screenSaver.time_interval", [formattedStartDate, formattedEndDate]);
    } else if (nextSchedule) {
      const formattedStartDate = getFormattedDate(nextSchedule?.startDate);
      return i18n.t("screenSaver.free_until", [formattedStartDate]);
    } else {
      const {closingTime} = site;
      const parsedClosingTime = parse(closingTime, "HH:mm:ss", new Date());
      const formattedEndDate = format(parsedClosingTime, getHourFormat(site?.hourFormat24))
      return i18n.t("screenSaver.free_until", [formattedEndDate]);
    }
  }

  function getStatus() {
    if (statusScheduling === StatusScheduling.FREE) {
      return i18n.t("free_label");
    } else if (statusScheduling === StatusScheduling.CHECKIN_ADVANCE) {
      return i18n.t("click_checkin_label");
    } else if (statusScheduling === StatusScheduling.OCCUPIED) {
      return i18n.t("occupied_label");
    }
  }

  const getProps = () => (
    {
      eventTitle: getEventTitle(),
      icon: getIcon(),
      status: getStatus(),
      occupancy: getOccupancy(),
      availability: getAvailability(),
      statusColor: getColor(),
      showCapacity: true,
      showStatusIcon: true,
      isScreenSaver
    }
  )

  return (
    <SchedulingInfoCard {...getProps()} />
  )

}

export default SchedulerHeader;
