import React, {useState} from "react";
import update from "immutability-helper";

export const UserSuggestionContext = React.createContext({});

const UserSuggestionProvider = ({children}) => {

  const [selectedUsers, setSelectedUsers] = useState([]);

  function removeUser(user) {
    const index = selectedUsers.indexOf(user);
    const updatedSelectedUser = update(selectedUsers, { $splice: [[index, 1]] });
    setSelectedUsers(updatedSelectedUser);
  }

  return (
    <UserSuggestionContext.Provider
      value={{selectedUsers, setSelectedUsers, removeUser}}
    >
      {children}
    </UserSuggestionContext.Provider>
  );
};

export default UserSuggestionProvider;