import React, {createContext, useContext, useState} from "react";
import filter from "lodash/filter";
import i18n from "i18n-js";
import find from "lodash/find";
import indexOf from "lodash/indexOf";
import update from "immutability-helper";

import {axios_, getAuthToken} from "api";

import {LoadingContext} from "contexts/LoadingContext";

import {mapDayColors} from "utils/dateUtils";
import BookingError from "utils/createSchedulingErrors";

import {
  CREATE_SCHEDULING,
  DO_CANCEL,
  DO_CHECK_OUT,
  GET_USER_ACCOUNT,
  LIST_OCCUPATION,
  LIST_SCHEDULINGS,
  SUGGEST_USER
} from "api/endpoints";

import {isToday} from "date-fns";

export const SchedulingContext = createContext({});

let authToken = undefined;

const SchedulingProvider = ({children}) => {

  const [currentDay, setCurrentDay] = useState(null);

  const [isSyncing, setSyncing] = useState(false);
  const [userActionId, setUserActionId] = useState(null);

  const [selectedSchedule, setSelectedSchedule] = useState(null);

  const [dayScheduling, setDayScheduling] = useState([]);
  const [todayDayScheduling, setTodayDayScheduling] = useState([]);
  const [isFetchingDayScheduling, setFetchingDayScheduling] = useState(false);

  const [occupancy, setOccupancy] = useState(null);
  const [isFetchingMonthOccupancy, setFetchingMonthOccupancy] = useState(false);

  const [userSuggestions, setUserSuggestions] = useState(null);
  const [isFetchingUserSuggestions, setFetchingUserSuggestions] = useState(false);

  const [userAccount, setUserAccount] = useState(null);
  const [isFetchingUserAccount, setFetchingUserAccount] = useState(false);
  const [isBookingTimeSchedule, setBookingTimeSchedule] = useState(false);
  const [hasBookedSuccessfully, setHasBookedSuccessfully] = useState(false);
  const [bookingError, setBookingError] = useState(null);
  const [isCheckingOutTimeSchedule, setCheckingOutTimeSchedule] = useState(false);
  const [genericError, setGenericError] = useState(null);
  const [cancelingTimeSchedule, setCancelingTimeSchedule] = useState(false);

  const {setLoadingResource} = useContext(LoadingContext);

  function setInitialState() {
    setCurrentDay(null);
    setSelectedSchedule(null);
    setBookingError(null);
    setGenericError(null);
    setSyncing(false);
    setOccupancy(null)
    setFetchingMonthOccupancy(false);
    setDayScheduling([]);
    setTodayDayScheduling([]);
    setFetchingDayScheduling(false);
    setUserSuggestions([]);
    setFetchingUserSuggestions(false);
    setUserAccount(null);
    setFetchingUserAccount(false);
    setBookingTimeSchedule(false);
    setHasBookedSuccessfully(false);
    setCancelingTimeSchedule(false);
    setCheckingOutTimeSchedule(false);
    setUserActionId(null);
  }

  async function fetchMonthOccupancy(date, index, openingTime, closingTime) {
    setLoadingResource(true);
    setFetchingMonthOccupancy(true);
    authToken = await getAuthToken();
    try {

      const response = await axios_.get(LIST_OCCUPATION, {
        params: {
          authToken,
          date,
        },
      });

      const occupancy_ = {
        ...occupancy,
        [index]: mapDayColors(response.data, openingTime, closingTime),
      }

      setOccupancy(occupancy_)
    } catch (error) {
      setFetchingMonthOccupancy(false);
      setLoadingResource(false);
      throw new Error(error.message);
    } finally {
      setFetchingMonthOccupancy(false);
      setLoadingResource(false);
    }
  }

  async function fetchDayScheduling() {
    setFetchingDayScheduling(true);
    authToken = await getAuthToken();

    try {
      const response = await axios_.get(LIST_SCHEDULINGS, {
        params: {
          authToken,
          date: currentDay,
        },
      });

      if (response) {
        if (isToday(currentDay)) {
          const filteredTodayDayScheduling = filter(response.data, (o) => o.endDate === o.checkoutDate);
          setTodayDayScheduling(filteredTodayDayScheduling);
        } else {
          setDayScheduling(response.data);
        }
      }
    } catch (error) {
      throw new Error(error.message);
    } finally {
      setFetchingDayScheduling(false);
    }
  }

  async function fetchUserSuggestions(query, limit = 12) {
    if (!query) {
      setUserSuggestions([]);
      return;
    }

    try {
      setFetchingUserSuggestions(true);
      authToken = await getAuthToken();

      const response = await axios_.get(SUGGEST_USER, {
        params: {
          authToken,
          query,
          limit,
        },
      });

      setUserSuggestions(response.data);
    } catch (error) {
      throw new Error(error.message);
    } finally {
      setFetchingUserSuggestions(false);
    }
  }

  async function fetchUserAccount(id, callback) {
    setFetchingUserAccount(true);
    authToken = await getAuthToken();

    try {
      const response = await axios_.get(GET_USER_ACCOUNT, {
        params: {authToken, id},
      });

      setUserAccount(response.data);

      if (callback) {
        callback()
      }
    } catch (error) {
      throw new Error(error.message);
    } finally {
      setFetchingUserAccount(false);
    }
  }

  async function bookTimeSchedule(isItToday, startDate, endDate, userId, attendees, subject) {
    setLoadingResource({show: true});
    setBookingTimeSchedule(true);
    authToken = await getAuthToken();

    try {
      const response = await axios_.post(CREATE_SCHEDULING, null,
        {
          params: {
            authToken,
            scheduling: {
              subject,
              startDate,
              endDate,
              userId,
              attendees,
            },
          },
        }
      );

      if (isItToday) {
        const todayDayScheduling_ = [...todayDayScheduling, response.data];
        setTodayDayScheduling(todayDayScheduling_);
      } else {
        const dayScheduling_ = [...dayScheduling, response.data];
        setDayScheduling(dayScheduling_)
      }

      setHasBookedSuccessfully(true);
    } catch (error) {
      let {errorCode = "", errorValue = ""} = error?.response?.data;
      if (errorCode === "space_not_available") {
        errorCode = "error_space_not_available";
        errorValue = 1;
      }
      setBookingError(BookingError({errorCode, errorValue}));
      setLoadingResource({show: false});
    } finally {
      setBookingTimeSchedule(false);
      setLoadingResource({show: false});
    }
  }

  async function checkoutTimeSchedule(userId, schedulingId) {
    setCheckingOutTimeSchedule(true);
    authToken = await getAuthToken();

    try {
      await axios_.post(DO_CHECK_OUT, null, {
        params: {
          authToken,
          userId,
          schedulingId,
        },
      });

      const schedule = find(todayDayScheduling, {id: schedulingId});
      const idx = indexOf(todayDayScheduling, schedule);
      const todayDayScheduling_ = update(todayDayScheduling, {$splice: [[idx, 1]]});

      setTodayDayScheduling(todayDayScheduling_)
    } catch (error) {
      let {errorCode = "error_http_500"} = error.response?.data;
      if (errorCode === "check_out_after_end") {
        errorCode = "error_check_out_after_end";
      }
      setGenericError(i18n.t(errorCode));
    } finally {
      setCheckingOutTimeSchedule(false);
    }
  }

  async function cancelTimeSchedule(userId, schedulingId, isItToday, callback) {
    setCancelingTimeSchedule(true);
    authToken = await getAuthToken();

    try {
      await axios_.post(DO_CANCEL, null, {
        params: {
          authToken,
          userId,
          schedulingId,
        },
      });

      if (isItToday) {
        const schedule = find(todayDayScheduling, {id: schedulingId});
        const idx = indexOf(todayDayScheduling, schedule);
        const todayDayScheduling_ = update(todayDayScheduling, {$splice: [[idx, 1]]});

        setTodayDayScheduling(todayDayScheduling_)
      } else {
        const dayScheduling_ = dayScheduling.filter(schedule => schedule.id !== schedulingId);
        setDayScheduling(dayScheduling_);
      }

      if (callback) {
        callback();
      }
    } catch (error) {
      throw new Error(error.message);
    } finally {
      setCancelingTimeSchedule(false);
    }
  }

  function clearGenericError() {
    setGenericError(null);
  }

  return (
    <SchedulingContext.Provider
      value={{
        isSyncing,
        setSyncing,
        userActionId,
        setUserActionId,
        selectedSchedule,
        setSelectedSchedule,
        currentDay,
        setCurrentDay,
        dayScheduling,
        setDayScheduling,
        todayDayScheduling,
        setTodayDayScheduling,
        isFetchingDayScheduling,
        setFetchingDayScheduling,
        occupancy,
        setOccupancy,
        isFetchingMonthOccupancy,
        setFetchingMonthOccupancy,
        userSuggestions,
        setUserSuggestions,
        isFetchingUserSuggestions,
        setFetchingUserSuggestions,
        userAccount,
        setUserAccount,
        isFetchingUserAccount,
        setFetchingUserAccount,
        isBookingTimeSchedule,
        setBookingTimeSchedule,
        hasBookedSuccessfully,
        setHasBookedSuccessfully,
        bookingError,
        setBookingError,
        isCheckingOutTimeSchedule,
        setCheckingOutTimeSchedule,
        genericError,
        setGenericError,
        cancelingTimeSchedule,
        setCancelingTimeSchedule,
        setInitialState,
        fetchMonthOccupancy,
        fetchDayScheduling,
        fetchUserSuggestions,
        fetchUserAccount,
        bookTimeSchedule,
        checkoutTimeSchedule,
        cancelTimeSchedule,
        clearGenericError,
      }}
    >
      {children}
    </SchedulingContext.Provider>
  )
}

export default SchedulingProvider;
