import React, {useContext} from "react";

import {StorageContext} from "contexts/StorageContext";
import {SyncCheckInsContext} from "contexts/SyncCheckInsContext";
import {SyncContext} from "contexts/SyncContext";

import useSync from "hooks/useSync";

const BackgroundSync = () => {

  const {appStorage} = useContext(StorageContext);
  const {sync} = useContext(SyncContext);
  const {checkInsList, clearCheckInsList, syncCheckIns} = useContext(SyncCheckInsContext);

  const syncCallback = () => {
    const {authToken} = appStorage;

    if (!authToken) return;

    sync();

    const onSuccessCallback = () => clearCheckInsList();
    syncCheckIns({checkIns: checkInsList, onSuccessCallback});
  };

  useSync(syncCallback);

  return null;
};

export default BackgroundSync;
