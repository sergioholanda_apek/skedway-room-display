import React, {createContext, useContext, useState} from "react";
import i18n from "i18n-js";

import {axios_} from "api";
import {LIST_COMPANY, LOGIN, LIST_SPACES, LOGIN_SPACE, LOGIN_ADMIN} from "api/endpoints";
import {DEVICE_TYPE} from "react-native-dotenv";

import {LoadingContext} from "contexts/LoadingContext";

export const LoginFlowContext = createContext({});

const OFFLINE_USER = "root@apektelecom.com";
const OFFLINE_PASSWORD = "apektelecom@9917";

const LoginFlowProvider = ({children}) => {

  const EMAIL_PATTERN = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

  const [isLoading, setLoading] = useState(false);

  //Welcome screen
  const [emailError, setEmailError] = useState(null);
  const [validEmail, setValidEmail] = useState(false);
  const [accounts, setAccounts] = useState([]);

  //Password screen
  const [passwordError, setPasswordError] = useState(null);
  const [validPassword, setValidPassword] = useState(false);
  const [userCompany, setUserCompany] = useState(null);

  //Room selection screen
  const [roomError, setRoomError] = useState("");
  const [space, setSpace] = useState(null);
  const [spaces, setSpaces] = useState(null);
  const [listEnd, setListEnd] = useState(false);
  const [isFetchingSpaces, setFetchingSpaces] = useState(false);

  //Auth form
  const [isFetchingAdminLogin, setFetchingAdminLogin] = useState(false);
  const [logoutSuccessfully, setLogoutSuccessfully] = useState(false);

  const {setLoadingResource} = useContext(LoadingContext);

  function setInitialState() {
    setLoading(false);
    setValidEmail(false);
    setEmailError(null);
    setValidPassword(false);
    setPasswordError(null);
    setRoomError("");
    setSpace(null)
  }

  /**
   * Welcome screen next button action
   *
   * @param email email entered by user
   * @returns {Promise<void>}
   */
  async function verifyEmailAndFetchAccounts(email) {
    setLoadingResource({show: true});
    if (!email) {
      setEmailError(i18n.t("error_field_required"));
      setValidEmail(false);
    } else if (!EMAIL_PATTERN.test(email)) {
      setEmailError(i18n.t("login.invalidEmail"));
      setValidEmail(false);
    } else {
      try {
        const response = await axios_.get(LIST_COMPANY, {
          params: {email},
        });
        setAccounts(response.data);
        setEmailError(null);
        setValidEmail(true);
      } catch (error) {
        errorHandler(error, setEmailError);
        setValidEmail(false);
        setLoadingResource({show: false});
      } finally {
        setLoadingResource({show: false});
      }
    }
  }

  /**
   * Password screen login button action
   *
   * @returns {(function({email: *, password: *, companyId: *}): Promise<void>)|*}
   */
  async function verifyPasswordAndLogin(email, password, companyId) {
    setLoadingResource({show: true});
    if (!password) {
      setPasswordError(i18n.t("error_field_required"));
      setValidPassword(false);
      setLoadingResource({show: false});
    } else {
      try {
        const response = await axios_.get(LOGIN, {
          params: {email, password, companyId},
        });
        setUserCompany(response.data);
        setPasswordError(null);
        setValidPassword(true);
      } catch (error) {
        errorHandler(error, setPasswordError);
        setValidPassword(false);
        setLoadingResource({show: false});
      } finally {
        setLoadingResource({show: false});
      }
    }
  }

  async function fetchSpaces(companyId, site, limit, query, offset) {
    setLoadingResource({show: true});
    const companySiteId = site && site.id;

    try {
      const isPagination = offset > 0;

      if (!isPagination) {
        setFetchingSpaces(true);
      }

      const response = await axios_.get(LIST_SPACES, {
        params: {
          companyId,
          companySiteId,
          limit,
          query,
          offset,
        },
      });

      const spaces_ = isPagination && Array.isArray(spaces) ? spaces.concat(response.data) : response.data;
      setSpaces(spaces_);
      setListEnd(isPagination && !response.data.length);
    } catch (error) {
      setRoomError(error.message);
      setLoadingResource({show: false});
    } finally {
      setFetchingSpaces(false);
      setLoadingResource({show: false});
    }
  }

  async function loginSpace(spaceId) {
    setLoadingResource({show: true});
    try {
      setLoading(true);

      const response = await axios_.get(LOGIN_SPACE, {
        params: {
          spaceId,
          deviceType: DEVICE_TYPE,
        },
      });

      setSpace(response.data)
    } catch (error) {
      setRoomError(i18n.t("error_space_login"));
      setLoadingResource({show: false});
    } finally {
      setLoading(false);
      setLoadingResource({show: false});
    }
  }

  async function loginAdmin(email, password, companyId) {
    setLoadingResource({show: true});
    setEmailError("");
    setValidEmail(false);
    setPasswordError("");
    setValidPassword(false);

    if (!email || !password) {
      if (!email) {
        setEmailError(i18n.t("error_field_required"));
      }
      if (!password) {
        setPasswordError(i18n.t("error_field_required"));
      }
      return;
    }

    setFetchingAdminLogin(true);

    try {
      await axios_.get(LOGIN_ADMIN, {
        params: {
          email,
          password,
          companyId,
        },
      });

      setLogoutSuccessfully(true);
      setValidPassword(true);
      setFetchingAdminLogin(false);

    } catch (error) {
      const {response} = error;

      if (response?.status === 401) {
        setEmailError(i18n.t("error_login_incorrect"));
      } else if (response?.status === 500) {
        if (email === OFFLINE_USER && password === OFFLINE_PASSWORD) {
          setEmailError("");
          setValidEmail(false);
          setPasswordError("");
          setValidPassword(false);
        }
        setEmailError(i18n.t("error_login_incorrect"));
        setLoadingResource({show: false});
      } else if (response?.status === 403) {
        setEmailError(i18n.t("error_permissions_not_granted"));
      }
    } finally {
      setLoadingResource({show: false});
    }
  }

  function errorHandler(error, setError) {
    const {data, status} = error?.response;

    switch (status) {
      case 400 | 407 | 500:
        setError(i18n.t(`error_http_${status}`));
        break;
      case 401 | 403:
        setError(i18n.t(`login.${data}`));
        break;
      case 404:
        setError(i18n.t(`error_${data.errorCode}`));
        break;
      default:
        setError(i18n.t("error_unknown"));
        break;
    }
  }

  return (
    <LoginFlowContext.Provider
      value={{
        setInitialState,
        isLoading,
        emailError,
        validEmail,
        accounts,
        passwordError,
        validPassword,
        userCompany,
        verifyEmailAndFetchAccounts,
        verifyPasswordAndLogin,
        fetchSpaces,
        loginSpace,
        roomError,
        space,
        spaces,
        setSpaces,
        listEnd,
        isFetchingSpaces,
        loginAdmin,
        logoutSuccessfully,
        isFetchingAdminLogin,
        setLogoutSuccessfully
      }}
    >
      {children}
    </LoginFlowContext.Provider>
  )
}

export default LoginFlowProvider;
