import React from "react";
import {StyleSheet, Text, View} from "react-native";

import normalize from "utils/normalize";
import i18n from "i18n-js";
import {useTheme} from "react-native-paper";

const WelcomeMessage = () => {

  const {colors} = useTheme();

  return (
    <View style={styles.mainView}>
      <Text style={[styles.helloTo, styles.textCenterBold, {color: colors.text}]}>
        {i18n.t("login.hello")}
      </Text>
      <Text style={[styles.welcome, styles.textCenterBold, {color: colors.text}]}>
        {i18n.t("login.welcome")}
      </Text>
      <Text style={[styles.helloTo, styles.textCenterBold, {color: colors.text}]}>
        {i18n.t("login.to")}
      </Text>
    </View>
  );
};

const FONT_SIZE = 16;

const styles = StyleSheet.create({
  mainView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignSelf: 'center',
    marginBottom: 10,
  },
  helloTo: {
    fontSize: normalize(FONT_SIZE),
    fontFamily: 'Manrope_300Light',
  },
  welcome: {
    fontSize: normalize(FONT_SIZE),
    fontFamily: 'LibreBaskerville_400Regular_Italic'
  },
  textCenterBold: {
    fontWeight: "bold",
    alignSelf: "center",
  },
});

export default WelcomeMessage;
