import login from '../login/en.js'
import screenSaver from '../screenSaver/en.js'

export default {
  login: {...login},
  screenSaver: {...screenSaver},

  app_name: "Skedway SK8",
  app_website: "https://www.skedway.com",
  app_description:
    "The corporate spaces management platform, and work positions in a simple and organized way, and realize that managing spaces is the smart way to save resources and increase productivity.",

  action_cancel: "Cancel",
  action_check_for_update: "Check for update",
  action_check_in: "Check-in",
  action_check_out: "Check-out",
  action_check_permissions: "Check permissions",
  action_close: "Close",
  action_confirm: "Confirm",
  action_disable_sound: "Disable sound",
  action_dismiss: "Dismiss",
  action_drawer_close: "Close menu",
  action_drawer_open: "Open menu",
  action_enable_sound: "Enable sound",
  action_factory_reset: "Factory reset",
  action_go: "Go",
  action_remove: "Remove",
  action_report: "Report",
  action_reset: "Reset",
  action_retry: "Retry",
  action_save: "Save",
  action_schedule: "Schedule",
  action_show_password: "Show password",
  action_sign_in: "Sign-in",
  action_swipe: "Swipe",
  action_synchronize: "Synchronize",

  confirm_factory_reset:
    "Confirms the factory reset?\nAll app data will be erased.",

  env_production: "Production",
  env_development: "Development",
  env_local: "Local",

  error_user_amenity_required: "User and amenities are required",
  error_amenity_required: "Amenity is required",
  error_camera_access_not_granted: "Camera access not granted",
  error_card_number_not_found: "Card number not found",
  error_check_out_after_end: "Scheduling already closed",
  error_check_out_before_start: "Scheduling not opened for check-out",
  error_company_plan_expired: "Your company plan expired",
  error_company_removed: "Your company was removed",
  error_connect_timeout: "Your connection expired",
  error_connection: "No Internet connection",
  error_field_required: "Field is required",
  error_host_not_found: "Host not found",
  error_http_400: "Bad request",
  error_http_401: "Invalid user or password",
  error_http_403: "Access forbidden",
  error_http_404: "Page not found",
  error_http_407: "Proxy authentication required",
  error_http_500: "Internal server error",
  error_invalid_card_number: "Invalid card number",
  error_invalid_email: "Invalid email",
  error_invalid_boarding_pass: "Invalid boarding pass",
  error_invalid_pin: "Invalid PIN",
  error_login_incorrect: "Invalid user or password",

  error_password_incorrect_title: "Incorrect password",
  error_password_incorrect_message: "The password you entered is incorrect. Please try again.",

  error_login_offline_incorrect: "Invalid offline user or password",
  error_no_internet_connection: "No Internet connection",
  error_permission_not_granted:
    "Permissions not granted. The app will be closed.",
  error_permissions_not_granted: "Not all permissions have been granted",
  error_recurrence_max_days:
    "You exceeded the %{input} days limit for recurrence",
  error_space_min_occupation: "Limit of %{input} space occupation not reached",
  error_space_not_available: "Space not available",
  error_unknown: "Unknown error. Please try again later.",
  error_user_blocked: "User blocked after too many attempts to access",
  error_user_not_administrator: "Este usuário não é administrador",
  error_user_not_enough_level: "You are not allowed to schedule this space",
  error_user_not_found: "User not found",
  error_user_time_exceeded:
    "You have exceeded the %{input} hours limit for a day",
  error_dialog: "An error has occurred",

  format_date: "MMMM dd, yyyy",
  format_date_and_time: "MMMM, dd yyyy 'at' HH:mm",
  format_date_weekday: "EEEE, MMMM dd yyyy",
  format_month_year: "MMMM yyyy",

  level_user_can: "All may schedule",
  level_manager_can: "Manager can schedule",
  level_director_can: "Directors may schedule",
  level_president_can: "Presidents may schedule",
  level_not_schedulable: "Not schedulable",

  prompt_amenities: "Amenities",
  prompt_domain: "Domain",
  prompt_email: "Email",
  prompt_enter_pin: "Enter your PIN",
  prompt_environment: "Environment",
  prompt_invite_attendees: "Invite attendees",
  prompt_add_subject: "Add subject (optional)",
  prompt_issue_description: "Issue description",
  prompt_items: "Items",
  prompt_password: "Password",
  prompt_search_for_attendee: "Search for attendee",
  prompt_search_for_user: "Search for name or email",
  prompt_site: "Site",
  prompt_space: "Space",
  prompt_user: "User",

  success_all_permissions_granted: "All permissions granted",
  success_app_synchronized: "App successfully synchronized",
  fail_app_synchronized: "App failed when trying to synchronize",
  success_app_synchronizer_running: "App synchronizer is running.",
  success_cancellation: "Scheduling successfully canceled",
  success_internet_connection: "Connected to Internet",
  success_issue_report: "Issue successfully reported",
  success_scheduling: "Scheduling successfully done",

  title_activity_about: "About",
  title_activity_issue_report_new: "Report issue",
  title_activity_space: "Select a space",
  title_activity_user: "Access user",
  title_activity_web_view: "Webview",
  title_about: "About",
  title_calendar: "Calendar",
  title_home: "Home",
  title_issues: "Issues",
  title_issue: "Issue",
  title_occupation_by_day: "occupation by day",
  title_skedway_crash: "Unexpected error in Skedway",

  // General texts
  Accepted: "Accepted",
  Maybe: "Maybe",
  Added_attendees: "Added attendees",
  Air_conditioning: "Air conditioning",
  Amenities: "Amenities",
  App: "App",
  Attendee: "Attendee",
  Aware: "Aware",
  Capacity: "Capacity",
  confirm_space: "Confirms space %{input}",
  Declined: "Declined",
  Environment_x: "Environment %{input}",
  Fixed: "Fixed",
  Free: "Free",
  Free_time: "Free time",
  Furniture: "Furniture",
  go_to_next_page: "Go to next page",
  go_to_previous_page: "Go to previous page",
  Time: "Time",
  Guest: "Guest",
  Guests: "Guests",
  Host: "Host",
  Lighting: "Lighting",
  Next_Meeting: "Next Meeting",
  New: "New",
  No_attendee_for_card_uid: "No attendee for this card",
  No_opened_issue: "No opened issue",
  No_operation_available: "No operation available",
  No_space_found: "No space found",
  Not_responded: "Not responded",
  Now_happening: "Now happening",
  Present: "Present",
  Privacy_policy: "Privacy policy",
  Reported_issues: "Reported issues",
  Schedule_for: "Schedule for %{input}",
  Scheduled_for_user: "Scheduled for %{input}",
  Select_the_site: "Select the site",
  Settings: "Settings",
  Site: "Site",
  Space: "Space",
  skedway_restart_in: "Skedway will restart in %{input} seconds",
  Tentative: "Tentative",
  today: "Today",
  User_picture: "User picture",
  Version_x: "Version %{input}",
  Wait_loading: "Loading…",
  Welcome_attendee: "WelcomeMessage %{input}",

  // Plurals
  one: "%{input} seat",
  other: "%{input} seats",
  booked_hour: "%{0} hour",
  booked_hours: "%{0} hours",

  // Week days
  day_of_week_mon: "Monday",
  day_of_week_tue: "Tuesday",
  day_of_week_wed: "Wednesday",
  day_of_week_thu: "Thursday",
  day_of_week_fri: "Friday",
  day_of_week_sat: "Saturday",
  day_of_week_sun: "Sunday",

  // Period codes
  day_for_meeting_room: "day for meeting room",
  week_for_meeting_room: "week for meeting room",
  month_for_meeting_room: "month for meeting room",
  day_for_desk: "day for desk",
  week_for_desk: "week for desk",
  month_for_desk: "month for desk",

  // Errors
  error_scheduling_limit_exceeded:
    "A space on your schedule has exceeded the schedule limit set on the Site %{0}",
  error_user_weekday_denied:
    "Scheduling not allowed for %{0} as per scheduling restriction per day of the week (%{1})",
  user_time_exceeded: "You exceeded the %{0} limit per %{1} (user rule)",
  error_space_time_exceeded:
    "You exceeded the %{0} limit per %{1} (space rule)",
  error_space_weekday_denied:
    "Scheduling not allowed for %{0} as per scheduling restriction per day of the week (%{1})",
  text_space_scheduled_by: "The space %{0} can only be scheduled by %{1}",
  error_concurrent_space: "The space %{0} do not allow concurrent spaces",
  max_days_scheduling: "You have exceeded the %{0} days limit for scheduling",
  text_could_not_parse_company_api:
    "Could not parse the API of your company (%{0})",
  text_could_not_schedule_company_api:
    "%{0}\n\n* scheduling not allowed by your company",
  text_could_not_read_company_api:
    "Could not read the API of your company (%{0}: %{1})",
  text_could_not_query_company_api: "%{0}\n\n* internal code %{1}",
  error_space_login: "Unable to get space login information",
  error_time_interval_hour_op_allowed_with_text: "%{0} allows scheduling of %{1} in the intervals %{2}",
  error_time_interval_hour_op_allowed_no_text: "%{0} do not allow schedulings of %{1}",
  error_time_interval_hour_op_denied: "%{0} do not allow schedulings of %{1} in the intervals %{2}",

  places: {
    "one": "1 place",
    "other": "%{count} places"
  },

  placesAvailability: "This %{0} holds %{1} ",
  room: "room",
  desk: "desk",
  peoples: {
    "one": "1 person",
    "other": "%{count} peoples"
  },
  object_created: "%{0} created successfully!",
  hour_from: "Begin",
  hour_to: "End",
  booking_required_field_title_error: "Host field is required",
  free_label: "Free",
  click_checkin_label: "Click to Check-in",
  occupied_label: "Occupied",
  start_in_label: "Starts in %{time}"
};
