import * as React from "react"
import Svg, {Path} from "react-native-svg"

const Power = ({height, width, color}) => {

  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      fill={color}
      viewBox="0 0 15 17"
      width={height}
      height={width}
    >
      <Path d="M7.5 1v7h1V1h-1z" />
      <Path d="M3 8.812a4.999 4.999 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812z" />
    </Svg>
  )
}

export default Power;
