import * as React from "react"
import {SvgXml} from "react-native-svg"

const xml = `
<svg width="655" height="190" viewBox="0 0 655 190" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M-34.2107 443.116L154.767 526.635L154.767 135.897L97.7992 144.913L-34.2107 117.882L-34.2107 443.116Z" fill="url(#paint0_linear_2_6751)" fill-opacity="0.93"/>
<path d="M363.925 -34.4297V104.217L173.676 19.0362V-120.174L363.925 -34.4297Z" fill="url(#paint1_linear_2_6751)"/>
<path d="M-35.3608 -21.3743V117.108L173.673 18.3083V-120.174L-35.3608 -21.3743Z" fill="url(#paint2_linear_2_6751)" fill-opacity="0.62"/>
<path d="M154.652 472.162L365.094 350.552L363.929 104.217L154.652 193.176L154.652 472.162Z" fill="url(#paint3_linear_2_6751)"/>
<path d="M-34.7183 117.6L176.299 16.6842L363.928 104.212L154.867 205.816L-34.7183 117.6Z" fill="url(#paint4_linear_2_6751)"/>
<g filter="url(#filter0_d_2_6751)">
<rect width="153.206" height="66.1196" rx="0.718382" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 313.602 80.8976)" fill="url(#paint5_linear_2_6751)"/>
</g>
<rect width="35.3106" height="35.2957" transform="matrix(0.906653 -0.421877 0.906653 0.421877 128.456 104.648)" fill="url(#paint6_linear_2_6751)"/>
<rect width="16.1562" height="32.8067" transform="matrix(0.906653 -0.421877 0.906653 0.421877 172.667 59.9462)" fill="url(#paint7_linear_2_6751)"/>
<path d="M160.49 84.9811V118.987L128.476 104.03V70.024L160.49 84.9811Z" fill="white"/>
<path d="M237.064 31.226V64.9334L192.146 45.3144L193.36 11.0315L237.064 31.226Z" fill="url(#paint8_linear_2_6751)"/>
<defs>
<filter id="filter0_d_2_6751" x="112.497" y="16.441" width="203.357" height="100.794" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="5.98652"/>
<feGaussianBlur stdDeviation="1.31703"/>
<feComposite in2="hardAlpha" operator="out"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_2_6751"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_2_6751" result="shape"/>
</filter>
<linearGradient id="paint0_linear_2_6751" x1="546.321" y1="237.49" x2="228.709" y2="621.493" gradientUnits="userSpaceOnUse">
<stop offset="0.0519365" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white" stop-opacity="0"/>
</linearGradient>
<linearGradient id="paint1_linear_2_6751" x1="662.143" y1="182.739" x2="39.9369" y2="-339.066" gradientUnits="userSpaceOnUse">
<stop offset="0.0519365" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white" stop-opacity="0"/>
</linearGradient>
<linearGradient id="paint2_linear_2_6751" x1="510.367" y1="-76.7538" x2="-82.6887" y2="115.34" gradientUnits="userSpaceOnUse">
<stop offset="0.395833" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white" stop-opacity="0"/>
</linearGradient>
<linearGradient id="paint3_linear_2_6751" x1="680.073" y1="-94.1688" x2="789.481" y2="224.784" gradientUnits="userSpaceOnUse">
<stop offset="0.0519365" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint4_linear_2_6751" x1="488.423" y1="-86.7408" x2="99.4813" y2="293.378" gradientUnits="userSpaceOnUse">
<stop offset="0.0519365" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint5_linear_2_6751" x1="33.0288" y1="77.4933" x2="31.1125" y2="10.2957" gradientUnits="userSpaceOnUse">
<stop stop-color="white"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint6_linear_2_6751" x1="-107.406" y1="36.9496" x2="43.5057" y2="22.9225" gradientUnits="userSpaceOnUse">
<stop stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint7_linear_2_6751" x1="68.0854" y1="15.3049" x2="-1.01613" y2="0.536709" gradientUnits="userSpaceOnUse">
<stop offset="0.0519365" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint8_linear_2_6751" x1="47.7699" y1="36.1777" x2="225.429" y2="-28.0718" gradientUnits="userSpaceOnUse">
<stop offset="0.0519365" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
</defs>
</svg>

`;

export default (props) => <SvgXml {...props} xml={xml} />;
