import React from "react";
import { TouchableOpacity, StyleSheet } from "react-native";
import { Title, useTheme } from "react-native-paper";
import { CheckBox } from "react-native-elements";
import normalize from "utils/normalize";

const CustomCheckbox = ({ id, title, checkCallback, uncheckCallback }) => {
  const { colors } = useTheme();
  const [checked, setChecked] = React.useState(false);

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        if (!checked) {
          checkCallback(id);
        } else {
          uncheckCallback(id);
        }
        setChecked(!checked);
      }}
    >
      <CheckBox
        disabled
        checkedColor={colors.primary}
        checkedIcon="check-square"
        checked={checked}
      />

      <Title>{title}</Title>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: normalize(16),
    left: -18,
  },
});

export default CustomCheckbox;
