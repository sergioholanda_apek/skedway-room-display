import React, {useContext, useState} from "react";
import {Keyboard, StyleSheet, View} from "react-native";
import {Button, Card, Title} from "react-native-paper";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import i18n from "i18n-js";

import {SchedulingContext} from "contexts/SchedulingContext";
import {StorageContext} from "contexts/StorageContext";

import normalize from "utils/normalize";
import PinDialog from "../../pinDialog/PinDialog";

const BookingTitle = (props) => {
  const {
    title,
    buttonString,
    buttonIsLoading,
    onDismiss,
    validationAction,
    selectedUser,
    setErrorRequiredField,
    cleanUp,
    pinAction,
  } = props;

  const [pinDialogOpened, setPinDialogOpened] = useState(false);

  const {userAccount, isFetchingUserAccount, fetchUserAccount} = useContext(SchedulingContext);

  const {appStorage} = useContext(StorageContext);

  const handleBooking = () => {
    if (selectedUser) {
      cleanUp();

      if (validationAction) {
        const isValidated = validationAction();
        if (!isValidated) {
          return;
        }
      }

      fetchUserAccount({
        id: selectedUser.id,
        callback: togglePinDialog,
      });
    } else {
      setErrorRequiredField(i18n.t("error_field_required"));
    }
  };

  const togglePinDialog = () => {
    if (appStorage?.company?.checkInRequiresAuthentication) {
      Keyboard.dismiss();
      setPinDialogOpened(true);
    } else {
      pinAction(selectedUser);
    }
  };

  return (
    <>
      <Card style={styles.cardContainer}>
        <View style={styles.titleContainer}>
          <MaterialCommunityIcons
            style={styles.leftArrowIcon}
            onPress={() => {
              onDismiss();
            }}
            name="arrow-left"
          />
          <Title style={styles.title} ellipsizeMode="tail" numberOfLines={1}>
            {title}
          </Title>
          <Button
            loading={isFetchingUserAccount || buttonIsLoading}
            onPress={handleBooking}
            mode="contained"
            style={styles.button}
          >
            {buttonString}
          </Button>
        </View>
      </Card>

      <PinDialog
        pictureUrl={userAccount && userAccount.pictureUrl}
        name={userAccount && userAccount.name}
        pinToValidate={userAccount && userAccount.pin}
        visible={pinDialogOpened}
        setDialogOpen={setPinDialogOpened}
        onConfirmCallback={() => pinAction(selectedUser)}
      />
    </>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    borderRadius: 0,
    zIndex: 1,
    elevation: 3,
  },
  titleContainer: {
    paddingHorizontal: normalize(16),
    flexDirection: "row",
    alignItems: "center",
  },
  leftArrowIcon: {
    fontSize: 24,
  },
  title: {
    flex: 1,
    paddingHorizontal: 16,
  },
  button: {
    marginVertical: 16,
    marginLeft: "auto",
  },
});

export default BookingTitle;
