import React, {useContext} from "react";
import {Image, Linking, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {Divider, useTheme} from "react-native-paper";

import i18n from "i18n-js";

import normalize from "utils/normalize";
import {version} from "utils/versionUtils";

import {DEVELOPMENT} from "react-native-dotenv";
import {StorageContext} from "contexts/StorageContext";
import CustomStackCard from "components/common/layout/CustomStackCard";

const DrawerAboutModal = ({navigation}) => {
  const {colors} = useTheme();

  const versionText = i18n.t("Version_x", {
    input: version,
  });

  const {appStorage} = useContext(StorageContext);

  const envLabel = appStorage?.env === DEVELOPMENT ? i18n.t("env_development") : i18n.t("env_production");

  const envText = i18n.t("Environment_x", {input: envLabel});

  function Logo() {
    return (
      <View style={{flex: 1, alignItems: "center", marginTop: 40}}>
        <Image
          style={{height: normalize(14), width: normalize(130)}}
          source={require("../../../assets/skedway_logo.png")}
        />
        <Text style={{
          fontFamily: 'LibreBaskerville_400Regular_Italic',
          fontSize: normalize(12),
          marginTop: 30,
          marginBottom: 70
        }}>
          Work beyond place
        </Text>
      </View>
    )
  }

  return (
    <CustomStackCard
      headerLabel={i18n.t("title_about")}
      subHeader={Logo()}
      backAction={() => navigation.goBack()}
    >
      <Text style={styles.env}>
        {envText}
      </Text>
      <Text style={styles.version}>
        {versionText}
      </Text>

      <Divider style={styles.divider}/>

      <Text style={styles.descPlatformFirst}>
        The platform to optimize the
      </Text>
      <View style={styles.descPlatformSecond}>
        <Text style={styles.descPlatformSecondInside}>
          hybrid work
        </Text>
        <Text style={styles.descPlatformLast}>
          {`  experience`}
        </Text>
      </View>

      <Text style={styles.descMain}>
        Gerencie espaços como salas, posições de trabalho,
        estacionamentos é a forma mais simples e organizada para
        poupar recursos e aumentar a produtividade.
      </Text>

      <TouchableOpacity onPress={() => Linking.openURL("https://website.skedway.com/")}>
        <Text style={[styles.website, {color: colors.darkGreen}]}>
          Website ->
        </Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => Linking.openURL("https://website.skedway.com/privacy")}>
        <Text style={[styles.privacyPolicy, {color: colors.darkGreen}]}>
          Política de privacidade ->
        </Text>
      </TouchableOpacity>
    </CustomStackCard>
  );
};

const styles = StyleSheet.create({
  env: {
    fontFamily: 'Manrope_500Medium',
    fontSize: normalize(12)
  },
  version: {
    fontFamily: 'LibreBaskerville_400Regular_Italic',
    fontSize: normalize(11),
    marginTop: 27
  },
  divider: {
    marginTop: 40, height: 3
  },
  descPlatformFirst: {
    fontFamily: 'Manrope_400Regular',
    fontSize: normalize(10),
    marginTop: 40
  },
  descPlatformSecond: {
    flexDirection: "row", alignItems: "center"
  },
  descPlatformSecondInside: {
    fontFamily: 'LibreBaskerville_400Regular_Italic',
    fontSize: normalize(10)
  },
  descPlatformLast: {
    fontFamily: 'Manrope_400Regular',
    fontSize: normalize(10)
  },
  descMain: {
    fontFamily: 'Manrope_400Regular',
    fontSize: normalize(10),
    marginTop: 20
  },
  website: {
    fontFamily: 'Manrope_600SemiBold',
    fontSize: normalize(9),
    marginTop: 80
  },
  privacyPolicy: {
    fontFamily: 'Manrope_600SemiBold',
    fontSize: normalize(9),
    marginTop: 40
  }
});

export default DrawerAboutModal;
