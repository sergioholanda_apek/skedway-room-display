   import React, {useContext, useEffect, useState} from "react";
import {StyleSheet, Text, TouchableHighlight, View} from "react-native";
import {useTheme} from "react-native-paper";
import {format, isToday} from "date-fns";
import QRCode from "react-qr-code";
import i18n from "i18n-js";

import {StorageContext} from "contexts/StorageContext";
import {SchedulingContext} from "contexts/SchedulingContext";
import {SnackbarContext} from "contexts/SnackbarContext";

import Capacity from "components/ScreenSaver/common/Capacity";
import normalize from "utils/normalize";
import {styledText} from "utils/styleUtils";

import ChevronLeft from "../../../assets/icons/ChevronLeft";
import Bin from "../../../assets/icons/Bin";

import useSync from "hooks/useSync";
import {getHourFormat} from "utils/hourUtils";
import PinDialog from "../pinDialog/PinDialog";
import {navigate} from "utils/navigationRef";

import {
  getNextMeeting,
  getNowHappeningMeeting,
} from "utils/schedulerUtils";

const SchedulingInfoCard = (props) => {

  const {colors} = useTheme();
  const today = useSync();

  const {
    hostId,
    schedulingId,
    showGoBack = false,
    showStatusBar = true,
    showDelete = false,
    eventTitle,
    icon,
    status,
    occupancy,
    availability,
    statusColor,
    showCapacity,
    showStatusIcon,
    onDismiss,
    isScreenSaver
  } = props;

  const [pinDialogOpened, setPinDialogOpened] = useState(false);

  const {appStorage} = useContext(StorageContext);

  const {
    fetchUserAccount,
    cancelTimeSchedule,
    currentDay,
    userAccount,
    setUserAccount,
    setSelectedSchedule,
    todayDayScheduling
  } = useContext(SchedulingContext);

  const {setNotification} = useContext(SnackbarContext);

  const {site, company} = appStorage;
  const {email} = appStorage?.space;

  const hourString = format(today, getHourFormat(site && site.hourFormat24));
  const isItToday = isToday(new Date(currentDay));
  const startedSchedule = getNowHappeningMeeting(today, todayDayScheduling);
  const nextSchedule = getNextMeeting(today, todayDayScheduling);

  const FormattedHour = () => {
    const splitHour = hourString.split(" ");
    return (
      <Text style={{fontFamily: "Manrope_500Medium", fontSize: normalize(22)}}>
        {splitHour[0]}
        {splitHour.length > 1 && (
          <Text style={{fontFamily: "Manrope_300Light", marginTop: normalize(8), marginLeft: normalize(4)}}>
            {splitHour[1]}
          </Text>
        )}
      </Text>
    );
  };

  useEffect(() => {
    return () => {
      setUserAccount(null);
    }
  }, [])

  const verify = () => {
    if (company?.checkInRequiresAuthentication) {
      setPinDialogOpened(true);
    } else {
      handleCancelTimeSchedule();
    }
  }

  const confirmCancel = () => {
    fetchUserAccount(hostId, verify);
  };

  function cancelTimeScheduleSuccessfully() {
    setPinDialogOpened(false);
    onDismiss();
    setNotification(i18n.t("success_cancellation"));
  }

  const handleCancelTimeSchedule = () => {
    const callback = cancelTimeScheduleSuccessfully();
    cancelTimeSchedule(hostId, schedulingId, isItToday, callback);
  };

  const openSchedulingDetail = () => {
    const currentSchedule = startedSchedule ?? nextSchedule;
    if (currentSchedule && !isScreenSaver) {
      const {id, userId, title, subject, attendees} = currentSchedule;
      setSelectedSchedule({id, userId, title, subject, attendees});
      navigate("Checkin");
    }
    if (isScreenSaver) {
      navigate("Home");
    }
  }

  return (
    <TouchableHighlight
      underlayColor="none"
      onPress={openSchedulingDetail}
      style={[styles.content, {backgroundColor: colors.background}]}
    >
      <>
        <View style={{alignItems: "center", flexDirection: "row"}}>
          {
            showGoBack &&
            <View style={{flex: 1}}>
              <TouchableHighlight
                style={[styles.backButton, {backgroundColor: colors.iconBackground}]}
                onPress={onDismiss}
                underlayColor={colors.white}
              >
                <ChevronLeft
                  color={colors.primary}
                  height={normalize(10)}
                  width={normalize(10)}
                />
              </TouchableHighlight>
            </View>
          }

          {
            showStatusBar &&
            <View style={[styles.bar, {
              backgroundColor: statusColor,
              marginLeft: showGoBack ? normalize(20) : 0,
              marginRight: showDelete ? normalize(20) : 0,
            }]}/>
          }

          {
            showDelete &&
            <View style={{flex: 1}}>
              <TouchableHighlight
                style={[styles.backButton, {backgroundColor: colors.lightPink}]}
                onPress={confirmCancel}
                underlayColor={colors.white}
              >
                <Bin
                  color={colors.red}
                  height={normalize(10)}
                  width={normalize(10)}
                />
              </TouchableHighlight>
            </View>
          }
        </View>

        <View style={styles.spaceInfo}>
          <View style={{flexDirection: "column"}}>
            <Text style={styledText("Manrope_400Regular", 16)}>
              {eventTitle}
            </Text>

            {
              showCapacity ? (
                <Capacity direction="row" fontSize={normalize(7)} isNextSchedule={false}/>
              ) : (
                <Text style={styledText("Manrope_500Medium", 6)}>
                  {occupancy}
                </Text>
              )
            }
          </View>

          <View style={{flexDirection: "row"}}>
            {FormattedHour()}
          </View>
        </View>

        {
          showStatusIcon &&
          <View>
            {icon}
          </View>
        }

        <View style={[styles.footer, {marginTop: isScreenSaver ? normalize(15) : 0}]}>
          <View style={{flexDirection: "column"}}>
            <Text style={styledText("LibreBaskerville_400Regular_Italic", 26)}>
              {status}
            </Text>
            <Text style={styledText("Manrope_500Medium", 14)}>{occupancy}</Text>
            {availability}
          </View>

          <View style={{flexDirection: "row"}}>
            {
              email &&
              <QRCode value={email} size={normalize(50)}/>
            }
          </View>
        </View>

        <PinDialog
          pictureUrl={userAccount?.pictureUrl}
          name={userAccount?.name}
          pinToValidate={userAccount?.pin}
          onConfirmCallback={handleCancelTimeSchedule}
          visible={pinDialogOpened}
          setDialogOpen={setPinDialogOpened}
        />
      </>
    </TouchableHighlight>
  )
}

const styles = StyleSheet.create({
  content: {
    marginTop: normalize(12),
    borderRadius: normalize(10),
    padding: normalize(10)
  },
  backButton: {
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    borderRadius: 50,
    height: normalize(16),
    width: normalize(16),
    paddingRight: 3
  },
  bar: {
    flex: 10,
    height: normalize(11),
    borderRadius: normalize(10),
  },
  spaceInfo: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: normalize(8),
    marginBottom: normalize(16)
  },
  footer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
  },
});

export default SchedulingInfoCard;
