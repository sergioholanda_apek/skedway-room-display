import React, {useEffect, useState} from 'react';
import {format} from "date-fns";

const TIME_LEFT = 3000; //5 minutes

export default function countdownTimer(deadline) {

  const countdownDate = deadline?.getTime();

  const [countdown, setCountdown] = useState(null);

  useEffect(() => {
    let timer = null;
    if (countdownDate) {
      timer = setInterval(() => {
        const now = new Date().getTime();
        const timeLeft = countdownDate - now;

        if (timeLeft <= TIME_LEFT) {
          const countdown_ = format(new Date(timeLeft), "mm:ss");
          setCountdown(countdown_);
        }

        if (timeLeft <= 0) {
          setCountdown(null);
          clearInterval(timer);
        }
      }, 1000);
    }

    return () => {
      clearInterval(timer);
    }
  }, [])

  return countdown;
};
