export default {
  hello: "Olá, ",
  welcome: "seja bem-vindo ",
  to: "ao",

  instructions: "Por favor, digite seu e-mail para começar!",
  email: "Email",
  invalidEmail: "Email inválido",
  next: "Próximo",
  login: "Entrar",
  password_instructions: "Por favor, digite sua senha senha",
  account_info: "Por favor, selecione a conta que você quer entrar",
  accounts: "Conta",
  forgot_password: "Esqueci minha senha",
  forgot_password_info: "Se você esqueceu sua senha, clique em Esqueci minha senha. %{0} enviará instruções para seu endereço de e-mail.",
  app_description: "A plataforma de gestão corporativa de espaços. Gerencie salas e posições de trabalho de forma simples e organizada, e perceba que administrar espaços é a forma mais inteligente de poupar recursos e aumentar a produtividade.",

  errorDialog: "Ocorreu um erro",
  user_not_administrator: "Este usuário não é administrador",
  company_removed: "Sua empresa foi removida",
  unknown_user_or_removed: "Usuário desconhecido ou removido",
  company_plan_expired: "O plano da sua empresa expirou",
  user_blocked: "Usuário bloqueado após muitas tentativas de acesso",
  error_password_expired: "A sua senha expirou! Cadastre uma nova senha.",
  user_unknown_or_cannot_log_in_to_the_device: "Usuário desconhecido ou não pode fazer login neste dispositivo."
}