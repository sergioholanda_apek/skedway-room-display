import React from "react";
import {View, StyleSheet, Dimensions, Text} from "react-native";
import {ActivityIndicator, useTheme} from "react-native-paper";
import { CalendarList } from "react-native-calendars";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import useOrientation from "hooks/useOrientation";
import normalize from "utils/normalize";

const ONE_YEAR = 12;

const ArrowComponent = (direction) => {
  if (direction === "left") {
    return (
      <MaterialCommunityIcons style={styles.leftArrow} name="arrow-left" />
    );
  }
  if (direction === "right") {
    return (
      <MaterialCommunityIcons style={styles.rightArrow} name="arrow-right" />
    );
  }
};

const CustomCalendarWithRef = React.forwardRef((props, ref) => {
  const { customMarkedDates, onVisibleMonthsChange, isFetching, onDayClicked } =
    props;

  const { colors } = useTheme();

  const orientation = useOrientation();
  const isPortrait = orientation === "PORTRAIT";

  const markedDates = isFetching ? {} : customMarkedDates;

  const calendarWidth = isPortrait ? Dimensions.get("window").width :
    Dimensions.get("window").width / 2;

  const calendarHeight = isPortrait ? Math.max(normalize(200), 360) : Math.max(normalize(220), 360);

  const skedwayTheme = {
    textMonthFontFamily: "LibreBaskerville_400Regular_Italic",
    textMonthFontSize: normalize(12),
    calendarBackground: "transparent",

    "stylesheet.calendar.header": {
      week: {
        marginTop: 30,
        flexDirection: "row",
        marginHorizontal: normalize(10),
        justifyContent: "space-around",
      },
      dayHeader: {
        color: colors.text,
        fontSize: 16,
        marginBottom: normalize(8),
        width: "100%",
        textAlign: "center",
        fontFamily: "Manrope_600SemiBold",
      },
    },

    "stylesheet.day.basic": {
      todayText: {
        fontWeight: "600",
        color: colors.text,
        marginTop: normalize(2),
      },
      text: {
        fontSize: 16,
        marginTop: normalize(2),
        fontFamily: "Manrope_400Regular",
      },
    },

    "stylesheet.calendar.main": {
      week: {
        marginVertical: normalize(4),
        paddingHorizontal: normalize(8),
        flexDirection: "row",
      },
    },

  }

  return (
    <>
      {
        isFetching ? (
          <ActivityIndicator
            size="large"
            animating={true}
            color="green"
          />
        ) : (
          <CalendarList
            ref={ref}
            calendarWidth={calendarWidth}
            calendarHeight={calendarHeight}
            style={{ width: calendarWidth }}
            theme={skedwayTheme}
            horizontal
            pagingEnabled
            scrollEnabled
            hideExtraDays
            hideArrows={false}
            showWeekNumbers={false}
            firstDay={0}
            monthFormat={"MMMM yyyy"}
            pastScrollRange={ONE_YEAR}
            futureScrollRange={ONE_YEAR}
            renderArrow={ArrowComponent}
            markingType={"custom"}
            markedDates={markedDates}
            onDayPress={onDayClicked}
            onVisibleMonthsChange={onVisibleMonthsChange}
          />
        )
      }
    </>

  );
});

const styles = StyleSheet.create({
  leftArrow: {
    position: "absolute",
    top: 0,
    fontSize: normalize(14),
    left: normalize(4),
  },
  rightArrow: {
    position: "absolute",
    top: 0,
    fontSize: normalize(14),
    right: normalize(4),
  },
});

export default CustomCalendarWithRef;
