import React from "react";
import { View, StyleSheet } from "react-native";
import {
  HelperText,
  useTheme,
  IconButton,
} from "react-native-paper";
import {
  TextInput
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

const CustomInput = (props) => {
  const {
    mode = "flat",
    label = "",
    value = "",
    errorMessage = "",
    onChangeText = () => {},
    style,
    secureTextEntry,
    isPassword = false,
    onPress,
    bordered = false,
    ...otherProps
  } = props;

  const { colors } = useTheme();

  return (
    <View style={styles.container}>
      <TextInput
        dense
        mode={mode}
        style={[style, styles.input, { borderColor: bordered ? "#274743" : "#FFF"}]}
        placeholder={label}
        value={value}
        secureTextEntry={secureTextEntry}
        onChangeText={onChangeText}
        error={errorMessage}
        underlineColor={errorMessage ? colors.error : colors.primary}
        activeOutlineColor="blue"
        {...otherProps}
      />
      <HelperText
        style={
          errorMessage ? { fontSize: 13 } : styles.errorMessageHidden
        }
        type="error"
        visible={errorMessage}
      >
        {errorMessage}
      </HelperText>
      {errorMessage ? (
        <MaterialIcons name="error" style={styles.warningIcon} color={"red"} />
      ) : null}
      {isPassword && !errorMessage && (
        <IconButton
          style={styles.passwordIcon}
          icon={secureTextEntry ? "eye-off" : "eye"}
          color={"black"}
          size={20}
          onPress={onPress}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 55,
    backgroundColor: "#fff",
    fontSize: 18,
    fontFamily: "Manrope_400Regular",
    paddingLeft: 20,
    borderWidth: 1,
    borderRadius: 10,

  },
  container: {
    position: "relative",
  },
  warningIcon: {
    position: "absolute",
    right: 10,
    top: 18,
    fontSize: 25,
  },
  passwordIcon: {
    position: "absolute",
    right: 4,
    top: 10,
  },
  errorMessageHidden: {
    display: "none",
  },
});

export default CustomInput;
