import React from "react";
import {LinearGradient} from "expo-linear-gradient";

import useOrientation from "hooks/useOrientation";

import normalize from "utils/normalize";

const MainLayout = ({children}) => {

  const orientation = useOrientation();
  const isPortrait = orientation === "PORTRAIT";

  const mainContainerStyle = isPortrait
    ? {display: "flex", flex: 1, flexDirection: "column"}
    : {display: "flex", flex: 1, flexDirection: "row", justifyContent: "space-between"};

  return (
    <LinearGradient
      colors={["#FFFFFF", "#FFFFFF", "#FFFFFF", "#FFFFFF", "#F7F7F7"]}
      style={[mainContainerStyle, {padding: normalize(12)}]}
    >
      {children}
    </LinearGradient>
  )
}

export default MainLayout;