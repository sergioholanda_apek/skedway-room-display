import * as React from "react"
import Svg, {Path} from "react-native-svg"
import {useTheme} from "react-native-paper";

const GeoAltFill = (props) => {

  const {height, width} = props;
  const {colors} = useTheme();

  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      fill={colors.active}
      viewBox="0 0 15 16"
      width={height}
      height={width}
    >
      <Path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/>
    </Svg>
  )
}

export default GeoAltFill;
