import { useState, useEffect } from 'react';
import { Linking, Platform } from 'react-native';
import { Camera } from 'expo-camera';

const usePermissionCamera = () => {
  const [status, setStatus] = useState('checking');

  useEffect(() => {
    Camera.getCameraPermissionsAsync().then(({ status }) => {
      setStatus(status)
      if (status === 'denied') {
        setStatus('checking')
        ask() // Dirty fix for https://github.com/expo/expo/issues/4618
      }
    })
  }, [])

  const ask = () => Camera.requestCameraPermissionsAsync().then(({ status }) => setStatus(status))
  return {
    ask,
    status,
    isChecking: status === 'checking',
    isUndetermined: status === 'undetermined',
    isGranted: status === 'granted',
    isDenied: status === 'denied',
    goToSettings: () => goToSettings(),
  }
}

export const goToSettings = Platform.select({
  ios: () => {
    return Linking.openURL('app-settings:');
  }
})

export default usePermissionCamera;