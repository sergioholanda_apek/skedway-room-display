import React, {useContext, useEffect, useState} from "react";
import {ScrollView, StyleSheet, Text, View} from "react-native";
import {Badge, Subheading, Title} from "react-native-paper";
import i18n from "i18n-js";
import isEmpty from "lodash/isEmpty";
import uuid from "react-native-uuid";

import {SchedulingContext} from "contexts/SchedulingContext";
import {StorageContext} from "contexts/StorageContext";

import SchedulerHeader from "components/Header/SchedulerHeader";
import SchedulerChunkDetail from "components/Home/BookingDetail/SchedulerChunkDetail";
import FacilityCard from "components/Issues/FacilityCard";
import MainLayout from "components/common/layout/MainLayout";
import CustomButton from "components/common/buttons/CustomButton";

import EmojiSmile from "../../assets/icons/EmojiSmile";
import Cone from "../../assets/icons/Cone";

import useOrientation from "hooks/useOrientation";
import normalize from "utils/normalize";

const MaintenanceScreen = ({navigation}) => {

  const [createdIssues, setCreatedIssues] = useState([]);

  const {selectedSchedule, setSelectedSchedule} = useContext(SchedulingContext);
  const {appStorage} = useContext(StorageContext);

  const {issues} = appStorage;

  const orientation = useOrientation();
  const isPortrait = orientation === "PORTRAIT";

  useEffect(() => {
    if (!isEmpty(issues)) {
      setCreatedIssues(issues);
    }
  }, [issues]);

  useEffect(() => {
    console.log("[Maintenance]")
    const listener = navigation.addListener("didFocus", () => {
      setSelectedSchedule(null);
    });

    return () => {
      listener.remove();
    };
  }, []);

  useEffect(() => {
    navigation.setParams({newIssuesCount: createdIssues.length});
    navigation.setParams({isPortrait});
    navigation.navigate("Home");
  }, [issues, isPortrait]);

  const NoIssuePlaceholder = () => (
    <View style={{alignItems: "center"}}>
      <EmojiSmile height="20%" width="20%"/>
      <Subheading style={{fontFamily: "Manrope_400Regular"}}>
        {i18n.t("No_opened_issue")}
      </Subheading>
    </View>
  );

  const Issues = () => (
    <ScrollView alwaysBounceVertical={false}>
      {issues.map((issue) => (
        <FacilityCard key={uuid.v4()} {...issue} />
      ))}
    </ScrollView>
  );

  const mainContainerStyle = isPortrait
    ? {display: "flex", flex: 1, flexDirection: "column"}
    : {display: "flex", flex: 1, flexDirection: "row"};

  const headerContainerStyle = isPortrait
    ? {}
    : {height: "100%", width: "50%"};

  const contentContainerStyle = {

    width: isPortrait ? "100%" : "50%",
    flex: 1,
    marginBottom: isPortrait ? Math.max(normalize(38), 50) : 0,
  };

  return (
    <MainLayout>
      <SchedulerHeader showStatusIcon={false}/>

      {selectedSchedule ? (
        <SchedulerChunkDetail {...selectedSchedule} />
      ) : (
        <View style={styles.facilitiesContainer}>
          <Title style={styles.title}>
            {i18n.t("Reported_issues")}
          </Title>

          {isEmpty(createdIssues)
            ? NoIssuePlaceholder()
            : Issues()}

          <View style={{alignItems: "flex-end"}}>
            <CustomButton
              type="secondary"
              style={styles.scheduleButton}
              labelStyle={styles.scheduleButtonLabel}
              onPress={() => navigation.navigate("IssueReport")}
              label={i18n.t("action_report")}
            />
          </View>
        </View>
      )}
    </MainLayout>
  );
};

MaintenanceScreen.navigationOptions = ({navigation}) => {
  const newIssuesCount = navigation.getParam("newIssuesCount");
  const isPortrait = navigation.getParam("isPortrait");

  return {
    tabBarLabel: ({focused}) => {
      return (
        <Text
          style={{
            color: focused ? "#717171" : "#BCC6C6",
            fontFamily: "Manrope_400Regular",
            fontSize: Math.max(normalize(7), 11),
            textAlign: "center"
          }}
        >
          {i18n.t("title_issues")}
        </Text>
      );
    },
    tabBarIcon: ({focused}) => {
      return (
        <>
          {
            newIssuesCount ? (
              <View
                style={{
                  position: "absolute",
                  borderRadius: 100,
                  borderWidth: 1.5,
                  marginLeft: 10,
                  top: 0,
                  borderColor: "white",
                  zIndex: 1
                }}
              >
                <Badge
                  style={{fontWeight: "700"}}
                  size={Math.max(normalize(11), 20)}
                >
                  {newIssuesCount}
                </Badge>
              </View>
            ) : <></>
          }
          <Cone height="100%" width="100%" focused={focused}/>
        </>
      );
    },
  };
};

const styles = StyleSheet.create({
  backButton: {
    alignItems: "center",
    justifyContent: 'center',
    backgroundColor: "#E9EDEC",
    borderRadius: 50,
    height: normalize(16),
    width: normalize(16),
    paddingRight: 3
  },
  scheduleButton: {
    paddingTop: normalize(4),
    paddingBottom: normalize(4),
    borderRadius: normalize(18),
    textTransform: "uppercase"
  },
  scheduleButtonLabel: {
    textTransform: "uppercase",
    fontSize: normalize(9)
  },
  facilitiesContainer: {
    flex: 0.85,
    justifyContent: "space-between",
  },
  card: {
    borderRadius: 0,
  },
  noIssueContainer: {
    alignSelf: "center",
  },
  happyIcon: {
    alignSelf: "center",
  },
  title: {
    fontFamily: "Manrope_400Regular",
    fontSize: normalize(10),
    marginTop: normalize(10)
  },
});

export default MaintenanceScreen;
