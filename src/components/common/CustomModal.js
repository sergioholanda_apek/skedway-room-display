import React from "react";
import { Modal, Portal, useTheme } from "react-native-paper";

const CustomModal = ({ visible, onDismiss, dismissable = false, children }) => {
  const { colors } = useTheme();
  return (
    <Portal>
      <Modal
        dismissable={dismissable}
        theme={{
          colors: { backdrop: colors.white },
        }}
        contentContainerStyle={{
          elevation: 0,
        }}
        visible={visible}
        onDismiss={onDismiss}
      >
        {children}
      </Modal>
    </Portal>
  );
};

export default CustomModal;
