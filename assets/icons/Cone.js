import * as React from "react"
import Svg, {Path} from "react-native-svg"
import {useTheme} from "react-native-paper";

const Cone = (props) => {

  const {colors} = useTheme();
  const {focused} = props;

  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      fill={focused ? colors.active : colors.inactive}
      viewBox="0 0 16.5 17"
      {...props}
    >
      <Path d="M7.03 1.88c.252-1.01 1.688-1.01 1.94 0l2.905 11.62H14a.5.5 0 0 1 0 1H2a.5.5 0 0 1 0-1h2.125L7.03 1.88z" />
    </Svg>
  )

}

export default Cone;
