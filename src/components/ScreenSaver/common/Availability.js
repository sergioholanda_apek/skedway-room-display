import React, {useEffect, useState} from 'react';
import {StyleSheet, Text} from "react-native";
import {differenceInHours, differenceInMilliseconds, format, parseISO} from "date-fns";
import i18n from "i18n-js";

import useSync from "hooks/useSync";

const Availability = ({startedSchedule, nextSchedule, color, fontSize}) => {

  const [availability, setAvailability] = useState("");
  
  const today = useSync();

  useEffect(() => {
    if (startedSchedule) {
      setAvailability(startedSchedule.subject)
    } else {
      const availability_ = nextSchedule ? i18n.t("screenSaver.availability_of", [getNextAvailability()]) :
        i18n.t("screenSaver.no_reservation_today");
      setAvailability(availability_)
    }
  }, [startedSchedule, nextSchedule, today]);

  const getNextAvailability = () => {
    const {startDate} = nextSchedule;
    const startDateISO = parseISO(startDate);
    const diff = differenceInMilliseconds(startDateISO, today);
    const hours = differenceInHours(startDateISO, today);
    const minutes = parseInt(format(new Date(diff), "m"));
    const seconds = parseInt(format(new Date(diff), "s"));

    let output = "";
    if (hours > 0) {
      output += `${hours} ${i18n.t("screenSaver.hours", {count: hours})} `;
    } else if (minutes === 59) {
      output += `${1} ${i18n.t("screenSaver.hour", {count: 1})} `;
    }

    if (minutes >= 0 && minutes < 59) {
      output += `${minutes + 1} ${i18n.t("screenSaver.minutes", {count: minutes + 1})}`;
    } else if (minutes === 0 && seconds > 0) {
      output += `${1} ${i18n.t("screenSaver.minutes", {count: 1})}`;
    }
    return output;
  }

  return (
    <Text style={[styles.spaceCapacity, {color, fontSize}]}>
      {availability}
    </Text>
  );
};

const styles = StyleSheet.create({
  spaceCapacity: {
    paddingTop: 10,
    fontFamily: "Manrope_300Light",
  },
});

export default Availability;