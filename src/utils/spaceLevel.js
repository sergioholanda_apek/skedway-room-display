export const LEVEL_USER = 1;
export const LEVEL_MANAGER = 2;
export const LEVEL_DIRECTOR = 3;
export const LEVEL_PRESIDENT = 4;
export const LEVEL_NOT_SCHEDULABLE = 99;