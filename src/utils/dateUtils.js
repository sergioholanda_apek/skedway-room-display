import {differenceInMinutes, format, getDate, getDaysInMonth, getMonth, getYear, parse,} from "date-fns";
import * as React from "react";
import {getHourFormat} from "./hourUtils";

/*
 * This function will return a map with a <key, value> pair like this:
 * { '2020-03-01': 0, '2020-03-02': 0, ..., '2020-03-31': 0 }
 */
export function mapMonth(date) {
  const dateObject = new Date(date);
  const days = getDaysInMonth(dateObject);
  const year = getYear(dateObject);
  const month = getMonth(dateObject) + 1; // The month array begins at 0

  const monthString = month < 10 ? `0${month}` : month;

  const map = [...Array(days).keys()].reduce((acc, curr) => {
    const currentDay = curr + 1;
    const dayString = currentDay < 10 ? `0${currentDay}` : currentDay;

    const key = `${year}-${monthString}-${dayString}`;

    /*
     * This value represents the scheduled minutes for a given day. It starts with zero and
     * then all the other days comming from the api will replace their value
     */
    acc[key] = 0;

    return acc;
  }, {});

  return map;
}

export function mapMonthChart(date, monthOccupancy) {
  if (!date || !monthOccupancy) {
    return;
  }

  const dateObject = new Date(date);
  const days = getDaysInMonth(dateObject);

  const array = Array(days).fill(0);

  Object.values(monthOccupancy).map((day) => {
    array[day.index] = day.percentage;
  });

  return array;
}

export function datesDifference(
  openingTime,
  closingTime,
  funcToDiff = differenceInMinutes
) {
  const openingDate = parse(openingTime, "HH:mm:ss", new Date());
  const closingDate = parse(closingTime, "HH:mm:ss", new Date());
  return funcToDiff(closingDate, openingDate);
}

export function percentageToColor(percentage) {
  if (percentage === 0) {
    return "#87dfb5";
  }
  if (percentage < 20) {
    return "#81c784";
  }
  if (percentage < 40) {
    return "#E6F8EF";
  }
  if (percentage < 60) {
    return "#e9e7f5";
  }
  if (percentage < 80) {
    return "#fdd4ae";
  }
  if (percentage < 90) {
    return "#fcc38e";
  }
  return "#fa983d";
}

const textColor = (percentage) => {
  if (percentage >= 90) {
    return "#FFFFFF";
  } else {
    return "#1D3731"
  }
}

export function getPercentage(value, openingTime, closingTime) {
  const minutesInADay = datesDifference(openingTime, closingTime);
  return Math.min((value / minutesInADay) * 100, 100);
}

export function withTimezone(date) {
  return `${date}T00:00:00-03:00`;
}

export function parseWithNoTZ(date) {
  return parse(`${date} +00`, "yyyy-MM-dd H:mm:ss x", new Date());
}

export function getHour({ date, locale, options }) {
  // Reseting timezone
  const parsedDate = parseWithNoTZ(date);

  // Setting timezone
  const dateWithTZ = new Date(parsedDate).toLocaleString(locale, {
    ...options,
  });

  const [_, ...rest] = dateWithTZ.split(" ");
  return rest.join(" ");
}

export function getHourWithoutParse({ date, locale, options }) {
  // Setting timezone
  const dateWithTZ = new Date(date).toLocaleString(locale, {
    ...options,
  });

  const [_, ...rest] = dateWithTZ.split(" ");
  const hour = rest.join(" ");

  return hour;
}

export function isHour(date) {
  return date.split(" ").length === 1;
}

export function getNextRoundedDate(minutes, date = new Date()) {
  const ms = 1000 * 60 * minutes; // convert minutes to ms
  return new Date(Math.ceil(date.getTime() / ms) * ms);
}

export function mapDayColors(occupancy, openingTime, closingTime) {
  return occupancy &&
    Object.keys(occupancy).reduce((acc, key) => {
      const occupancyValue = occupancy[key];

      const occupancyPercentage = getPercentage(
        occupancyValue,
        openingTime,
        closingTime
      );

      const backgroundColor = percentageToColor(occupancyPercentage);
      const color = textColor(occupancyPercentage)
      const index = getDate(new Date(withTimezone(key))) - 1;

      acc[key] = {
        index,
        percentage: occupancyPercentage,
        customStyles: {
          container: {
            backgroundColor,
            borderRadius: 10,
            height: 45,
            width: 65,
            justifyContent: "center",
            textAlign: "center",
          },
          text: {
            marginTop: 5,
            fontFamily: "Manrope_400Regular",
            fontSize: 16,
            color: "#1D3731",
          },
        },
      };
      return acc;
    }, {});
}

export const getFormattedDate = (date, hourFormat24) => {
  const dateWithNoTZ = parseWithNoTZ(date);
  return format(dateWithNoTZ, getHourFormat(hourFormat24))
}
