export default {
  free_now: "Libre ahora",
  all_day_free: "Todo el día libre",
  awaiting_check_in: "Esperando Check-in",
  happening_now: "Sucediendo ahora",
  next_meeting: "Próxima reunión",
  no_reservation_today: "Ninguna reserva hoy",
  availability_of: "Disponible en %{0}",
  hour: "hora",
  hours: {
    "one": "hora y",
    "other": "horas y"
  },
  minutes: {
    "one": "minuto",
    "other": "minutos"
  },
  time_interval: "%{0} a %{1}",
  free_until: "hasta %{0}",
  countdown_to: "Cuenta regresiva para",
  check_in: "check-in"
}