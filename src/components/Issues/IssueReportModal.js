import React, {useContext, useEffect, useState} from "react";
import {Image, StyleSheet, Text, TextInput, TouchableHighlight, View} from "react-native";
import {useTheme} from "react-native-paper";
import i18n from "i18n-js";
import isEmpty from "lodash/isEmpty";
import merge from "lodash/merge";
import update from 'immutability-helper';

import {IssueContext} from "contexts/IssueContext";
import {SnackbarContext} from "contexts/SnackbarContext";
import {StorageContext} from "contexts/StorageContext";

import Facilities from "./Facilities";
import CustomDialog from "components/common/dialog/CustomDialog";

import Display from "../../../assets/icons/Display";
import CardText from "../../../assets/icons/CardText";
import PersonFill from "../../../assets/icons/PersonFill";

import normalize from "utils/normalize";
import {navigate} from "utils/navigationRef";
import {encodeBitwiseList} from "utils/bitwiseUtils";
import CustomStackCard from "../common/layout/CustomStackCard";

export const ASSET_LIGHTING = 1;
export const ASSET_FURNITURE = 2;
export const ASSET_AIR_CONDITIONING = 4;

const ASSET_LIGHTING_OBJ = {id: ASSET_LIGHTING, title: "Lighting"};
const ASSET_FURNITURE_OBJ = {id: ASSET_FURNITURE, title: "Furniture"};
const ASSET_AIR_CONDITIONING_OBJ = {id: ASSET_AIR_CONDITIONING, title: "Air_conditioning"};

export const ASSETS = [
  ASSET_LIGHTING_OBJ,
  ASSET_FURNITURE_OBJ,
  ASSET_AIR_CONDITIONING_OBJ,
];

const IssueReportModal = ({navigation}) => {

  const {colors} = useTheme();

  const [selectedUser, setSelectedUser] = useState(null);

  const [assets_, setAssets] = useState(ASSETS);
  const [amenities_, setAmenities] = useState([]);

  const [description, setDescription] = useState("");
  const [showErrorMessage, setShowErrorMessage] = useState(false);

  const {appStorage, insertAppStorage} = useContext(StorageContext);
  const {createIssue, issues, hasCreatedIssueSuccessfully} = useContext(IssueContext);
  const {setNotification} = useContext(SnackbarContext);

  const {amenities} = appStorage;

  useEffect(() => {
    if (!isEmpty(amenities)) {
      setAmenities(amenities);
    }
  }, [amenities]);

  useEffect(() => {
    if (hasCreatedIssueSuccessfully) {
      // The modal should be closed when a issue has been reported
      const merged = merge(appStorage, {issues});
      insertAppStorage(merged);
      onDismiss();
      const message = i18n.t("object_created", {0: i18n.t('title_issue')});
      setNotification(message);
    }
  }, [hasCreatedIssueSuccessfully]);

  const onDismiss = () => navigation.goBack();

  const dismissWithCleanUp = () => {
    setDescription("");
    setAssets([]);
    setAmenities([]);
    onDismiss();
  };

  const changeAssetsStatus = (asset) => {
    const index = assets_.indexOf(asset);

    setAssets(
      updatedAssets => update(updatedAssets, {
        [index]: {
          status: {
            $set: !asset?.status
          }
        }
      })
    );
  }

  const changeAmenityStatus = (amenity) => {
    const index = amenities_.indexOf(amenity);

    setAmenities(
      updatedAmenities => update(updatedAmenities, {
        [index]: {
          status: {
            $set: !amenity?.status
          }
        }
      })
    );
  }

  const handleCreateIssue = () => {
    const selectedAssets = assets_.filter(a => a.status);
    const selectedAmenities = amenities_.filter(a => a.status);
    const assetsIds = selectedAssets.map(sa => sa.id);
    const amenitiesIds = selectedAmenities.map(sa => sa.id);

    if (isEmpty(selectedAssets) && isEmpty(selectedAmenities)) {
      setShowErrorMessage(true);
      return;
    }

    createIssue(selectedUser.id, encodeBitwiseList(assetsIds), amenitiesIds, description);
  }

  return (
    <CustomStackCard
      headerLabel={i18n.t("title_activity_issue_report_new")}
      backAction={dismissWithCleanUp}
      action={handleCreateIssue}
      actionLabel={i18n.t("action_report")}
    >
      <View style={{flexDirection: "row", alignItems: "center", marginBottom: normalize(10)}}>
        <TouchableHighlight
          style={styles.icon}
          onPress={() => {
            navigate("UserSuggestion", {
              searchUserPlaceholder: i18n.t("prompt_search_for_user"),
              showLocalStorageList: true,
              shouldDismissWhenClicked: true,
              selectUser: setSelectedUser
            });
          }}
          underlayColor={colors.white}
        >
          {
            selectedUser ? (
              <Image
                style={{
                  height: "100%",
                  width: "100%",
                  borderRadius: normalize(8),
                  padding: 0,
                  resizeMode: "cover",
                }}
                size={50}
                source={{uri: selectedUser.pictureUrl}}
              />
            ) : (
              <PersonFill height={normalize(14)} width={normalize(14)} color={colors.primary}/>
            )
          }
        </TouchableHighlight>
        <Text style={{marginLeft: normalize(8), fontFamily: "Manrope_400Regular", fontSize: normalize(9)}}>
          {selectedUser ? selectedUser.name : i18n.t("prompt_user")}
        </Text>
      </View>

      <View style={{flexDirection: "row", marginBottom: normalize(10)}}>
        <View style={styles.icon}>
          <CardText height={normalize(14)} width={normalize(14)} color={colors.primary}/>
        </View>
        <View
          style={{
            backgroundColor: "#FFF",
            borderRadius: normalize(8),
            width: "90%",
            height: "100%",
            marginLeft: normalize(8),
            padding: normalize(10),
          }}>
          <TextInput
            style={{fontSize: normalize(9), fontFamily: "Manrope_400Regular"}}
            multiline
            numberOfLines={4}
            editable
            maxLength={40}
            placeholder={i18n.t("prompt_issue_description")}
            onChangeText={setDescription}
          />
        </View>
      </View>

      <View style={{flexDirection: "row", alignItems: "center"}}>
        <View style={styles.icon}>
          <Display height={normalize(14)} width={normalize(14)} color={colors.primary}/>
        </View>
        <Text style={{marginLeft: normalize(8), fontFamily: "Manrope_400Regular", fontSize: normalize(9)}}>
          {i18n.t("prompt_amenities")}
        </Text>
      </View>

      <Facilities
        assets={assets_}
        amenities={amenities_}
        changeAssetsStatus={changeAssetsStatus}
        changeAmenityStatus={changeAmenityStatus}
      />

      <CustomDialog
        visible={showErrorMessage}
        onDismiss={() => setShowErrorMessage(false)}
        confirmAction={() => setShowErrorMessage(false)}
        title="Error"
        confirmLabel="Ok"
        content={i18n.t("error_user_amenity_required")}
      />
    </CustomStackCard>
  );
};

const styles = StyleSheet.create({
  icon: {
    alignItems: "center",
    justifyContent: 'center',
    backgroundColor: "#E9EDEC",
    borderRadius: normalize(8),
    height: normalize(28),
    width: normalize(28)
  },
  searchBar: {
    zIndex: -1,
    paddingLeft: normalize(8),
    paddingVertical: 8,
    borderBottomWidth: 1,
    elevation: 0,
    borderBottomColor: "#D5D5D5",
    textAlign: "center",
    alignContent: "center",
    alignSelf: "center",
  },
  searchBarText: {
    alignSelf: "center",
    marginBottom: 5,
  },
});
export default IssueReportModal;
