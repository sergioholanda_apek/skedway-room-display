import React, {useEffect, useState} from "react";
import i18n from "i18n-js";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {Avatar, Caption, List, Subheading, useTheme} from "react-native-paper";

import {navigate} from "utils/navigationRef";
import {normalize} from "react-native-elements";

const BASED_HEIGHT = 100;

const EventSlot = (props) => {

  const {colors} = useTheme();

  const {
    todayTextTitle,
    currentDay,
    setSelectedSchedule,
    attendees,
    fulfill,
    schedulingId,
    userId,
    subject,
    openingTime,
    closingTime,
    heightProportion = 1,
    title
  } = props;

  const [host, setHost] = useState(null);

  useEffect(() => {
    if (attendees) {
      const host_ = attendees?.find(attendee => attendee.host);
      setHost(host_)
    }
  }, [attendees])

  const schedulerBooking = () => {
    if (fulfill) {
      setSelectedSchedule(
        {
          schedulingId,
          userId,
          title,
          subject,
          attendees,
        }
      );
      navigate("Checkin");
    } else {
      navigate("SchedulerBooking", {
        currentHour: title,
        title: todayTextTitle,
        currentDay,
        openingTime,
        closingTime,
      });
    }
  };

  return (
    <TouchableOpacity onPress={schedulerBooking}>
      <List.Item
        style={[
          styles.listItem,
          fulfill ? {backgroundColor: colors.warning} : {backgroundColor: colors.darkGreen},
          {height: BASED_HEIGHT * heightProportion},
          {marginTop: normalize(10), borderRadius: 10}
        ]}
        description={() =>
          <Text style={{fontFamily: "Manrope_600SemiBold", fontSize: normalize(20), color: colors.white}}>
            {title}
          </Text>
        }
        right={() =>
          <View style={{flexDirection: "row", marginTop: normalize(5)}}>
            <View style={{flexDirection: "column", alignItems: "flex-end",  marginRight: 20}}>
              <Text style={{fontFamily: "Manrope_600SemiBold", fontSize: normalize(16), color: colors.white}}>
                {subject?.length > 20 ? subject?.substring(0, 20) + "..." : subject}
              </Text>

              <Text style={{fontFamily: "Manrope_400Regular", fontSize: normalize(16), color: colors.white}}>
                {host?.name || host?.email}
              </Text>
            </View>

            {
              host?.userPictureUrl &&
              <Avatar.Image
                size={60}
                style={styles.avatar}
                source={{uri: host?.userPictureUrl}}
              />
            }
          </View>
        }
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: "#EAF6EC",
    position: "relative",
  },
  text: {
    fontFamily: "Manrope_400Regular",
    fontSize: normalize(18),
  },
  subject: {
    marginTop: 20,
    fontFamily: "Manrope_400Regular",
    fontSize: normalize(16),
  },
  userName: {
    fontFamily: "Manrope_400Regular",
    fontSize: normalize(12),
  },
  avatar: {
    borderRadius: normalize(10)
  },
});

export default EventSlot;
