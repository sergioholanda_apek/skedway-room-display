import React, { useContext } from "react";
import { StyleSheet } from "react-native";
import { Portal, Snackbar } from "react-native-paper";

import {SnackbarContext } from "contexts/SnackbarContext";

const DURATION = 1500;

const SnackbarNotifier = () => {
  const {setNotification, notification } = useContext(SnackbarContext);

  return notification ? (
    <Portal>
      <Snackbar
        style={styles.snackbar}
        visible={notification}
        duration={DURATION}
        onDismiss={() => setNotification(null)}
      >
        {notification}
      </Snackbar>
    </Portal>
  ) : null;
};

const styles = StyleSheet.create({
  snackbar: {
    bottom: 60,
    flexDirection: "column",
    marginLeft: "auto",
    marginRight: "auto",
  },
});
export default SnackbarNotifier;
