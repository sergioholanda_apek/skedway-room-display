export default {
  hello: "Hola, ",
  welcome: "Te damos la bienvenida ",
  to: "a",

  instructions: "Por favor, introduce tu e-mail para empezar!",
  email: "Email",
  invalidEmail: "Email inválido",
  next: "Siguiente",
  login: "Entrar",
  password_instructions: "Por favor, introduce tu contraseña",
  account_info: "Por favor, selecciona la cuenta a la que quieres acceder",
  accounts: "Cuentas",
  forgot_password: "Olvidé mi contraseña",
  forgot_password_info: "Si no la recuerdas, haz clic en Olvidé mi contraseña. %{0} enviará instrucciones a tu dirección de e-mail.",
  app_description: "La plataforma de gestión del espacio corporativo. Gestionar salas y puestos de trabajo de forma sencilla y organizada, y darse cuenta de que gestionar espacios es la forma más inteligente de ahorrar recursos y aumentar la productividad.",

  errorDialog: "Ocurrio un error",
  user_not_administrator: "Este usuario no es administrador",
  company_removed: "Su empresa ha sido eliminada",
  unknown_user_or_removed: "Usuario desconocido o eliminado",
  company_plan_expired: "El plan de su empresa expiró",
  user_blocked: "Usuario bloqueado después de muchos intentos de acceso",
  error_password_expired: "¡Tu contraseña ha expirado! Registre una nueva contraseña.",
  user_unknown_or_cannot_log_in_to_the_device: "Usuario desconocido o no puede iniciar sesión en este dispositivo."
}