import React, {useContext, useEffect, useState} from "react";
import {Image, StyleSheet, Text, TouchableHighlight, View} from "react-native";
import {SafeAreaView} from "react-navigation";
import i18n from "i18n-js";

import {ActivityIndicator, Caption, Paragraph, Portal, Subheading, Surface, useTheme,} from "react-native-paper";

import {SyncContext} from "contexts/SyncContext";
import {SnackbarContext} from "contexts/SnackbarContext";
import {StorageContext} from "contexts/StorageContext";

import AuthDialog from "components/Drawer/AuthDialog";
import CustomDialog from "components/common/CustomDialog";

import {navigate} from "utils/navigationRef";
import normalize from "utils/normalize";
import {version} from "utils/versionUtils";

import ExclamationCircle from "../../assets/icons/ExclamationCircle";
import Power from "../../assets/icons/Power";
import ClockHistory from "../../assets/icons/ClockHistory";
import ArrowRepeat from "../../assets/icons/ArrowRepeat";

import {DEVELOPMENT} from "react-native-dotenv";

const Drawer = () => {
  const {colors} = useTheme();

  const [isLoading, setLoading] = useState(false);

  const {userActionSync, userActionSynced, isSynced} = useContext(SyncContext);

  const {setNotification} = useContext(SnackbarContext);

  const {appStorage} = useContext(StorageContext);

  const [authDialogVisible, setAuthDialogVisible] = useState(false);
  const [confirmDialogVisible, setConfirmDialogVisible] = useState(false);

  const {env} = appStorage;

  const envLabel =
    env === DEVELOPMENT
      ? i18n.t("env_development")
      : i18n.t("env_production");

  const envText = i18n.t("Environment_x", {input: envLabel});

  const versionText = i18n.t("Version_x", {
    input: version,
  });

  //TODO Transferir este código para dentro do context
  useEffect(() => {
    setLoading(false);
    if (userActionSynced === "SYNCED") {
      setNotification(i18n.t("success_app_synchronized"));
    } else if (userActionSynced === "ERROR") {
      setNotification(i18n.t("fail_app_synchronized"));
    }
  }, [userActionSynced, isSynced]);

  const RenderSyncingLoading = () => {
    return (
      <Portal>
        {isLoading ? (
          <View style={styles.backgroundContainer}>
            <Surface style={styles.surfaceCircle}>
              <ActivityIndicator
                size="small"
                animating={true}
                color={colors.primary}
              />
            </Surface>
          </View>
        ) : null}
      </Portal>
    );
  };

  const synchronizeAppData = () => {
    setLoading(true);
    userActionSync();
  };

  function ItemMenu(action, icon, label) {
    return (
      <TouchableHighlight
        onPress={action}
        style={styles.itemMenu}
        underlayColor={colors.white}
      >
        <>
          <View style={styles.icon}>
            {icon}
          </View>
          <Text style={styles.iconLabel}>
            {i18n.t(label)}
          </Text>
        </>
      </TouchableHighlight>
    )
  }

  return (
    <SafeAreaView>
      {RenderSyncingLoading()}

      <View style={styles.screenContainer}>
        <Image
          style={{height: normalize(9), width: normalize(80)}}
          source={require("../../assets/skedway_logo.png")}
        />
        <Subheading style={styles.environment}>{envText}</Subheading>
        <Paragraph style={{fontFamily: 'LibreBaskerville_400Regular_Italic'}}>{versionText}</Paragraph>
      </View>

      <View style={styles.sectionContainer}>
        {
          ItemMenu(
            () => synchronizeAppData(),
            <ArrowRepeat height={normalize(11)} width={normalize(11)} color={colors.primary}/>,
            "action_synchronize"
          )
        }
        {
          ItemMenu(
            () => setAuthDialogVisible(true),
            <Power height={normalize(11)} width={normalize(11)} color={colors.primary}/>,
            "action_close"
          )
        }
        {
          ItemMenu(
            () => setConfirmDialogVisible(true),
            <ClockHistory height={normalize(10)} width={normalize(10)} color={colors.primary}/>,
            "action_factory_reset"
          )
        }
        {
          ItemMenu(
            () => navigate("DrawerAbout"),
            <ExclamationCircle height={normalize(10)} width={normalize(10)} color={colors.primary}/>,
            "title_about"
          )
        }
      </View>
      <AuthDialog
        visible={authDialogVisible}
        onDismiss={() => setAuthDialogVisible(false)}
      />
      <CustomDialog
        style={styles.confirmDialogStyles}
        visible={confirmDialogVisible}
        onDismiss={() => setConfirmDialogVisible(null)}
        cancelAction={() => setConfirmDialogVisible(null)}
        confirmAction={() => {
          setConfirmDialogVisible(false);
          setAuthDialogVisible(true);
        }}
        confirmLabel={i18n.t("action_reset")}
      >
        {confirmDialogVisible ? (
          <>
            <Subheading>{i18n.t("action_factory_reset")}</Subheading>
            <Caption>{i18n.t("confirm_factory_reset")}</Caption>
          </>
        ) : null}
      </CustomDialog>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    padding: normalize(16),
  },
  sectionContainer: {
    paddingVertical: normalize(6),
    paddingHorizontal: normalize(16)
  },
  appLogo: {
    width: "70%",
    height: 60,
    alignSelf: "flex-start",
    resizeMode: "contain",
  },
  backgroundContainer: {
    height: 245,
    justifyContent: "flex-end",
  },
  surfaceCircle: {
    borderRadius: 50,
    width: 50,
    height: 50,
    alignSelf: "center",
    justifyContent: "center",
  },
  confirmDialogStyles: {
    marginHorizontal: "15%",
  },
  environment: {
    fontFamily: 'Manrope_400Regular',
    fontSize: normalize(7),
    marginTop: normalize(30)
  },
  itemMenu: {
    flexDirection: "row",
    alignItems: 'center',
    marginBottom: normalize(10)
  },
  icon: {
    padding: 10,
    alignItems: "center",
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: "#E9EDEC",
    borderRadius: normalize(8),
    height: normalize(22),
    width: normalize(22)
  },
  iconLabel: {
    fontFamily: 'Manrope_400Regular',
    fontSize: normalize(8),
    marginLeft: normalize(4)
  }
});
export default Drawer;
