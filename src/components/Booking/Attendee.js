import React, {useContext, useState} from "react";
import {StyleSheet, Text, View} from "react-native";
import {ActivityIndicator, Button, useTheme,} from "react-native-paper";
import uuid from "react-native-uuid";
import {isToday} from "date-fns";
import Icon from 'react-native-vector-icons/AntDesign';

import {styledText} from "utils/styleUtils";
import {SnackbarContext} from "contexts/SnackbarContext";
import {SchedulingContext} from "contexts/SchedulingContext";
import {SyncCheckInsContext} from "contexts/SyncCheckInsContext";
import {StorageContext} from "contexts/StorageContext";

import UserImage from "../User/UserImage";
import PinDialog from "../pinDialog/PinDialog";

import normalize from "utils/normalize";
import i18n from "i18n-js";
import update from "immutability-helper";

import {StatusScheduling} from "utils/schedulerUtils";

const Attendee = ({statusScheduling, isThereAtLeastOneCheckIn}) => {

  const {colors} = useTheme();

  const {setNotification} = useContext(SnackbarContext);

  const {
    fetchUserAccount,
    cancelTimeSchedule,
    setUserActionId,
    isFetchingUserAccount,
    isCancelingTimeSchedule,
    userActionId,
    currentDay,
    selectedSchedule,
    setSelectedSchedule,
    userAccount
  } = useContext(SchedulingContext);

  const attendees = selectedSchedule?.attendees;
  // const checkedIn = selectedSchedule?.checkedIn;

  const {mergeIntoCheckInsList, setCheckIn, syncCheckIns, isSyncingCheckIns, checkIn} = useContext(SyncCheckInsContext);

  const {appStorage} = useContext(StorageContext);

  const [pinDialogOpened, setPinDialogOpened] = useState(null);
  const [selectedAttendee, setSelectedAttendee] = useState(null);

  const onClose = () => setSelectedSchedule(null);

  // const userHasCheckedIn = !!(
  //   checkedIn || checkIn?.find((checkIn) => checkIn.schedulingAttendeeId === id)
  // );

  const isItToday = isToday(new Date(currentDay));

  const handleOnPressHost = (attendee, isCheckIn_) => {
    setSelectedAttendee(attendee);
    if (isCheckIn_) {
      confirmCheckIn(attendee);
    } else {
      confirmCancel(attendee);
    }
  };

  const confirmCancel = (attendee) => {
    const {id} = attendee;
    fetchUserAccount(id, doCancel);
  };

  const doCancel = () => {
    setPinDialogOpened({
      ...userAccount,
      onConfirmCallback: handleCancelTimeSchedule,
    });
  };

  const handleCancelTimeSchedule = () => {
    const callback = () => {
      onClose();
      setNotification(i18n.t("success_cancellation"));
    }
    const {userId, schedulingId} = selectedAttendee;

    cancelTimeSchedule(
      userId,
      schedulingId,
      isItToday,
      callback
    );
  };

  const handleOnPressCheckIn = (attendee) => {
    confirmCheckIn(attendee);
  };

  const confirmCheckIn = (attendee) => {
    const {id, name, userPictureUrl, userPin} = attendee;

    if (appStorage?.company?.checkInRequiresAuthentication) {
      setPinDialogOpened({
        name,
        pictureUrl: userPictureUrl,
        pin: userPin,
        onConfirmCallback: () => doCheckIn(id)
      });
    } else {
      doCheckIn(id);
    }
  };

  const doCheckIn = (schedulingAttendeeId) => {
    const checkInDate = new Date();
    const checkInObj = {
      id: schedulingAttendeeId,
      schedulingAttendeeId,
      checkInDate,
    };
    const updatedCheckIns = [...checkIn, checkInObj];
    const onErrorCallback = () => {
      mergeIntoCheckInsList(checkInObj);
    };
    setCheckIn(updatedCheckIns);
    syncCheckIns(updatedCheckIns, null, onErrorCallback);

    const attendee_ = attendees.find(a => a.id === schedulingAttendeeId);
    const index = attendees.indexOf(attendee_);
    attendee_.checkedIn = true;

    const updatedAttendees = update(selectedSchedule, {
      attendees: {
        [index]: {
          $set: attendee_
        }
      }
    })

    setSelectedSchedule(updatedAttendees);
  };

  function disabledHostButton() {
    return !(statusScheduling === StatusScheduling.OCCUPIED && !isThereAtLeastOneCheckIn);
  }

  function showCheckinButtons() {
    return statusScheduling !== StatusScheduling.FREE;
  }

  return (
    <View style={{backgroundColor: colors.background, borderRadius: normalize(10), padding: normalize(10)}}>
      <ActivityIndicator animating={isSyncingCheckIns}/>
      {
        attendees?.map(attendee =>
          <View key={uuid.v4()}>
            {
              !attendee?.isResource &&
              <View style={styles.attendeeItem}>
                <View style={{flexDirection: "row", alignItems: "center"}}>
                  <View>
                    {
                      attendee?.userPictureUrl &&
                      <UserImage pictureUrl={attendee?.userPictureUrl}/>
                    }
                  </View>
                  <View style={{marginLeft: 10}}>
                    <View style={{flexDirection: "row", alignItems: "center"}}>
                      <Text style={styledText("Manrope_400Regular", normalize(4))}>{attendee?.name}</Text>
                      {
                        attendee?.host &&
                        <Text style={[styledText("LibreBaskerville_400Regular_Italic", normalize(3)), {marginLeft: 5}]}>
                          (Host)
                        </Text>
                      }
                    </View>
                    <Text style={[styledText("Manrope_400Regular", normalize(3)), {color: colors.disabled}]}>
                      {attendee?.email}
                    </Text>
                  </View>
                </View>

                {
                  showCheckinButtons() &&
                  <View>
                    {
                      attendee?.host ? (
                        attendee?.checkedIn ? (
                          <Button
                            mode="contained"
                            elevation={0}
                            uppercase={false}
                            disabled={disabledHostButton()}
                            style={{
                              backgroundColor: disabledHostButton() ? colors.intermediateDisabled : colors.intermediate,
                              elevation: 0
                            }}
                            onPress={() => handleOnPressHost(attendee, false)}
                          >
                            <Text style={[styledText("Manrope_500Medium", normalize(3.5)), {
                              marginRight: 10,
                              color: colors.white
                            }]}>
                              {`Check-out   `}
                            </Text>
                            <Icon name="arrowleft" size={18} color="#fff" style={{padding: 20}}/>
                          </Button>
                        ) : (
                          <Button
                            mode="contained"
                            elevation={0}
                            uppercase={false}
                            style={{backgroundColor: colors.intermediate, elevation: 0}}
                            onPress={() => handleOnPressHost(attendee, true)}
                          >
                            <Text style={[styledText("Manrope_500Medium", normalize(3.5)), {
                              marginRight: 10,
                              color: colors.white
                            }]}>
                              {`Check-in   `}
                            </Text>
                            <Icon name="arrowright" size={18} color="#fff" style={{padding: 20}}/>
                          </Button>
                        )
                      ) : attendee?.checkedIn ? (
                        <Icon name="check" size={24} color={colors.darkGreen} style={{marginRight: normalize(26)}}/>
                      ) : (
                        <Button
                          mode="contained"
                          elevation={0}
                          uppercase={false}
                          style={{backgroundColor: colors.intermediate, elevation: 0}}
                          onPress={() => handleOnPressCheckIn(attendee)}
                        >
                          <Text style={[styledText("Manrope_500Medium", normalize(3.5)), {marginRight: 10}]}>
                            {`Check-in   `}
                          </Text>
                          <Icon name="arrowright" size={18} color="#fff" style={{padding: 20}}/>
                        </Button>
                      )
                    }
                  </View>
                }
              </View>
            }
          </View>
        )
      }

      <PinDialog
        pictureUrl={pinDialogOpened && pinDialogOpened.pictureUrl}
        name={pinDialogOpened && pinDialogOpened.name}
        pinToValidate={pinDialogOpened && pinDialogOpened.pin}
        onConfirmCallback={
          pinDialogOpened && pinDialogOpened.onConfirmCallback
        }
        visible={pinDialogOpened}
        setDialogOpen={setPinDialogOpened}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  attendeeItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: normalize(8)
  },
  avatar: {
    alignSelf: "center",
  },
  row: {
    flexDirection: "row",
  },
  name: {
    fontSize: 18,
    marginRight: 8,
  },
  button: {
    width: 150,
    marginVertical: 8,
  },
  responseString: {
    alignSelf: "flex-end",
    marginRight: "auto",
    marginBottom: 4,
    fontSize: 16,
    flex: 1,
  },
});

export default Attendee;
