import React, {createContext, useContext, useState} from "react";

import {axios_, getAuthToken} from "api";
import {formatISO, isBefore, parse} from "date-fns";

import {SchedulingContext} from "contexts/SchedulingContext";
import {StorageContext} from "contexts/StorageContext";

import {SYNC} from "api/endpoints";
import merge from "lodash/merge";

export const SyncContext = createContext({});

let authToken = undefined

const SyncProvider = ({children}) => {

  const {fetchDayScheduling, currentDay} = useContext(SchedulingContext);
  const {appStorage, insertAppStorage} = useContext(StorageContext);

  const [lastSync, setLastSync] = useState("2017-01-01 00:00:00");
  const [isSyncing, setSyncing] = useState(null);

  const [amenities, setAmenities] = useState([]);
  const [issues, setIssues] = useState([]);

  const [synced, setSynced] = useState(undefined);
  const [userActionSynced, setUserActionSSynced] = useState(undefined);

  async function userActionSync() {
    setUserActionSSynced(undefined);
    try {
      await sync();
    } catch (error) {
      setUserActionSSynced("ERROR");
    }
    setUserActionSSynced("SYNCED");
  }

  async function sync() {
    setSynced(undefined);
    if (currentDay) {
      setSyncing(true);
      authToken = await getAuthToken();

      try {
        const response = await axios_.get(SYNC, {
          params: {
            authToken,
            lastSync,
          },
        });

        const {planProducts, ...restData} = response?.data;

        await filterAmenities(restData)
        await filterIssues(restData);

        if (restData?.lastSync) {
          setLastSync(restData.lastSync);
        }

        if (restData?.company) {
          const {company} = restData;
          const merged = merge(appStorage, {company});
          await insertAppStorage(merged);
        }

        if (restData?.site) {
          const {site} = restData;
          const merged = merge(appStorage, {site});
          await insertAppStorage(merged);
        }

        await fetchDayScheduling();
        //await fetchDayScheduling(formatISO(new Date()), true); TODO verificar isto
      } catch (error) {
        setSynced("ERROR");
        throw new Error("Home Sync error: " + error?.response?.data);
      } finally {
        setSyncing(false);
        setSynced("SYNCED");
      }
    }
  }

  async function filterAmenities(data) {
    if (data?.amenities) {
      const amenitiesToSync = data.amenities;

      const updatedAmenities = amenitiesToSync.reduce((acc, curr) => {
        if (curr.removed) {
          return acc.filter((am) => am.amenityId !== curr.amenityId);
        }
        if (!curr.removed) {
          return [...acc, curr];
        }
        return acc;
      }, amenities);

      const updatedAppStorage = merge(appStorage, {amenities: updatedAmenities});
      await insertAppStorage(updatedAppStorage);
    }
  }

  async function filterIssues(data) {
    if (data?.issues) {
      const issuesToSync = data.issues;

      const updatedIssues = issuesToSync.reduce((acc, curr) => {
        // If it is a fixed issue, remove it from the array
        if (curr.status === 3) {
          return acc.filter((is) => is.id !== curr.id);
        }

        // Otherwise, first remove the old one from the array
        const issuesArrayWithoutCurr = acc.filter((is) => is.id !== curr.id);

        // And then include the updated one
        return [...issuesArrayWithoutCurr, curr];
      }, issues);

      // Now sort them by date
      const sortedIssues = updatedIssues.sort((issue1, issue2) => {
        const format = "yyyy-MM-dd HH:mm:ss";
        const date1 = parse(issue1.createdDate, format, new Date());
        const date2 = parse(issue2.createdDate, format, new Date());

        if (isBefore(date1, date2)) {
          return 1;
        }

        if (isBefore(date2, date1)) {
          return -1;
        }

        return 0;
      });

      const updatedAppStorage = merge(appStorage, {issues: sortedIssues});
      await insertAppStorage(updatedAppStorage);
    }
  }

  return (
    <SyncContext.Provider value={{
      lastSync,
      setLastSync,
      isSyncing,
      setSyncing,
      amenities,
      setAmenities,
      issues,
      setIssues,
      sync,
      filterAmenities,
      filterIssues,
      synced,
      userActionSync,
      userActionSynced,
    }}>
      {children}
    </SyncContext.Provider>
  );
};

export default SyncProvider
