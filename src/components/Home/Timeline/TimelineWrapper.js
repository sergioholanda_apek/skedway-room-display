import React, {useContext, useEffect, useState} from "react";
import {RefreshControl, StyleSheet, View} from "react-native";
import {Card, List, Subheading, useTheme} from "react-native-paper";
import {format, isToday} from "date-fns";
import i18n from "i18n-js";
import isEmpty from "lodash/isEmpty";

import useSync from "hooks/useSync";
import uuid from "react-native-uuid";

import normalize from "utils/normalize";
import {getHourFormat} from "utils/hourUtils";
import {getCurrentLocale} from "utils/localizationUtils";
import buildTimeline from "utils/timelineBuilder";

import EventSlot from "./EventSlot";
import EmptySlot from "./EmptySlot";
import ScrollViewWithElevation from "../../ScrollViewWithElevation";

import {StorageContext} from "contexts/StorageContext";
import {SchedulingContext} from "contexts/SchedulingContext";

const TimelineWrapper = () => {

  const today = useSync();
  const {colors} = useTheme();

  const {appStorage} = useContext(StorageContext);

  const {
    currentDay,
    dayScheduling,
    todayDayScheduling,
    isFetchingDayScheduling,
    setSelectedSchedule,
    fetchDayScheduling,
  } = useContext(SchedulingContext);

  const [isItToday, setIsItToday] = useState(null);
  const [todayTextTitle, setTodayTextTitle] = useState(null);
  const [cardText, setCardText] = useState(null);

  useEffect(() => {
    if (currentDay) {
      const isItToday = isToday(currentDay);
      setIsItToday(isItToday);

      const todayTextTitle = i18n.t("Schedule_for", {
        input: isItToday
          ? i18n.t("today")
          : format(currentDay, i18n.t("format_date")),
      });

      const cardText = isItToday
        ? i18n.t("today")
        : format(currentDay, i18n.t("format_date_weekday"), {
          locale: getCurrentLocale(),
        });

      setTodayTextTitle(todayTextTitle);
      setCardText(cardText);
    }
  }, [currentDay])

  const {site} = appStorage;

  const {
    closingTime,
    timezone,
    openingTime: openingTimeSite
  } = site;

  const nowString = format(today, "HH:mm:ss", {
    locale: getCurrentLocale(),
  });

  const dayScheduling_ = isItToday ? todayDayScheduling : dayScheduling;
  const openingTime = isItToday ? nowString : openingTimeSite;
  const locale = site && site.lang.split("_").join("-");
  const hourFormat = getHourFormat(site && site.hourFormat24);

  const {updatedSchedules, newOpeningTime} = buildTimeline({
    today: isItToday && today,
    hourFormat,
    hourFormat24: site?.hourFormat24,
    openingTime,
    closingTime,
    dayScheduling: dayScheduling_,
    timeZone: timezone,
    locale
  });

  const getEmptyTimelineProps = () => ({
    openingTime: newOpeningTime,
    closingTime,
    todayTextTitle,
    currentDay,
    hourFormat,
    timezone,
    locale
  });

  const getEventTimelineProps = (updatedSchedules_) => ({
    todayTextTitle,
    currentDay,
    setSelectedSchedule,
    ...updatedSchedules_,
  });

  return (
    <>
      {
        !!(openingTime && closingTime) &&
        <>
          <Card style={[styles.titleContainer, {backgroundColor: colors.white}]}>
            <Subheading style={styles.header}>
              {cardText}
            </Subheading>
          </Card>

          <View style={[styles.container, {backgroundColor: colors.background}]}>
            <ScrollViewWithElevation
              refreshControl={
                <RefreshControl
                  refreshing={isFetchingDayScheduling}
                  onRefresh={() => {
                    fetchDayScheduling();
                  }}
                />
              }
            >
              <List.Section style={styles.listContainer}>
                {isEmpty(updatedSchedules) ? (
                  <EmptySlot {...getEmptyTimelineProps()} />
                ) : (
                  updatedSchedules.map(updatedSchedule_ => (
                    <EventSlot
                      key={uuid.v4()}
                      {...getEventTimelineProps(updatedSchedule_)}
                    />
                  ))
                )}
              </List.Section>
            </ScrollViewWithElevation>
          </View>
        </>
      }
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: normalize(2),
    flex: 0.82,
    borderRadius: normalize(10),
    padding: normalize(10),
  },
  listContainer: {
    marginTop: 0,
  },
  titleContainer: {
    elevation: 0,
    zIndex: 1,
    borderRadius: 0,
    marginTop: normalize(10),
  },
  header: {
    fontFamily: "Manrope_400Regular",
    fontSize: normalize(8),
  }
});

export default TimelineWrapper;
