import React from "react";
import {Image, StyleSheet, View} from "react-native";
import {useTheme} from "react-native-paper";

import loginStyles from "styles/loginStyles";

const LoginScreenLayout = ({children}) => {

  const {colors} = useTheme();

  return (
    <View style={[styles.safeAreaContainer, {backgroundColor: colors.background}]}>
      <View style={{flex: 6, zIndex: 2}}>
        <View style={loginStyles.appLogoContainer}>
          <Image
            style={loginStyles.appLogo}
            source={require("../../../assets/skedway_logo.png")}
          />
        </View>

        {children}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    flexDirection: "column",
    marginTop: 0,
    position: "relative"
  },
});

export default LoginScreenLayout;