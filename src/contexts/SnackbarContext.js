import React, {createContext, useState} from "react";

export const SnackbarContext = createContext({});

const SnackbarProvider = ({children}) => {

  const [notification, setNotification] = useState(null);

  return (
    <SnackbarContext.Provider value={{notification, setNotification}}>
      {children}
    </SnackbarContext.Provider>
  )

}

export default SnackbarProvider;
