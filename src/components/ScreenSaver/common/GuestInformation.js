import React, {useContext, useEffect, useState} from "react";
import {Text, View} from "react-native";
import {useTheme} from "react-native-paper";
import i18n from "i18n-js";

import {SchedulingContext} from "contexts/SchedulingContext";
import {StorageContext} from "contexts/StorageContext";

import {getNextMeeting, getNowHappeningMeeting, getScheduleGuests, getScheduleHost,} from "utils/schedulerUtils";

import useSync from "hooks/useSync";

const GuestInformation = (props) => {

  const {colors} = useTheme();
  const today = useSync();

  const {fontSize = 20, isNextSchedule = false, color = colors.text} = props;

  const {appStorage} = useContext(StorageContext);
  const {todayDayScheduling} = useContext(SchedulingContext);
  const {space} = appStorage;

  const [hostName, setHostName] = useState(null);
  const [guests, setGuests] = useState(null);

  const startedSchedule = getNowHappeningMeeting(today, todayDayScheduling);
  const nextSchedule = getNextMeeting(today, todayDayScheduling);
  const spaceCapacity = space && space.capacity;

  useEffect(() => {
    const schedule = startedSchedule || nextSchedule;

    if (schedule) {
      setValues(schedule);
    } else {
      setDefaultValues();
    }
  }, [startedSchedule, nextSchedule, spaceCapacity]);

  const setValues = (schedule) => {
    const host = getScheduleHost(schedule);
    const guests = getScheduleGuests(schedule);

    setHostName(host.name);
    setGuests(guests.length);
  };

  const setDefaultValues = () => {
    spaceCapacity === 1
      ? i18n.t("one", {input: spaceCapacity})
      : i18n.t("other", {input: spaceCapacity || 0});

    setHostName(i18n.t("Capacity"));
    setGuests(spaceCapacity);
  };

  const getStyle = () => ({
    paddingLeft: 8,
    color,
    fontSize: fontSize,
    fontFamily: "Manrope_400Regular"
  });

  function getAvailabilityText() {
    const {space} = appStorage;

    const spaceName = space?.baseType === 1 ? i18n.t("desk") : i18n.t("room");
    const people = i18n.t("peoples", {count: getTotalGuests()})
    return `${i18n.t("placesAvailability", {0: spaceName, 1: people})}`;
  }

  const getTotalGuests = () => (guests ? Number.parseInt(guests) : 0);

  return (
    <View style={{flexDirection: "row"}}>
      {
        !isNextSchedule &&
        <>
          <Text style={getStyle()}>
            {getAvailabilityText()}
          </Text>
        </>
      }
      {
        isNextSchedule &&
        <>
          <Text style={getStyle()}>{hostName}</Text>
          <Text style={getStyle()}>
            {i18n.t("places", {count: getTotalGuests()})}
          </Text>
        </>
      }

    </View>
  );
};

export default GuestInformation;
