import React from "react";
import { StyleSheet } from "react-native";
import { Caption, useTheme } from "react-native-paper";

import i18n from "i18n-js";

const ResponseText = ({ response }) => {
  const { colors } = useTheme();

  function getStyleAndLabel() {
    switch (response) {
      case 1:
        return {
          style: styles.responseString,
          label: i18n.t("Not_responded"),
        };
      case 2:
        return {
          style: [styles.responseString, { color: colors.primary }],
          label: i18n.t("Accepted"),
        };
      case 3:
        return {
          style: [styles.responseString, { color: colors.error }],
          label: i18n.t("Declined"),
        };
      case 4:
        return {
          style: styles.responseString,
          label: i18n.t("Maybe"),
        };
      default:
        return {
          style: null,
          label: null,
        };
    }
  }

  return (
    <Caption
      ellipsizeMode="tail"
      numberOfLines={1}
      style={getStyleAndLabel().style}
    >
      {getStyleAndLabel().label}
    </Caption>
  );
};

const styles = StyleSheet.create({
  responseString: {
    alignSelf: "flex-end",
    marginRight: "auto",
    marginBottom: 4,
    fontSize: 16,
    flex: 1,
  },
});

export default ResponseText;
