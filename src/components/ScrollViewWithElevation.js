import React from "react";
import { ScrollView } from "react-native";

const ScrollViewWithElevation = ({
  min = 0,
  max = 3,
  elevationCallback = () => {},
  throttle = 250,
  children,
  ...otherProps
}) => {
  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      scrollEventThrottle={throttle}
      onScroll={({ nativeEvent: { contentOffset } }) => {
        if (contentOffset.y <= 0) {
          elevationCallback(min);
        } else {
          elevationCallback(max);
        }
      }}
      {...otherProps}
    >
      {children}
    </ScrollView>
  );
};

export default ScrollViewWithElevation;
