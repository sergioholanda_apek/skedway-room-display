import React from "react";
import {StyleSheet, Text, View} from "react-native";
import {Checkbox, useTheme} from "react-native-paper";

import uuid from "react-native-uuid";

import normalize from "utils/normalize";

const Facilities = (props) => {

  const {
    assets,
    amenities,
    changeAssetsStatus,
    changeAmenityStatus
  } = props;

  const {colors} = useTheme();

  return (
    <>
      {
        assets.map(asset => (
          <View
            key={uuid.v4()}
            style={styles.checkbox}
          >
            <Checkbox.Android
              position="leading"
              color={colors.available}
              uncheckedColor={colors.disabled}
              onPress={() => changeAssetsStatus(asset)}
              status={asset.status ? "checked" : "unchecked"}
            />
            <Text style={styles.checkboxLabel}>{asset.title}</Text>
          </View>
        ))
      }

      {
        amenities.map(amenity => (
          <View
            key={uuid.v4()}
            style={styles.checkbox}
          >
            <Checkbox.Android
              position="leading"
              color={colors.available}
              uncheckedColor={colors.disabled}
              onPress={() => changeAmenityStatus(amenity)}
              status={amenity.status ? "checked" : "unchecked"}
            />
            <Text style={styles.checkboxLabel}>{amenity.amenityName}</Text>
          </View>
        ))
      }
    </>
  );
};

const styles = StyleSheet.create({
  checkbox: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: normalize(4),
    marginLeft: normalize(28)
  },
  checkboxLabel: {
    fontFamily: "Manrope_400Regular",
    fontSize: normalize(8)
  },
  mainContainer: {
    paddingBottom: 16,
  },
  container: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: normalize(16),
    paddingVertical: 16,
  },
  tvIcon: {
    paddingRight: 20,
  },
  title: {
    fontWeight: "400",
  },
  warningIcon: {
    position: "absolute",
    right: normalize(16),
    top: 16,
    fontSize: 26,
    color: "red",
  },
  errorContainer: {
    position: "absolute",
    right: normalize(13),
    bottom: -16,
    color: "white",
    backgroundColor: "black",
    borderTopWidth: 3,
    borderTopColor: "red",
  },
  arrowUp: {
    borderLeftWidth: 8,
    borderRightWidth: 8,
    borderBottomWidth: 10,
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: "red",
    right: 10,
    top: -10,
    position: "absolute",
  },
  helperText: {
    color: "white",
    margin: 3,
    backgroundColor: "black",
  },
  divider: {
    height: 1.2,
  },
});

export default Facilities;
