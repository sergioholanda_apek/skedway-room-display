import * as React from "react"
import {SvgXml} from "react-native-svg"

const xml = `
<svg width="566" height="189" viewBox="0 0 586 189" fill="none" xmlns="http://www.w3.org/2000/svg">
<g clip-path="url(#clip0_1519_38623)">
<rect width="424" height="138" rx="5.82069" fill="white"/>
<path d="M-18.8642 470.433L192.639 563.907L192.639 126.595L128.881 136.686L-18.8643 106.432L-18.8642 470.433Z" fill="url(#paint0_linear_1519_38623)" fill-opacity="0.93"/>
<path d="M426.73 -64.0353V91.1376L213.804 -4.19643V-160L426.73 -64.0353Z" fill="url(#paint1_linear_1519_38623)"/>
<path d="M-20.1523 -49.4238V105.565L213.798 -5.01106V-160L-20.1523 -49.4238Z" fill="url(#paint2_linear_1519_38623)" fill-opacity="0.62"/>
<path d="M192.511 502.94L428.037 366.835L426.733 91.1377L192.511 190.7L192.511 502.94Z" fill="url(#paint3_linear_1519_38623)"/>
<path d="M-19.432 106.115L216.739 -6.82995L426.732 91.1307L192.751 204.846L-19.432 106.115Z" fill="url(#paint4_linear_1519_38623)"/>
<g filter="url(#filter0_d_1519_38623)">
<rect width="171.468" height="74.0008" rx="0.336987" transform="matrix(-0.906653 -0.421877 -0.906653 0.421877 370.404 65.0415)" fill="url(#paint5_linear_1519_38623)"/>
</g>
<rect width="39.5196" height="39.5029" transform="matrix(0.906653 -0.421877 0.906653 0.421877 163.191 91.6191)" fill="url(#paint6_linear_1519_38623)"/>
<rect width="18.082" height="36.7172" transform="matrix(0.906653 -0.421877 0.906653 0.421877 212.676 41.5869)" fill="url(#paint7_linear_1519_38623)"/>
<path d="M199.043 69.6081V107.667L163.213 90.9274V52.8682L199.043 69.6081Z" fill="white"/>
<path d="M284.748 9.44734V47.1726L234.476 25.215L235.834 -13.1543L284.748 9.44734Z" fill="url(#paint8_linear_1519_38623)"/>
</g>
<defs>
<filter id="filter0_d_1519_38623" x="112.135" y="-22.7352" width="293.984" height="175.179" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="20.3724"/>
<feGaussianBlur stdDeviation="17.9471"/>
<feComposite in2="hardAlpha" operator="out"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1519_38623"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1519_38623" result="shape"/>
</filter>
<linearGradient id="paint0_linear_1519_38623" x1="630.865" y1="240.297" x2="275.394" y2="670.072" gradientUnits="userSpaceOnUse">
<stop offset="0.0519365" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white" stop-opacity="0"/>
</linearGradient>
<linearGradient id="paint1_linear_1519_38623" x1="760.495" y1="179.019" x2="64.1234" y2="-404.984" gradientUnits="userSpaceOnUse">
<stop offset="0.0519365" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white" stop-opacity="0"/>
</linearGradient>
<linearGradient id="paint2_linear_1519_38623" x1="590.625" y1="-111.404" x2="-73.1215" y2="103.587" gradientUnits="userSpaceOnUse">
<stop offset="0.395833" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white" stop-opacity="0"/>
</linearGradient>
<linearGradient id="paint3_linear_1519_38623" x1="780.561" y1="-130.896" x2="903.01" y2="226.076" gradientUnits="userSpaceOnUse">
<stop offset="0.0519365" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint4_linear_1519_38623" x1="566.067" y1="-122.583" x2="130.764" y2="302.846" gradientUnits="userSpaceOnUse">
<stop offset="0.0519365" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint5_linear_1519_38623" x1="36.9658" y1="86.7303" x2="34.8211" y2="11.5229" gradientUnits="userSpaceOnUse">
<stop stop-color="white"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint6_linear_1519_38623" x1="-120.208" y1="41.3539" x2="48.6915" y2="25.6548" gradientUnits="userSpaceOnUse">
<stop stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint7_linear_1519_38623" x1="76.201" y1="17.1292" x2="-1.13725" y2="0.600686" gradientUnits="userSpaceOnUse">
<stop offset="0.0519365" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint8_linear_1519_38623" x1="72.8903" y1="14.9893" x2="271.726" y2="-56.9186" gradientUnits="userSpaceOnUse">
<stop offset="0.0519365" stop-color="#C9C1E7"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<clipPath id="clip0_1519_38623">
<rect width="424" height="138" rx="5.82069" fill="white"/>
</clipPath>
</defs>
</svg>
`;

export default (props) => <SvgXml {...props} xml={xml} />;
