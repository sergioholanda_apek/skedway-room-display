import * as React from "react"
import Svg, {Path} from "react-native-svg"

const PersonFill = ({height, width, color}) => {

  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      fill={color}
      viewBox="0 0 15 17"
      width={height}
      height={width}
    >
      <Path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
    </Svg>
  )
}

export default PersonFill;
