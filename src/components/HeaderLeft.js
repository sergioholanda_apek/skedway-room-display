import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { Feather } from "@expo/vector-icons";
import normalize from "utils/normalize";

const HeaderLeft = ({ onIconPress }) => (
  <View style={styles.iconContainer}>
    <TouchableOpacity
      onPress={onIconPress}
      centered
      rippleColor="rgba(0, 0, 0, .32)"
    >
      <Feather name="arrow-left" style={styles.backIcon} />
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  backIcon: {
    fontSize: 24,
    padding: 0,
    color: "#7A7A7A",
  },
  iconContainer: {
    marginLeft: normalize(16),
  },
});

export default HeaderLeft;
