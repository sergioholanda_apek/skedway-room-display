import React, {useContext, useEffect} from "react";
import {StyleSheet, Text} from "react-native";
import {ActivityIndicator} from "react-native-paper";

import i18n from "i18n-js";
import {isToday} from "date-fns";

import MainLayout from "components/common/layout/MainLayout";
import SchedulerHeader from "components/Header/SchedulerHeader";
import TimelineWrapper from "components/Home/Timeline/TimelineWrapper";

import {SchedulingContext} from "contexts/SchedulingContext";
import {SyncContext} from "contexts/SyncContext";
import {ScreenSaverContext} from "contexts/ScreenSaverContext";
import {LoadingContext} from "contexts/LoadingContext";

import HomeDoor from "../../assets/icons/HomeDoor";

import normalize from "utils/normalize";

const Home = ({navigation}) => {

  const resetTimeline = navigation.getParam("resetTimeline");

  const {
    isFetchingDayScheduling,
    setCurrentDay,
    currentDay,
    fetchDayScheduling,
    setSelectedSchedule
  } = useContext(SchedulingContext);

  const {sync} = useContext(SyncContext);

  const {setActive} = useContext(ScreenSaverContext);

  const {setLoadingResource} = useContext(LoadingContext);

  const isItToday = isToday(new Date(currentDay));

  /**
   * HomeScreen will be responsible for keeping in memory everything that
   * comes out of the localStorage information or the login information
   */
  useEffect(() => {
    const listener = navigation.addListener("didFocus", () => {
      setSelectedSchedule(null);
    });

    setActive();

    return () => {
      listener.remove();
    };
  }, []);


  useEffect(() => {
    setLoadingResource({show: isFetchingDayScheduling, isTransparent: false});
  }, [isFetchingDayScheduling]);

  useEffect(() => {
    if (currentDay) {
      sync();
      fetchDayScheduling(isItToday);
    } else {
      setCurrentDay(new Date());
    }
  }, [currentDay]);

  useEffect(() => {
    setCurrentDay(new Date());
  }, [resetTimeline]);

  return (
    <MainLayout>
      <SchedulerHeader/>
      {
        isFetchingDayScheduling ? (
          <ActivityIndicator
            size="large"
            style={styles.loadingIndicator}
            animating={true}
            color="green"
          />
        ) : (
          <TimelineWrapper/>
        )
      }
    </MainLayout>
  );
};

Home.navigationOptions = ({navigation}) => {
  const isPortrait = navigation.getParam("isPortrait");

  return {
    tabBarLabel: ({focused}) => {
      return (
        <Text
          style={{
            color: focused ? "#717171" : "#BCC6C6",
            fontFamily: "Manrope_400Regular",
            fontSize: Math.max(normalize(7), 11),
            textAlign: "center",
          }}
        >
          {i18n.t("title_home")}
        </Text>
      );
    },
    tabBarIcon: ({focused}) => {
      return (
        <HomeDoor height="100%" width="100%" focused={focused}/>
      );
    },
  };
};

const styles = StyleSheet.create({
  loadingIndicator: {
    position: "absolute",
    top: 340,
    alignSelf: "center",
  },
});

export default Home;
