import * as React from "react"
import Svg, {Path} from "react-native-svg"
import {useTheme} from "react-native-paper";

const KeyFill = (props) => {

  const {height, width} = props;
  const {colors} = useTheme();

  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      fill={colors.active}
      viewBox="0 0 15 16"
      width={height}
      height={width}
    >
      <Path
        d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
    </Svg>
  )
}

export default KeyFill;