import React, {useState} from "react";

import {axios_, getAuthToken} from "api";

import {SYNC_CHECKIN} from "api/endpoints";

import useAsyncStorage from "hooks/useAsyncStorage";

let authToken = undefined;

export const SyncCheckInsContext = React.createContext({});

const SyncCheckInsProvider = ({children}) => {

  const [checkIn, setCheckIn] = useState([]);
  const [isSyncingCheckIns, setSyncingCheckIns] = useState(false);
  const [checkInsList, mergeIntoCheckInsList, clearCheckInsList] = useAsyncStorage("@FAILED_CHECKINS", []);

  async function syncCheckIns(checkIn_ = checkIn, onSuccessCallback, onErrorCallback) {
    setSyncingCheckIns(true);
    authToken = await getAuthToken();
    try {
      if (checkIn_.length) {
        await axios_.post(SYNC_CHECKIN, null, {
          params: {
            authToken,
            checkIns: JSON.stringify(checkIn_),
          },
        });

        if (onSuccessCallback) {
          onSuccessCallback();
        }
      }
    } catch (error) {
      if (onErrorCallback) {
        onErrorCallback();
      }
      setSyncingCheckIns(false);
      throw new Error(error.message);
    } finally {
      setSyncingCheckIns(false);
    }
  }

  return (
    <SyncCheckInsContext.Provider
      value={{
        checkInsList,
        mergeIntoCheckInsList,
        clearCheckInsList,
        checkIn,
        setCheckIn,
        syncCheckIns,
        isSyncingCheckIns
      }}
    >
      {children}
    </SyncCheckInsContext.Provider>
  );
};

export default SyncCheckInsProvider;
