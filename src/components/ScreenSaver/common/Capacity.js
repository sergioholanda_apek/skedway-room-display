import React from "react";
import { useTheme } from "react-native-paper";
import { View } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import normalize from "utils/normalize";

import GuestInformation from "./GuestInformation";

const Capacity = (props) => {
  const { colors } = useTheme();

  const {
    direction = "column",
    size = "small",
    fontSize = normalize(5),
    alignItems = "center",
    isNextSchedule,
    color = colors.text
  } = props;

  const calculateSize = () => {
    switch (size) {
      case "small":
        return normalize(10);
      case "medium":
        return normalize(18);
      case "large":
        return normalize(26);
      default:
        return normalize(10);
    }
  };

  return (
    <View style={{ flexDirection: direction, alignItems: alignItems }}>
      <MaterialCommunityIcons
        name="account"
        size={calculateSize()}
        style={{color}}
      />
      <GuestInformation fontSize={fontSize} isNextSchedule={isNextSchedule} color={color}/>
    </View>
  );
};

export default Capacity;
