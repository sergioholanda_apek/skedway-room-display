import React, {useContext, useEffect, useState} from "react";
import {PanResponder, View} from "react-native";
import _ from "lodash";

import {StorageContext} from "contexts/StorageContext";
import {ScreenSaverContext} from "contexts/ScreenSaverContext";

import {navigate} from "utils/navigationRef";

const ScreenSaverController = ({children}) => {
  const [resetTrigger, setResetTrigger] = useState(false);

  const {appStorage} = useContext(StorageContext);
  const {space} = appStorage;

  const {active, setActive} = useContext(ScreenSaverContext);

  useEffect(() => {
    if (space && !active) {
      navigate("Home", {resetTimeline: resetTrigger});
      setResetTrigger(!resetTrigger);
      if (space.screensaver) {
        navigate("ScreenSaver");
      }
    }
  }, [active]);

  const panResponder = React.useRef(
    PanResponder.create({
      onStartShouldSetPanResponderCapture: _.debounce(() => {
        setActive();
        return false;
      }, 300),
    })
  ).current;

  return (
    <View style={{flex: 1}} {...panResponder.panHandlers}>
      {children}
    </View>
  );
};

export default ScreenSaverController;
