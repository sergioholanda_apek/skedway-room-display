import * as React from "react"
import Svg, {Path, Rect} from "react-native-svg"
import {useTheme} from "react-native-paper";

const Chair = (props) => {

  const {height, width} = props;
  const {colors} = useTheme();

  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      fill={colors.active}
      viewBox="0 0 22 34"
      width={height}
      height={width}
    >
      <Rect x={4.682} y={0.703} width={13.715} height={17.889} rx={1.839}/>
      <Rect x={2.944} y={19.915} width={17.189} height={5.056} rx={1.839}/>
      <Rect x={19.588} y={10.838} width={2.982} height={8.348} rx={1.011}/>
      <Rect x={0.508} y={10.838} width={2.982} height={8.348} rx={1.011}/>
      <Path
        stroke={colors.active}
        strokeWidth={2.385}
        d="M11.241 24.608v4.174M17.056 29.474H6.024"
      />
    </Svg>
  )
}

export default Chair
