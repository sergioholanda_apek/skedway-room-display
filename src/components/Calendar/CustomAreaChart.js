import React from "react";
import {View, StyleSheet, Text, Dimensions} from "react-native";
import {Title} from "react-native-paper";
import {Path} from "react-native-svg";

import i18n from "i18n-js";

import SkedwayLineChart from "components/Calendar/SkedwayLineChart";

import normalize from "utils/normalize";

const percentageArray = [...Array(101).keys()];
const monthArray = [...Array(31).keys()];

const GraphicLine = ({line}) => (
  <Path strokeWidth={2} key="line" d={line} stroke={"#4caf50"} fill={"none"}/>
);

const CustomAreaChart = ({data}) => {

  data = data ?? [];

  const getLabels = () => {
    return monthArray.map((day) => {
      if ((day+1) % 5 === 0) return day+1;
      else return ""
    });
  }

  return (
    <View style={{alignItems: "center"}}>
      <Title style={styles.title}>
        {i18n.t("title_occupation_by_day")}
      </Title>
      <View style={{padding: normalize(8)}}>
        <SkedwayLineChart
          data={{
            labels: getLabels(),
            datasets: [
              {
                data: data,
              },
              {
                data: [0] // min
              },
              {
                data: [100] // max
              },
            ],
          }}
          width={Dimensions.get('window').width - 120}
          height={normalize(150)}
          chartConfig={{
            backgroundGradientFrom: '#fbfbfb',
            backgroundGradientTo: '#fbfbfb',
            color: (opacity = 1) => 'url(#grad)',
            labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            propsForBackgroundLines: {
              stroke: "lightgrey",
              strokeWidth: 0.7,
              strokeDasharray: "" // solid background lines with no dashes
            },
            strokeWidth: 2
          }}
          bezier
          withOuterLines={false}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  yAxis: {
    marginBottom: 30,
  },
  xAxis: {
    marginHorizontal: -10,
    height: 30,
  },
  mainContainer: {
    height: Math.max(normalize(150), 200),
    flexDirection: "row",
  },
  graphicContainer: {
    flex: 1,
  },
  xAxisContentInset: {
    right: 10,
  },
  contentInset: {
    top: 10,
    bottom: 10,
  },
  flex: {
    flex: 1,
  },
  title: {
    marginBottom: normalize(12),
    fontFamily: "LibreBaskerville_400Regular_Italic",
    fontSize: normalize(12)
  },
});

export default CustomAreaChart;
