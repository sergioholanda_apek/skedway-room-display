import React from "react";
import { StyleSheet, View } from "react-native";
import { Avatar, Caption, Subheading } from "react-native-paper";
import normalize from "utils/normalize";

const Host = ({startedSchedule, nextSchedule}) => {

  const currentSchedule  = startedSchedule ? startedSchedule : nextSchedule;
  const { subject, attendees } = currentSchedule;
  const host = attendees && attendees.find((user) => user.host);
  const { userPictureUrl, name } = host;

  return (
    <View style={styles.schedulerHostContainer}>
      <View style={{ alignSelf: "center" }}>
        {subject ? (
          <Subheading style={styles.text}>{subject}</Subheading>
        ) : null}
        {name ? (
          <Caption style={[styles.text, { marginTop: -4 }]}>{name}</Caption>
        ) : null}
      </View>
      {userPictureUrl ? (
        <Avatar.Image size={60} source={{ uri: userPictureUrl }} />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  schedulerHostContainer: {
    paddingVertical: normalize(8),
    flexDirection: "row",
    justifyContent: "space-between",
  },
  text: {
    color: "white",
    textShadowColor: "rgba(0, 0, 0, 0.2)",
    textShadowOffset: { width: 0.5, height: 0.5 },
    textShadowRadius: 2,
  },
});

export default Host;
