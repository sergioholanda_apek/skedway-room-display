import {
  parse,
  getMinutes,
  addMinutes,
  format,
  isBefore,
  isAfter,
  differenceInSeconds,
} from "date-fns";

import {
  datesDifference,
  parseWithNoTZ,
  getHour,
  getHourWithoutParse,
  isHour,
} from "./dateUtils";

export const ONE_MINUTE = 60;
export const MIN_HEIGHT_PROPORTION = 0.55;

const sortTimelineBasedOnDates = (timeline) =>
  timeline.sort((schedule1, schedule2) => {
    const date1 = parseWithNoTZ(schedule1.startDate);
    const date2 = parseWithNoTZ(schedule2.startDate);

    if (isBefore(date1, date2)) {
      return -1;
    }

    if (isBefore(date2, date1)) {
      return 1;
    }

    return 0;
  });

const validSchedules = ({
  openingTime,
  today,
  dayScheduling,
  locale,
  timeZone,
}) => {
  if (!today) {
    return { newDaySchedule: dayScheduling, newOpeningTime: openingTime };
  }
  let newOpeningTime = openingTime;
  const newDaySchedule = dayScheduling.filter((schedule) => {
    const startDate = parseWithNoTZ(schedule.startDate);
    const todayDate = new Date(today);
    const seconds = Math.max(differenceInSeconds(startDate, todayDate), 0);
    const minutesDiff = Math.ceil(seconds / ONE_MINUTE);

    if (minutesDiff === 0) {
      const nowOpeningTime = parse(openingTime, "HH:mm:ss", new Date());
      newOpeningTime = isAfter(parseWithNoTZ(schedule.endDate), nowOpeningTime)
        ? getHour({
            date: schedule.endDate,
            locale: locale,
            options: { timeZone },
          })
        : getHourWithoutParse({
            date: nowOpeningTime,
            locale: locale,
            options: { timeZone },
          });
    }
    return minutesDiff > 0;
  });

  return { newDaySchedule, newOpeningTime };
};

const getTimeChunk = ({
  startDate,
  endDate,
  hourFormat,
  fulfill = false,
  locale = "pt-BR",
  timeZone = "America/Recife",
  minHeightProportion = MIN_HEIGHT_PROPORTION,
  ...otherProps
}) => {
  //TODO: Remove the fixed pt-BR from the locale. Build the timeline based on the site.hourFormat24.
  const startDateHour = isHour(startDate)
    ? startDate
    : getHour({ date: startDate, locale, options: { timeZone } });

  const endDateHour = isHour(endDate)
    ? endDate
    : getHour({ date: endDate, locale, options: { timeZone } });

  const diff = datesDifference(startDateHour, endDateHour);

  if (!diff || diff < 0) {
    return [];
  }

  /*
   * If fulfill is false, timechunk should be splitted by hour,
   * i.e., no meeting is booked at that time period
   */
  if (!fulfill) {
    const iterations = Math.ceil(diff / ONE_MINUTE);
    return [...Array(iterations).keys()].reduce((acc, _, index) => {
      const first = index === 0;
      const last = index === iterations - 1;
      const parsedStartDateHour = parse(startDateHour, "HH:mm:ss", new Date());
      const minutesToAdd = ONE_MINUTE * index - getMinutes(parsedStartDateHour);
      const increasedDate = addMinutes(parsedStartDateHour, minutesToAdd);

      if (first) {
        const firstHour = format(parsedStartDateHour, hourFormat);
        const minutes = getMinutes(parsedStartDateHour);
        const isGapBiggerThanAnHour = diff >= 60;
        const height = isGapBiggerThanAnHour
          ? 1 - minutes / ONE_MINUTE
          : diff / ONE_MINUTE;

        return [
          ...acc,
          {
            title: firstHour,
            heightProportion: Math.max(height, minHeightProportion),
            openingTime: startDateHour,
            closingTime: endDateHour,
          },
        ];
      }

      if (last) {
        const lastHour = format(increasedDate, hourFormat);
        const minutesDiff = datesDifference(
          format(increasedDate, "HH:mm:ss"),
          endDateHour
        );
        return [
          ...acc,
          {
            title: lastHour,
            heightProportion: Math.max(
              minutesDiff / ONE_MINUTE,
              minHeightProportion
            ),
            openingTime: startDateHour,
            closingTime: endDateHour,
          },
        ];
      }

      return [
        ...acc,
        {
          title: format(increasedDate, hourFormat),
          heightProportion: 1,
          openingTime: startDateHour,
          closingTime: endDateHour,
        },
      ];
    }, []);
  }

  // Otherwise, timechunk should be fulfilled, i.e., a meeting is booked at that time
  const heightProportion = Math.max(diff / ONE_MINUTE, 1);
  const formatedStartDateHour = format(
    parse(startDateHour, "HH:mm:ss", new Date()),
    hourFormat
  );

  const formatedEndDateHour = format(
    parse(endDateHour, "HH:mm:ss", new Date()),
    hourFormat
  );

  const title = fulfill
    ? `${formatedStartDateHour} - ${formatedEndDateHour}`
    : formatedStartDateHour;

  return [{ title, heightProportion, fulfill, ...otherProps }];
};

const buildTimeline = ({
  today,
  openingTime,
  closingTime,
  dayScheduling,
  ...otherProps
}) => {
  const sortedDayScheduling = sortTimelineBasedOnDates(dayScheduling);
  const { newDaySchedule: finalSchedules, newOpeningTime } = validSchedules({
    openingTime,
    today,
    dayScheduling: sortedDayScheduling,
    ...otherProps,
  });

  const updatedSchedules = finalSchedules
    .reduce((acc, schedule, index) => {
      const first = index === 0;
      const last = index === finalSchedules.length - 1;
      const previous = !first && finalSchedules[index - 1];

      /*
       * This is a reserved schedule chunk, i.e., a meeting is booked at that
       * time period. From schedule.startDate to schedule.endDate
       */
      const currentChunk = getTimeChunk({
        ...otherProps,
        startDate: schedule.startDate,
        endDate: schedule.endDate,
        timeZone: schedule.timezone, // we should pass its own timezone
        userPictureUrl: schedule.userPictureUrl,
        userName: schedule.userName,
        userEmail: schedule.userEmail,
        subject: schedule.subject,
        attendees: schedule.attendees,
        schedulingId: schedule.id,
        userId: schedule.userId,
        fulfill: true,
      });

      // To handle if there's only one meeting at the day
      if (finalSchedules.length === 1) {
        return [
          ...acc,
          ...getTimeChunk({
            startDate: newOpeningTime,
            endDate: schedule.startDate,
            ...otherProps,
          }),
          ...currentChunk,
          ...getTimeChunk({
            startDate: schedule.endDate,
            endDate: closingTime,
            ...otherProps,
          }),
        ];
      }

      // To handle the first schedule chunk
      if (first) {
        return [
          ...acc,
          ...getTimeChunk({
            startDate: newOpeningTime,
            endDate: schedule.startDate,
            ...otherProps,
          }),
          ...currentChunk,
        ];
      }

      // To handle the last schedule chunk
      if (last) {
        return [
          ...acc,
          ...getTimeChunk({
            startDate: previous.endDate,
            endDate: schedule.startDate,
            ...otherProps,
          }),
          ...currentChunk,
          ...getTimeChunk({
            startDate: schedule.endDate,
            endDate: closingTime,
            ...otherProps,
          }),
        ];
      }

      // To handle inner schedule chunks that are empty (no meeting is booked)
      return [
        ...acc,
        ...getTimeChunk({
          startDate: previous.endDate,
          endDate: schedule.startDate,
          ...otherProps,
        }),
        ...currentChunk,
      ];
    }, [])
    .filter((schedule) => schedule);

  return { updatedSchedules, newOpeningTime };
};

export default buildTimeline;
