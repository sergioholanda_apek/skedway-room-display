import React, { useState } from "react";
import {View, Image, TextInput, StyleSheet, Text} from "react-native";
import { Divider, Title, useTheme } from "react-native-paper";

import i18n from "i18n-js";

import normalize from "utils/normalize";

import CustomDialog from "../common/CustomDialog";
import NumberPad from "./NumberPad";

const MAX_PIN_LENGTH = 4;

const PinDialog = (props) => {

  const {colors} = useTheme();

  const {
    visible,
    setDialogOpen,
    pictureUrl,
    name,
    onConfirmCallback,
    pinToValidate,
  } = props;

  const { unit } = useTheme();

  const [pinValue, setPinValue] = useState("");
  const [error, setError] = useState("");
  const cleanUp = () => {
    setDialogOpen(null);
    setPinValue("");
  };

  const pinIsValidated = pinValue === pinToValidate;

  return (
    <CustomDialog
      style={[styles.pinDialogContainer, {backgroundColor: colors.white}]}
      visible={visible}
      onDismiss={cleanUp}
      dismissable={false}
    >
      <View style={styles.profileContainer}>
        <Image
          style={[styles.image, { marginVertical: unit(2) }]}
          source={{ uri: "https://static.vecteezy.com/system/resources/thumbnails/002/275/847/small/male-avatar-profile-icon-of-smiling-caucasian-man-vector.jpg" }}
        />
        <Text style={{fontFamily: "Manrope_600SemiBold", fontSize: normalize(8), color: colors.onSurface}}>
          Sérgio Holanda
        </Text>

        <Text style={{marginTop: normalize(6), fontFamily: "Manrope_500Medium", textAlign: "center", fontSize: normalize(8), color: colors.tintColor, flexWrap: "wrap"}}>
          Digite seu PIN para confirmar o agendamento
        </Text>
      </View>

      <View style={{marginTop: normalize(10), borderTopLeftRadius: normalize(10), borderTopRightRadius: normalize(10), backgroundColor: colors.background}}>
        {error ? (
          <Title style={styles.error}>{error}</Title>
        ) : (
          <>
            <TextInput
              style={[styles.pinInput, { padding: unit(0.5) }]}
              editable={false}
              value={pinValue}
              secureTextEntry
            />
            <View style={{width: 150, height: 5, backgroundColor: colors.disabled, alignSelf: "center", textAlign: "center"}}/>
          </>
        )}

        <NumberPad
          onDeleteCallback={() => {
            if (!pinValue) {
              setDialogOpen(null);
              setError("");
              return;
            }
            setPinValue(`${pinValue.slice(0, -1)}`);
          }}
          onConfirmCallback={() => {
            if (!pinIsValidated) {
              setPinValue("");
              setError(i18n.t("error_invalid_pin"));
              return;
            }

            setDialogOpen(null);
            setPinValue("");
            onConfirmCallback();
          }}
          onClickCallback={(number) => {
            if (error) {
              setError("");
            }
            if (pinValue.length < MAX_PIN_LENGTH) {
              setPinValue(`${pinValue}${number}`);
            }
          }}
        />
      </View>
    </CustomDialog>
  );
};

const styles = StyleSheet.create({
  pinDialogContainer: {
    width: normalize(150),
    minWidth: normalize(150),
    alignSelf: "center",
  },
  profileContainer: {
    alignItems: "center",
  },
  image: {
    height: 90,
    width: 90,
    borderRadius: normalize(10),
    resizeMode: "contain",
  },
  pinContainer: {
    height: 50,
  },
  pinInput: {
    minWidth: 100,
    backgroundColor: "transparent",
    alignSelf: "center",
    textAlign: "center",
    fontSize: 20,
    marginTop: normalize(12)
  },
  error: {
    alignSelf: "center",
    color: "#900000",
    fontSize: 20,
  },
});

export default PinDialog;
