import {differenceInSeconds, differenceInMinutes, addMinutes} from "date-fns";
import {parseWithNoTZ} from "./dateUtils";
import isEmpty from "lodash/isEmpty";
import {LEVEL_PRESIDENT} from "utils/spaceLevel";

const ONE_MINUTE = 60;
const MAX_NUMBER_TO_COMPARE = 9999999999;

export const getNowHappeningMeeting = (today, dayScheduling) => {
  return dayScheduling.reduce((acc, schedule) => {
    const start_seconds = differenceInSeconds(
      parseWithNoTZ(schedule.startDate),
      today
    );

    const end_seconds = differenceInSeconds(
      parseWithNoTZ(schedule.endDate),
      today
    );

    const startMinutesDiff = Math.ceil(start_seconds / ONE_MINUTE);
    const endMinutesDiff = Math.ceil(end_seconds / ONE_MINUTE);

    if (startMinutesDiff <= 0 && endMinutesDiff > 0) {
      return schedule;
    }
    return acc;
  }, null);
};

export const getNextMeeting = (today, dayScheduling) => {
  const reducedObject = dayScheduling.reduce(
    (acc, schedule) => {
      const diff = differenceInMinutes(
        parseWithNoTZ(schedule.startDate),
        today
      );

      if (diff >= 0 && diff < acc.timeToCompare) {
        return {nextMeeting: schedule, timeToCompare: diff};
      }
      return acc;
    },
    {nextMeeting: null, timeToCompare: MAX_NUMBER_TO_COMPARE}
  );
  return reducedObject.nextMeeting;
};

export const getScheduleHost = (schedule) => {
  if (!schedule) {
    return null;
  }

  const {attendees} = schedule;

  return attendees.find((att) => att.host);
};

export const getScheduleGuests = (schedule) => {
  if (!schedule) {
    return null;
  }

  const {attendees} = schedule;

  return attendees.filter((att) => !att.host && !att.isResource);
};

export const getStatusColor = (appStorage, startedSchedule, nextSchedule, colors, checkIns) => {
  const {error, intermediate, inactive, darkGreen} = colors;

  if (verifyAtLeastOneCheckIn(checkIns, startedSchedule, nextSchedule)) {
    return error;
  } else if (startedSchedule) {
    return intermediate;
  } else {
    const isPresident = verifyPresident(appStorage);
    return verifyCheckInAdvance(appStorage, nextSchedule) ? intermediate : (isPresident ? inactive : darkGreen)
  }
};

export const verifyCheckInAdvance = (appStorage, nextSchedule) => {
  const {company, space} = appStorage;
  const checkInAdvance = space?.baseType === 1 ? company?.checkInAdvance ?? 0 : company?.checkInAdvance2 ?? 0;

  if (nextSchedule) {
    const {startDate} = nextSchedule;
    const startDateParsed = parseWithNoTZ(startDate)
    const availableCheckIn = addMinutes(new Date(), checkInAdvance);
    return availableCheckIn.getTime() >= new Date(startDateParsed).getTime();
  }
  return false;
};

export const verifyAtLeastOneCheckIn = (checkIns, startedSchedule, nextSchedule) => {
  if (!isEmpty(checkIns)) {
    return true;
  }
  if (startedSchedule?.qtyCheckIn > 0) {
    return true;
  }
  return nextSchedule?.qtyCheckIn > 0;
}

export const verifyPresident = (loginInfoStorage) => (loginInfoStorage.space?.level === LEVEL_PRESIDENT);

export const StatusScheduling = Object.freeze({
  FREE: 0,
  CHECKIN_ADVANCE: 1,
  OCCUPIED: 3
})

export function getCurrentStatus(startedSchedule, nextSchedule, isCheckInAdvance) {
  if (!isEmpty(startedSchedule)) {
    return StatusScheduling.OCCUPIED;
  } else if (nextSchedule) {
    if (isCheckInAdvance) {
      return StatusScheduling.CHECKIN_ADVANCE;
    } else {
      return StatusScheduling.FREE;
    }
  }
  return StatusScheduling.FREE;
}