import React, {useContext, useEffect, useState} from "react";
import {View} from "react-native";
import {TouchableRipple, useTheme} from "react-native-paper";
import {format, addMinutes, isAfter, parseISO, isEqual, parse} from "date-fns";
import i18n from "i18n-js";
import {parseWithNoTZ} from "utils/dateUtils";

import {ScreenSaverContext} from "contexts/ScreenSaverContext";
import {StorageContext} from "contexts/StorageContext";
import {SchedulingContext} from "contexts/SchedulingContext";

import {getHourFormat} from "utils/hourUtils";
import {
  getNextMeeting,
  getNowHappeningMeeting,
  getStatusColor,
  verifyCheckInAdvance,
  verifyAtLeastOneCheckIn,
  verifyPresident,
  getCurrentStatus,
  StatusScheduling
} from "utils/schedulerUtils";

import useSync from "hooks/useSync";

import SkedwayLayout from "components/ScreenSaver/layouts/SkedwayLayout";
import {normalize} from "react-native-elements";

/**
 *  Screen Saver will appears when the app getting inactive during
 * x seconds.
 *
 * @param {*} param0
 * @returns a screen with local, capacity, day, next scheduled event infos
 */
const ScreenSaverScreen = ({navigation: {navigate}}) => {

  const [status, setStatus] = useState("");
  const [occupancy, setOccupancy] = useState("");

  const {setActive} = useContext(ScreenSaverContext);

  const {checkoutTimeSchedule, checkIn, todayDayScheduling} = useContext(SchedulingContext);

  const {appStorage} = useContext(StorageContext);
  const {space} = appStorage;

  const today = useSync();
  const {colors} = useTheme();

  const startedSchedule = getNowHappeningMeeting(today, todayDayScheduling);
  const nextSchedule = getNextMeeting(today, todayDayScheduling);
  const spaceName = space && space.name;
  const isThereAtLeastOneCheckIn = verifyAtLeastOneCheckIn(checkIn, startedSchedule, nextSchedule)
  const isCheckInAdvance = verifyCheckInAdvance(appStorage, nextSchedule);
  const currentStatus = getCurrentStatus(startedSchedule, nextSchedule, isCheckInAdvance);

  useEffect(() => {
    setScheduleValues();
    doCheckoutIfSpaceIsIdle();
  }, [startedSchedule, nextSchedule, today]);

  const setScheduleValues = () => {
    const {site} = appStorage;

    if (startedSchedule) {
      const {startDate, endDate} = startedSchedule;
      const formattedStartDate = getFormattedDate(startDate);
      const formattedEndDate = getFormattedDate(endDate);

      const status_ = isThereAtLeastOneCheckIn ? i18n.t("screenSaver.happening_now") :
        i18n.t("screenSaver.awaiting_check_in");
      setStatus(status_);
      setOccupancy(i18n.t("screenSaver.time_interval", [formattedStartDate, formattedEndDate]));
    } else {
      if (nextSchedule) {
        const formattedStartDate = getFormattedDate(nextSchedule?.startDate);

        if (isThereAtLeastOneCheckIn) {
          setStatus(i18n.t("screenSaver.happening_now"));
        } else if (isCheckInAdvance) {
          setStatus(i18n.t("screenSaver.awaiting_check_in"));
        } else {
          setStatus(i18n.t("screenSaver.next_meeting"));
        }

        setOccupancy(i18n.t("screenSaver.free_until", [formattedStartDate]));
      } else {
        const {closingTime} = site;
        const parsedClosingTime = parse(closingTime, "HH:mm:ss", new Date());
        const formattedEndDate = format(parsedClosingTime, getHourFormat(site?.hourFormat24))

        setStatus(i18n.t("screenSaver.all_day_free"));
        setOccupancy(i18n.t("screenSaver.free_until", [formattedEndDate]));
      }
    }
  }

  const getFormattedDate = (date) => {
    const {site} = appStorage;
    const dateWithNoTZ = parseWithNoTZ(date);
    return format(dateWithNoTZ, getHourFormat(site?.hourFormat24))
  }

  const doCheckoutIfSpaceIsIdle = () => {
    if (!isThereAtLeastOneCheckIn && !!startedSchedule) {
      const {id, userId} = startedSchedule;
      const noShowDateLimit = getDeadlineToCheckout();
      if (isEqual(today, noShowDateLimit) || isAfter(today, noShowDateLimit)) {
        checkoutTimeSchedule(userId, id);
      }
    }
  }

  const getDeadlineToCheckout = () => {
    if (startedSchedule) {
      const {startDate} = startedSchedule;
      const startDateISO = parseWithNoTZ(startDate);
      return addMinutes(startDateISO, getNoShowMinutes());
    }
  }

  const getNoShowMinutes = () => {
    const {company, space} = appStorage;
    return space?.baseType === 1 ? company?.noShowMinutes ?? 0 : company?.noShowMinutes2 ?? 0;
  }

  const statusColor = getStatusColor(appStorage, startedSchedule, nextSchedule, colors, checkIn);

  function getColor() {
    return currentStatus === StatusScheduling.FREE ? colors.lightGreen : currentStatus === StatusScheduling.CHECKIN_ADVANCE ?
      colors.lightYellow : currentStatus === StatusScheduling.OCCUPIED ? colors.lightRed : colors.lightGreen;
  }

  const isPresident = verifyPresident(appStorage);

  const getProps = () => (
    {
      startedSchedule,
      nextSchedule,
      currentStatus,
      statusColor,
      spaceName,
      isPresident,
      isThereAtLeastOneCheckIn,
      status,
      occupancy,
      deadlineToCheckout: getDeadlineToCheckout(),
      isCheckInAdvance
    }
  )

  return (
    <View style={{flex: 1, backgroundColor: getColor(), padding: normalize(20)}}>
      <TouchableRipple
        onPress={() => {
          setActive();
          navigate("Home");
        }}
      >
        <SkedwayLayout {...getProps()}/>
      </TouchableRipple>
    </View>
  );
};

ScreenSaverScreen.navigationOptions = () => {
  return {
    headerShown: false,
  };
};

export default ScreenSaverScreen;
