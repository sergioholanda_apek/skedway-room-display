import React, {useContext, useEffect, useState} from "react";
import {StyleSheet, View,} from "react-native";
import {Text, useTheme, ActivityIndicator} from "react-native-paper";
import i18n from "i18n-js";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {navigate} from "utils/navigationRef";

import {StorageContext} from "contexts/StorageContext";
import {LoginFlowContext} from "contexts/LoginFlowContext";
import {SchedulingContext} from "contexts/SchedulingContext";

import CustomDialog from "components/common/dialog/CustomDialog";
import CustomInput from "components/common/CustomInput";

import normalize from "utils/normalize";

const AuthDialog = ({visible, onDismiss}) => {

  const {unit, colors} = useTheme();

  const {appStorage} = useContext(StorageContext);
  const {clearErrors, isFetching} = useContext(LoginFlowContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const {setInitialState} = useContext(SchedulingContext);

  const {
    setInitialState: setInitialStateLogin,
    loginAdmin,
    logoutSuccessfully,
    emailError,
    passwordError
  } = useContext(LoginFlowContext);

  const onConfirmAction = () => {
    const {site} = appStorage;
    loginAdmin(email, password, site?.companyId);
  };

  useEffect(() => {
    if (logoutSuccessfully) {
      logout();
    }
  }, [logoutSuccessfully])

  const logout = () => {
    try {
      onDismiss();
      clearAppData();
      setInitialStateLogin();
      setInitialState();
      navigate("CheckLogin", {factoryReset: true});
    } catch (error) {
      throw new Error(error.message);
    }
  };

  const clearAppData = async function () {
    try {
      const keys = await AsyncStorage.getAllKeys();
      await AsyncStorage.multiRemove(keys);
    } catch (error) {
      throw new Error('Error clearing app data. Cause: ' + error.message);
    }
  }

  function AuthForm() {
    return (
      <View style={styles.dialogContentContainer}>
        <Text
          style={[
            styles.connectivityText,
            {
              color: colors.primary,
              paddingBottom: unit(2),
            },
          ]}
        >
          {i18n.t("success_internet_connection")}
        </Text>

        <View style={{paddingVertical: normalize(2)}}>
          <CustomInput
            mode="outlined"
            label={i18n.t("prompt_email")}
            value={email}
            onChangeText={setEmail}
            errorMessage={emailError}
            autoCapitalize="none"
            autoCorrect={false}
            autoFocus
            ellipsizeMode="tail"
            numberOfLines={1}
            multiline={false}
          />
        </View>

        <View style={{paddingVertical: normalize(2)}}>
          <CustomInput
            mode="outlined"
            label={i18n.t("prompt_password")}
            value={password}
            onChangeText={setPassword}
            errorMessage={passwordError}
            secureTextEntry
            autoCapitalize="none"
            autoCorrect={false}
          />
        </View>
      </View>
    )
  }

  return (
    <>
      {
        isFetching &&
        <ActivityIndicator
          size="large"
          animating={true}
          color="green"
        />
      }
      <CustomDialog
        style={{marginHorizontal: "15%"}}
        visible={visible}
        onDismiss={onDismiss}
        title={i18n.t("title_activity_user")}
        confirmLabel="Ok"
        confirmAction={onConfirmAction}
        cancelLabel={i18n.t("action_cancel")}
        cancelAction={onDismiss}
        content={AuthForm()}
      />
    </>
  );
};

const styles = StyleSheet.create({
  leftArrowIcon: {
    fontSize: 24,
    padding: 8,
    paddingLeft: 0,
  },
  loadingIndicator: {
    padding: 16,
  },
  connectivityText: {
    textTransform: "uppercase",
    fontWeight: "600",
  },
  dialogContentContainer: {
    justifyContent: "space-around",
  },
});

export default AuthDialog;
