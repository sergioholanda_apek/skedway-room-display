import React, {createContext, useContext, useState} from "react";

import {axios_, getAuthToken} from "api";
import {CREATE_ISSUE} from "api/endpoints";

import {LoadingContext} from "contexts/LoadingContext";

export const IssueContext = createContext({});

let authToken = undefined;

const IssueProvider = ({children}) => {

  const [issues, setIssues] = useState([]);
  const [hasCreatedIssueSuccessfully, setHasCreatedIssueSuccessfully] = useState(false);
  const [isCreatingIssue, setCreatingIssue] = useState(false);

  const {setLoadingResource} = useContext(LoadingContext);

  function setInitialState() {
    setIssues([]);
  }

  async function createIssue(userId, asset, amenities, description) {
    setLoadingResource({show: true});
    setCreatingIssue(true);
    authToken = await getAuthToken();

    try {

      const response = await axios_.post(CREATE_ISSUE, null, {
        params: {
          authToken,
          issue: {
            userId,
            asset,
            amenities,
            description,
          },
        },
      });

      const issues_ = [response.data, ...issues];
      setIssues(issues_);

      setHasCreatedIssueSuccessfully(true);
    } catch (error) {
      throw new Error(error.message);
      setLoadingResource({show: false});
    } finally {
      setCreatingIssue(false);
      // Resetting so it can be inferred in another request
      setHasCreatedIssueSuccessfully(false);
      setLoadingResource({show: false});
    }
  }

  return (
    <IssueContext.Provider value={{
      issues,
      setIssues,
      hasCreatedIssueSuccessfully,
      setHasCreatedIssueSuccessfully,
      isCreatingIssue,
      setCreatingIssue,
      createIssue
    }}>
      {children}
    </IssueContext.Provider>
  )
}

export default IssueProvider;
