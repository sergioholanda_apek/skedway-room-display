import React, {useEffect} from "react";
import {Keyboard, KeyboardAvoidingView, StyleSheet, View} from "react-native";
import {Dialog, Headline, Paragraph, Portal, useTheme,} from "react-native-paper";

import i18n from "i18n-js";
import normalize from "utils/normalize";
import CustomButton from "../buttons/CustomButton";

const CustomDialog = (props) => {

  const {
    visible = false,
    onDismiss = null,
    cancelAction = null,
    confirmAction = null,
    confirmLabel = i18n.t("action_confirm"),
    cancelLabel = i18n.t("action_cancel"),
    dismissable = true,
    title = "",
    content = "",
    Icon = null,
    children,
    type = "primary",
    ...otherProps
  } = props;

  if (!visible) {
    return null;
  }

  useEffect(() => {
    Keyboard.dismiss();
  }, []);

  const {colors} = useTheme();

  return (
    <Portal>
      <KeyboardAvoidingView
        keyboardVerticalOffset={-100}
        behavior="position"
        contentContainerStyle={styles.avoidViewContainer}
        style={styles.avoidViewContainer}
      >
        <Dialog
          visible={visible}
          dismissable={dismissable}
          onDismiss={onDismiss}
          {...otherProps}
        >
          <Dialog.Title style={styles.titleContainer}>
            <Headline style={[styles.title, {color: colors.text}]}>{title}</Headline>
          </Dialog.Title>
          <Dialog.Content>
            {
              (typeof content === 'string') ? (
                <Paragraph style={[styles.content, {color: colors.placeholder}]}>
                  {content}
                </Paragraph>
              ) : (
                <View style={styles.contentEl}>
                  {content}
                </View>
              )
            }
          </Dialog.Content>
          {cancelAction || confirmAction ? (
            <Dialog.Actions style={{padding: normalize(14)}}>
              <View style={{flex: 1, flexDirection: "row", justifyContent: "flex-end"}}>
                {
                  cancelAction &&
                  <CustomButton
                    onPress={cancelAction}
                    type="primary"
                    label={cancelLabel}
                    style={styles.scheduleButton}
                    labelStyle={styles.scheduleButtonLabel}
                  />
                }
                {
                  confirmAction &&
                  <View style={{paddingLeft: 20}}>
                    <CustomButton
                      onPress={confirmAction}
                      type="secondary"
                      label={confirmLabel}
                      style={styles.scheduleButton}
                      labelStyle={styles.scheduleButtonLabel}
                    />
                  </View>
                }
              </View>
            </Dialog.Actions>
          ) : null}
        </Dialog>
      </KeyboardAvoidingView>
    </Portal>
  );
};

const styles = StyleSheet.create({
  scheduleButton: {
    paddingTop: normalize(2),
    paddingBottom: normalize(2),
    borderRadius: normalize(10),
  },
  scheduleButtonLabel: {
    fontSize: normalize(9)
  },
  cardContainer: {
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    elevation: 3,
  },
  iconContainer: {
    alignSelf: "center",
  },
  titleContainer: {
    flexDirection: "row",
    paddingTop: normalize(12),
    paddingLeft: normalize(10)
  },
  title: {
    fontFamily: 'LibreBaskerville_400Regular_Italic',
    fontSize: normalize(12),
  },
  content: {
    padding: normalize(5),
    height: normalize(30),
    paddingLeft: normalize(9),
    fontSize: normalize(9),
    fontFamily: "Manrope_400Regular",
    flexWrap: "wrap",
  },
  contentEl: {
    padding: normalize(5),
    paddingLeft: normalize(9),
    fontSize: normalize(9),
    fontFamily: "Manrope_400Regular",
  },
  button: {
    fontFamily: "Manrope_400Regular",
  },
  buttonLabel: {
    fontSize: normalize(8),
    fontFamily: "Manrope_600SemiBold",
  },
  avoidViewContainer: {
    flex: 1,
    position: "absolute",
    bottom: 0,
    top: 0,
    right: 0,
    left: 0,
  },
});

export default CustomDialog;
